package com.jsl.stepper

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jsl.stepper.utils.BaseApp
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProcessControlActivity : AppCompatActivity(),BaseApp.Listener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_process_control)
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
    }

    override fun setAdapter() {
    }

    override fun loadData() {
    }

    override fun setContent() {
    }
}