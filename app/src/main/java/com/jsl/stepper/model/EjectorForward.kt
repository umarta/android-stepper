package com.jsl.stepper.model

class EjectorForward(
    var press: Press,
    var speed: Speed,
    var stroke: Store
) {
    class Press(
        var efp1: Int,
        var efp2: Int,
        var efp3: Int
    )

    class Speed(
        var efs1: Int,
        var efs2: Int,
        var efs3: Int
    )

    class Store(
        var efst1: Int,
        var efst2: Int,
        var eft13: Int
    )
}
