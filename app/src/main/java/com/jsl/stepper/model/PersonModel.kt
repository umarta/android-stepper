package com.jsl.stepper.model

import okhttp3.MultipartBody

class PersonModel(
    var fullname: String,
    var sex: String,
    var ktpNo: String,
//    var photoKtp: MultipartBody.Part,
    var photoKtp64: String,
    var photoSelfie: MultipartBody.Part,
    var photoSelfie64: String,
    var npwpNo: String,
    var phoneNo: String?,
    var username: String,
    var password: String) {
}