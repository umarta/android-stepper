package com.jsl.stepper.model

class Charging(
    var bp: BP,
    var press: Press,
    var speed: Speed,
    var stroke: Stroke
) {
    class BP(
        var Chbp1: Int,
        var Chbp2: Int,
        var Chbp3: Int,
        var Chbp4: Int
    )

    class Press(
        var Chp1: Int,
        var Chp2: Int,
        var Chp3: Int,
        var Chp4: Int
    )

    class Speed(
        var Chs1: Int,
        var Chs2: Int,
        var Chs3: Int,
        var Chs4: Int
    )
    class Stroke(
        var Chst1: Int,
        var Chst2: Int,
        var Chst3: Int,
        var Chst4: Int
    )

}