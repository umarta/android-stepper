package com.jsl.stepper.model

class EjectorBackward(
    var press: Press,
    var speed: Speed,
    var stroke: Store
) {
    class Press(
        var ebp1: Int,
        var ebp2: Int,
        var ebp3: Int
    )

    class Speed(
        var ebs1: Int,
        var ebs2: Int,
        var ebs3: Int
    )

    class Store(
        var ebst1: Int,
        var ebst2: Int,
        var ebt13: Int
    )

}