package com.jsl.stepper.model

class EjectorSetting(
    var ejectorForward: EjectorForward,
    var ejectorBackward: EjectorBackward
) {
    class EjectorForward(
        var press: Press,
        var speed: Speed,
        var stroke: Store
    ) {
        class Press(
            var efp1: Int,
            var efp2: Int,
        )

        class Speed(
            var efs1: Int,
            var efs2: Int,
        )

        class Store(
            var efst1: Int,
            var efst2: Int,
        )
    }

    class EjectorBackward(
        var press: Press,
        var speed: Speed,
        var stroke: Store
    ) {
        class Press(
            var ebp1: Int,
            var ebp2: Int,
        )

        class Speed(
            var ebs1: Int,
            var ebs2: Int,
        )

        class Store(
            var ebst1: Int,
            var ebst2: Int,
        )

    }
}