package com.jsl.stepper.model

class InjectionSetting(
    var holding: Holding,
    var injection: Injection
) {
    class Holding(
        var press: Press,
        var speed: Speed,
        var time: Time
    ) {
        class Press(
            var Ishp1: Int? = null,
            var Ishp2: Int? = null,
            var Ishp3: Int? = null,
            var Ishp4: Int? = null,
            var Ishp5: Int? = null,
        )

        class Speed(
            var Ishs1: Int? = null,
            var Ishs2: Int? = null,
            var Ishs3: Int? = null,
            var Ishs4: Int? = null,
            var Ishs5: Int? = null,
        )

        class Time(
            var Isht1: Int? = null,
            var Isht2: Int? = null,
            var Isht3: Int? = null,
            var Isht4: Int? = null,
            var Isht5: Int? = null,
        )
    }

    class Injection(
        var press: Press,
        var speed: Speed,
        var stroke: Stroke,
        var time: Time
    ) {
        class Press(
            var Isip1: Int? = null,
            var Isip2: Int? = null,
            var Isip3: Int? = null,
            var Isip4: Int? = null,
            var Isip5: Int? = null,
            var Isip6: Int? = null,
        )

        class Speed(
            var Issp1: Int? = null,
            var Issp2: Int? = null,
            var Issp3: Int? = null,
            var Issp4: Int? = null,
            var Issp5: Int? = null,
            var Issp6: Int? = null,
        )

        class Stroke(
            var Isstp1: Int? = null,
            var Isstp2: Int? = null,
            var Isstp3: Int? = null,
            var Isstp4: Int? = null,
            var Isstp5: Int? = null,
            var Isstp6: Int? = null,
        )

        class Time(
            var Istp1: Int? = null
        )
    }
}