package com.jsl.stepper.model

class Plastizing(
    var charging: Charging,
    var suckBack: SuckBack

) {
    class Charging(
        val backPress: BackPress,
        val press: Press,
        val speed: Speed,
        val stroke: Stroke
    ) {
        class BackPress(
            val pUcBP1: Int? = null,
            val pUcBP2: Int? = null,
            val pUcBP3: Int? = null,
            val pUcBP4: Int? = null,

            )

        class Press(
            val pUcP1: Int? = null,
            val pUcP2: Int? = null,
            val pUcP3: Int? = null,
            val pUcP4: Int? = null,

            )

        class Speed(
            val pUcS1: Int? = null,
            val pUcS2: Int? = null,
            val pUcS3: Int? = null,
            val pUcS4: Int? = null,

            )

        class Stroke(
            val pUcSt1: Int? = null,
            val pUcSt2: Int? = null,
            val pUcSt3: Int? = null,
            val pUcSt4: Int? = null,

            )
    }

    class SuckBack(
        val press: Press,
        val speed: Speed,
        val stroke: Stroke,
        val coolTime: CoolTime

    ) {
        class Press(
            val pUsBP1: Int? = null,

            )

        class Speed(
            val pUsBS1: Int? = null,

            )

        class Stroke(
            val pUsBSt1: Int? = null,

            )

        class CoolTime(
            val pUsBT1: Int? = null,

            )
    }
}