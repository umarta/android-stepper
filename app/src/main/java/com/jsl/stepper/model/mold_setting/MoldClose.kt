package com.jsl.stepper.model.mold_setting

import com.jsl.stepper.model.mold_setting.mold_close.Press
import com.jsl.stepper.model.mold_setting.mold_close.Speed
import com.jsl.stepper.model.mold_setting.mold_close.Stroke

class MoldClose(
    var press: Press,
    var speed: Speed,
    var stroke: Stroke

) {
}