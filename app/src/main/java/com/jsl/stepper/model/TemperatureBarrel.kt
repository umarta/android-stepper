package com.jsl.stepper.model

class TemperatureBarrel(
    var setting: Setting,
    var acttual: Actual
) {
    class Setting(
        var HN: Int,
        var H1: Int,
        var H2: Int,
        var H3: Int,
        var H4: Int,
        var H5: Int,
        var H6: Int,
        var Hopper: Int,
        var Sat: String
    )

    class Actual(
        var HN: Int,
        var H1: Int,
        var H2: Int,
        var H3: Int,
        var H4: Int,
        var H5: Int,
        var H6: Int,
        var Hopper: Int,
        var Sat: String
    )
}