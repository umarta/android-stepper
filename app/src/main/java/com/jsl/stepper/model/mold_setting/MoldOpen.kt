package com.jsl.stepper.model.mold_setting

import com.jsl.stepper.model.mold_setting.mold_open.Press
import com.jsl.stepper.model.mold_setting.mold_open.Speed
import com.jsl.stepper.model.mold_setting.mold_open.Stroke

class MoldOpen(
    var press: Press,
    var speed: Speed,
    var stroke: Stroke,
)