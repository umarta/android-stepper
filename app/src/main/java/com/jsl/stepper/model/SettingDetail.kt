package com.jsl.stepper.model

import com.jsl.stepper.data.remote.response.wo.WoDetailData

class SettingDetail(
    val preview: WoDetailData,
    val mold_setting: MoldSetting,
    val injection_setting: InjectionSetting,
    val ejector_setting: EjectorSetting,
    val plastizing: Plastizing,
    val temperature_setting: TemperatureSetting,
    val id: Int,
    val molding_id: Int,
    val womolding_id: Int,
    val sequenceno: Int,
    val code: String,
    val cavity: Int,
    val name: String,
    val tonase: Int,
    val colour: String,
    val machine: String,
    val jenis_proses: String,
    val cycle_time: Int,
    val material: String,
    val berat_part: Int,
    val berat_runner: Int,
) {

}