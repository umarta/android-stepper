package com.jsl.stepper.model

import com.jsl.stepper.model.mold_setting.MoldClose
import com.jsl.stepper.model.mold_setting.MoldOpen

class MoldSetting(
    var mold_open: MoldOpen,
    var mold_close: MoldClose
)
