package com.jsl.stepper.model

class TemperatureSetting(
    val temperatureMtc: TemperatureMtc,
    val temperatureBarrel: TemperatureBarrel,
    val temperatureHopper: TemperatureHopper
) {
    class TemperatureBarrel(
        val actual: Actual,
        val setting: Setting
    ) {
        class Actual(
            val tSTbAHn: Int? = null,
            val tSTbAH1: Int? = null,
            val tSTbAH2: Int? = null,
            val tSTbAH3: Int? = null,
            val tSTbAH4: Int? = null,
            val tSTbAH5: Int? = null,
            val tSTbAH6: Int? = null,

            )

        class Setting(
            val tSTbSHn: Int? = null,
            val tSTbSH1: Int? = null,
            val tSTbSH2: Int? = null,
            val tSTbSH3: Int? = null,
            val tSTbSH4: Int? = null,
            val tSTbSH5: Int? = null,
            val tSTbSH6: Int? = null,
        )
    }

    class TemperatureHopper(
        val actual: Actual,
        val setting: Setting
    ) {
        class Actual(
            val tStHA: Int? = null,

            )

        class Setting(
            val tStHS: Int? = null,

            )
    }

    class TemperatureMtc(
        val actual: Actual,
        val setting: Setting
    ) {
        class Actual(
            val tStMA: Int? = null,

            )

        class Setting(
            val tStMS: Int? = null,

            )
    }
}