package com.jsl.stepper.widget


import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager
import com.jsl.stepper.R

class MyViewPager(context: Context, attrs: AttributeSet?) : ViewPager(context, attrs) {
    private var swipeable = false

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.MyViewPager)
        swipeable = try {
            a.getBoolean(R.styleable.MyViewPager_swipeable, true)
        } finally {
            a.recycle()
        }
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return swipeable && super.onInterceptTouchEvent(event)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return swipeable && super.onTouchEvent(event)
    }


}