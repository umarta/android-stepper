package com.jsl.stepper.data.remote.response

class IdNameData(
    var id: String,
    var name: String) {

    override fun toString(): String {
        return name
    }
}