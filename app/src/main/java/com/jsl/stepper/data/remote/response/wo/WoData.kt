package com.jsl.stepper.data.remote.response.wo

class WoData(
    var id: Int,
    var documentno: String,
    var molding:Int,
    var checked:Int,
    var date:String,
    var desc:String
)