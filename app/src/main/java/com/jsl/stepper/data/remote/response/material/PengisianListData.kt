package com.jsl.stepper.data.remote.response.material

class PengisianListData(
    val id: Int,
    val l_machine_id: Long? = null,
    val material_id: Long? = null,
    val grade_id: Long? = null,
    val color_id: Long? = null,
    val grade_remark_id: Long? = null,
    val diisi: Long? = null,
    val diambil: Long? = null,
    val machine: String? = null,
    val material: String? = null,
    val grade: String? = null,
    val color: String? = null,
    val grade_remark: String? = null,

    ) {
}