package com.jsl.stepper.data.user

import com.jsl.stepper.data.local.Storage
import com.jsl.stepper.data.remote.response.auth.LoginData
import com.jsl.stepper.data.user.UserDataRepository
import com.jsl.stepper.utils.Constants
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserManager @Inject constructor(
    private val storage: Storage,
    private val userDataRepository: UserDataRepository
) {
    fun loginSaveUserData(loginData: LoginData){
        storage.setBoolean(Constants.PREF_IS_LOGIN, true)
        storage.setString(Constants.PREF_TOKEN, "Bearer " + loginData.token!!)
        storage.setString(Constants.PREF_ID, loginData.user.id!!)
        storage.setString(Constants.PREF_NAME, loginData.user.name!!)
    }


    val tokenFCM: String get() = storage.getString(Constants.PREF_TOKEN_FCM)
    val token: String get() = storage.getString(Constants.PREF_TOKEN)
    val isLogin: Boolean get() = storage.getBoolean(Constants.PREF_IS_LOGIN)
    val name: String get() = storage.getString(Constants.PREF_NAME)
    val id: String get() = storage.getString(Constants.PREF_ID)

    val avatar: String get() = storage.getString(Constants.PREF_AVATAR)
    val phone: String get() = storage.getString(Constants.PREF_PHONE_NUMBER)
    val email: String get() = storage.getString(Constants.PREF_EMAIL)
    val warehouseId: String get() = storage.getString(Constants.WAREHOUSE_ID)
    val warehouseName: String get() = storage.getString(Constants.WAREHOUSE_NAME)

    fun setNumberOtp(number: String) = userDataRepository.setPhoneNumber(number)
    fun setOtpID(otpID: String) = userDataRepository.setOtpID(otpID)
    fun setFcmToken(token: String) = storage.setString(Constants.PREF_TOKEN, token)
    fun setName(name: String) = storage.setString(Constants.PREF_NAME, name)
    fun setAvatar(avatar: String) = storage.setString(Constants.PREF_AVATAR, avatar)
    fun setTokenFCM(tokenFCM: String) = storage.setString(Constants.PREF_TOKEN_FCM, tokenFCM)

    val numberOtp: String? get() = userDataRepository.phoneNumber
    val otpID: String get() = userDataRepository.otpID
    val getFcmToken: String get() = storage.getString(Constants.PREF_TOKEN_FCM)

    fun logout() {
        storage.clear()
    }

    fun setWarehouse(warehouseId:String,warehouseName:String){
        storage.setString(Constants.WAREHOUSE_ID, warehouseId)
        storage.setString(Constants.WAREHOUSE_NAME, warehouseName)

    }


}
