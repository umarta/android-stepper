package com.jsl.stepper.data.remote

import androidx.lifecycle.MutableLiveData
import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.auth.LoginData
import com.jsl.stepper.data.remote.response.ipqc.IpQcData
import com.jsl.stepper.data.remote.response.material.PengisianListData
import com.jsl.stepper.data.remote.response.production.ProductionControlData
import com.jsl.stepper.data.remote.response.production.ProductionData
import com.jsl.stepper.data.remote.response.serahterima.SerahTerimaListData
import com.jsl.stepper.data.remote.response.serahterima.SerahterimaData
import com.jsl.stepper.data.remote.response.setup.MoldDetail
import com.jsl.stepper.data.remote.response.shifting.ShiftingDetail
import com.jsl.stepper.data.remote.response.shifting.ShiftingList
import com.jsl.stepper.data.remote.response.wo.WoData
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.model.SettingDetail
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.HashMap


class MainRepository(private val mainService: MainService) {
    private val compositeDisposable = CompositeDisposable()
    val loading = MutableLiveData<Boolean>()

    fun login(username: String, password: String): MutableLiveData<BaseResponse<LoginData>> {

        val data = MutableLiveData<BaseResponse<LoginData>>()
        loading.value = true
        mainService.login(username, password)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun refreshToken(): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
//        loading.value = true

        mainService.refreshToken()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun woList(): MutableLiveData<BaseResponse<MutableList<WoData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<WoData>>>()
        loading.value = true
        mainService.woList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun woDetail(
        woId: String,
        page: Int
    ): MutableLiveData<BaseResponse<MutableList<WoDetailData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<WoDetailData>>>()
        loading.value = true
        mainService.woDetail(woId, page)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun getProductionControlData(
        page: Int
    ): MutableLiveData<BaseResponse<MutableList<ProductionControlData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<ProductionControlData>>>()
        loading.value = true
        mainService.getProductionControlData(page)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun serahterimaList(page: Int): MutableLiveData<BaseResponse<MutableList<SerahTerimaListData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<SerahTerimaListData>>>()
        loading.value = true
        mainService.serahterimaList(page)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun shiftingList(
        page: Int,
        tahun: Int,
        bulan: Int
    ): MutableLiveData<BaseResponse<MutableList<ShiftingList>>> {
        val data = MutableLiveData<BaseResponse<MutableList<ShiftingList>>>()
        mainService.shiftingList(page, tahun, bulan)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                }, {
                    it.printStackTrace()
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun getShiftHarian(
        tanggal: String
    ): MutableLiveData<BaseResponse<MutableList<ShiftingDetail>>> {
        val data = MutableLiveData<BaseResponse<MutableList<ShiftingDetail>>>()
        mainService.getShiftHarian(tanggal)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                }, {
                    it.printStackTrace()
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun editShift(request: HashMap<String, Any>): MutableLiveData<BaseResponse<ShiftingDetail>> {
        val data = MutableLiveData<BaseResponse<ShiftingDetail>>()
        loading.value = true
        mainService.updateShift(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }
    fun generateShift(request: HashMap<String, Any>): MutableLiveData<BaseResponse<MutableList<ShiftingList>>> {
        val data = MutableLiveData<BaseResponse<MutableList<ShiftingList>>>()
        loading.value = true
        mainService.generateShift(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }



    fun terima(page: Int): MutableLiveData<BaseResponse<MutableList<SerahTerimaListData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<SerahTerimaListData>>>()
        loading.value = true
        mainService.terima(page)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun woDetailOther(
        page: Int,
        type: String
    ): MutableLiveData<BaseResponse<MutableList<WoDetailData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<WoDetailData>>>()
        loading.value = true
        mainService.woDetailOther(page, type)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun colour(): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.colour()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun settingDetail(settingId: Int): MutableLiveData<BaseResponse<SettingDetail>> {
        val data = MutableLiveData<BaseResponse<SettingDetail>>()
        loading.value = true
        mainService.settingDetail(settingId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun getIpMaterialList(machineId: Long): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.getIpMaterialList(machineId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun getWoSerahterimaDetail(
        settingId: String,
        type: String
    ): MutableLiveData<BaseResponse<SerahterimaData>> {
        val data = MutableLiveData<BaseResponse<SerahterimaData>>()
        loading.value = true
        mainService.getWoSerahterimaDetail(settingId, type)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun terimaAct(request: HashMap<String, Any>): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
        loading.value = true
        mainService.terimaAct(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun getIpQcDetail(id: Long): MutableLiveData<BaseResponse<IpQcData>> {
        val data = MutableLiveData<BaseResponse<IpQcData>>()
        loading.value = true
        mainService.getIpQcDetail(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun jenisProses(): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.jenisProses()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun getMachineList(): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.getMachineList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun getPengisianList(
        page: Int,
        machineId: Long? = null,
        materialId: Int? = null,
        startFrom: String? = null,
        endAt: String? = null
    ): MutableLiveData<BaseResponse<MutableList<PengisianListData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<PengisianListData>>>()
        loading.value = true
        mainService.getPengisianList(page, machineId, materialId, startFrom, endAt)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun material(): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.material()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun getMaterialGrade(): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.getMaterialGrade()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun getMaterialGradeRemark(materialId: String,gradeId: String): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.getMaterialGradeRemark(materialId,gradeId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun getMachine(): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.getMachine()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun getWoSerahterima(): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.getWoSerahterima()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun getSeqSerahterima(id: String): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.getSeq(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun getWO(): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.getWO()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun getWoDetail(query: String): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.getWoDetail(query)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun setupMoldDetail(
        wo_id: String,
        wo_mold_id: Long
    ): MutableLiveData<BaseResponse<MoldDetail>> {
        val data = MutableLiveData<BaseResponse<MoldDetail>>()
        loading.value = true
        mainService.getSetupMolDetail(wo_id, wo_mold_id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun getProductionData(query: Int): MutableLiveData<BaseResponse<ProductionData>> {
        val data = MutableLiveData<BaseResponse<ProductionData>>()
        loading.value = true
        mainService.getProductionData(query)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun getWoList(): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.getMachine()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun submit(request: HashMap<String, Any>): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
        loading.value = true
        mainService.submit(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun submitSerah(request: HashMap<String, Any>): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
        loading.value = true
        mainService.submitSerah(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun submitIpQc(id: Long, request: HashMap<String, Any>): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
        loading.value = true
        mainService.submitIpQc(id, request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun submitSetupMold(request: HashMap<String, Any>): MutableLiveData<BaseResponse<MoldDetail>> {
        val data = MutableLiveData<BaseResponse<MoldDetail>>()
        loading.value = true
        mainService.submitSetupMold(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun submitSetupMachine(request: HashMap<String, Any>): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
        loading.value = true
        mainService.submitSetupMachine(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun submitProduction(request: HashMap<String, Any>): MutableLiveData<BaseResponse<ProductionData>> {
        val data = MutableLiveData<BaseResponse<ProductionData>>()
        loading.value = true
        mainService.submitProduction(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun submitPengisian(request: HashMap<String, Any>): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
        loading.value = true
        mainService.submitPengisian(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    private fun Disposable.addTo(disposable: CompositeDisposable) {
        disposable.add(this)
    }

    fun onDestroy() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

}