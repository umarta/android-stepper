package com.jsl.stepper.data.remote.response.serahterima

class SerahTerimaListData(
    val id: Int? = null,
    val kode_part: String? = null,
    val nama_produk: String? = null,
    val qty_produksi: Long? = null,
    val qty_terima: Long? = null,
    val qty_serah: Long? = null,
    val qty_box: Long? = null,
    val qty_sisa: Long? = null,
    val qty_balance: Long? = null,
    val no_wo: String? = null,
    val sequenceno: Long? = null,

    ) {
}