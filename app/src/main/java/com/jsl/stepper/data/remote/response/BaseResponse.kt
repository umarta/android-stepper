package com.jsl.stepper.data.remote.response

class BaseResponse<T>(
    var code: Int?,
    var success :Boolean?,
    var data: T,
    var message: MutableList<String>?,
    var header: Header
) {

    class Header(
        var current_page: Int,
        var total_page: Int,
        var total_data: Int,
        var next_page: Boolean) {}

}