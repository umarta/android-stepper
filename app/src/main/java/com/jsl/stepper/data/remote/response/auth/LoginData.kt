package com.jsl.stepper.data.remote.response.auth

class LoginData(
    var user: User,
    var token: String?
) {

    class User(
        var id: String?,
        var name: String?
    )
}