package com.jsl.stepper.data.remote.response.serahterima

class SerahterimaData(
    var id: Int? = null,
    var kode_mold: String? = null,
    var kode_part: String? = null,
    var qty_wo: Int? = null,
    var qty_ok: Int? = null,
    var qty_sisa_wo: Int? = null,
    var wo_no: String? = null,
    var qty_serah: String? = null,
    var qty_box: String? = null,
    var qty_sisa: String? = null,
    var sequenceno: String? = null,
) {
}