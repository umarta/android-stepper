package com.jsl.stepper.data.remote.response.production

class ProductionControlData(
    val l_wo_id: Long? = null,
    val l_womolding_id: Long? = null,
    val no_wo: String? = null,
    val kode_mold: String? = null,
    val part_name: String? = null,
    val plan_machine: String? = null,
    val actual_machine: String? = null,
    val production_status: String? = null,
    val production_remark: String? = null
) {
}