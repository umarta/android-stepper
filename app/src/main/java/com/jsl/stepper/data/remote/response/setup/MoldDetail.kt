package com.jsl.stepper.data.remote.response.setup

import com.jsl.stepper.data.remote.response.IdNameData

class MoldDetail(
    val id: Long? = null,
    val type: Int? = null,
    val wo_id: String? = null,
    val duration: Long? = null,
    val wo_mold_id: Long? = null,
    val machine_id: Long? = null,
    val part_no: String? = null,
    val status: String? = null,
    val machine: IdNameData? = null
) {
}
/*        "id": 9,
        "type": 1,
        "wo_id": "1000025",
        "duration": "391",
        "wo_mold_id": "1000034",
        "machine_id": "1000013",
        "part_no": "M 9550-1-2",
*/