package com.jsl.stepper.data.remote.response.shifting

class ShiftingList(
    val tanggal: String? = null,
    val hari: String? = null,
    val list: MutableList<ShiftingDetail>
) {
}