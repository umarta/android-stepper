package com.jsl.stepper.data.remote.response.machine

class MachineList(
    var id: Int,
    var name: String,
    var value: String,
    var tonase: String
) {
}