package com.jsl.stepper.data.remote.response.ipqc

class IpQcData(
    val l_womolding_id: Long? = null,
    val urutan: String? = null,
    val image: String? = null,
    val part_bom: MutableList<BomPartData>
) {
}