package com.jsl.stepper.data.remote.response.production

class ProductionData(
    var id: Int? = null,
    var user_id: Int? = null,
    var total_wo: Int? = null,
    var total_shot: Int? = null,
    var total_ng: Int? = null,
    var total_ok: Int? = null,
    var start_at: String? = null,
    var finish_at: String? = null,
    var status: Int? = null,
    var total_short: Int? = null,
    var total_welding: Int? = null,
    var total_silver: Int? = null,
    var total_flow_mark: Int? = null,
    var total_crack: Int? = null,
    var total_scratch: Int? = null,
    var total_other: Int? = null,

    ) {
}