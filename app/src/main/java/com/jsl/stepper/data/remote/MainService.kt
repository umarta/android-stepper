package com.jsl.stepper.data.remote

import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.auth.LoginData
import com.jsl.stepper.data.remote.response.ipqc.IpQcData
import com.jsl.stepper.data.remote.response.material.PengisianListData
import com.jsl.stepper.data.remote.response.production.ProductionControlData
import com.jsl.stepper.data.remote.response.production.ProductionData
import com.jsl.stepper.data.remote.response.serahterima.SerahTerimaListData
import com.jsl.stepper.data.remote.response.serahterima.SerahterimaData
import com.jsl.stepper.data.remote.response.setup.MoldDetail
import com.jsl.stepper.data.remote.response.shifting.ShiftingDetail
import com.jsl.stepper.data.remote.response.shifting.ShiftingList
import com.jsl.stepper.data.remote.response.wo.WoData
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.model.SettingDetail
import io.reactivex.Observable
import retrofit2.http.*
import java.util.*

interface MainService {
    @POST("auth/login")
    fun login(
        @Query("username") username: String,
        @Query("password") password: String
    ): Observable<BaseResponse<LoginData>>


    @GET("auth/check")
    fun refreshToken(): Observable<BaseResponse<Any>>

    @GET("molding/wo-list")
    fun woList(): Observable<BaseResponse<MutableList<WoData>>>

    @GET("molding/wo-item-list")
    fun woDetail(
        @Query("wo_id") warehouseId: String,
        @Query("page") page: Int
    ): Observable<BaseResponse<MutableList<WoDetailData>>>

    @GET("production/control")
    fun getProductionControlData(
        @Query("page") page: Int
    ): Observable<BaseResponse<MutableList<ProductionControlData>>>

    @GET("serah-terima")
    fun serahterimaList(
        @Query("page") page: Int
    ): Observable<BaseResponse<MutableList<SerahTerimaListData>>>

    @GET("serah-terima/terima")
    fun terima(
        @Query("page") page: Int
    ): Observable<BaseResponse<MutableList<SerahTerimaListData>>>

    @POST("serah-terima")
    fun submitSerah(@Body request: HashMap<String, Any>): Observable<BaseResponse<Any>>

    @GET("molding/wo-item-list")
    fun woDetailOther(
        @Query("page") page: Int,
        @Query("type") warehouseId: String
    ): Observable<BaseResponse<MutableList<WoDetailData>>>

    @GET("molding/colour")
    fun colour(): Observable<BaseResponse<MutableList<IdNameData>>>


    @GET("molding/setting")
    fun settingDetail(@Query("wo_id") warehouseId: Int): Observable<BaseResponse<SettingDetail>>

    @GET("molding/jenis-proses")
    fun jenisProses(): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("material/pengisian")
    fun getPengisianList(
        @Query("page") page: Int,
        @Query("machine_id") machine_id: Long? = null,
        @Query("material_id") material_d: Int? = null,
        @Query("start_from") start_from: String? = null,
        @Query("end_at") end_at: String? = null,
    ): Observable<BaseResponse<MutableList<PengisianListData>>>

    @GET("molding/material")
    fun material(): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("master/machine")
    fun getMachine(): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("serah-terima/wo")
    fun getWoSerahterima(): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("serah-terima/detail")
    fun getWoSerahterimaDetail(
        @Query("id") production_id: String,
        @Query("type") type: String,
    ): Observable<BaseResponse<SerahterimaData>>

    @POST("serah-terima/terima")
    fun terimaAct(@Body request: HashMap<String, Any>): Observable<BaseResponse<Any>>

    @GET("ipqc/{id}/data")
    fun getIpQcDetail(
        @Path("id") id: Long

    ): Observable<BaseResponse<IpQcData>>

    @GET("material/{id}/det")
    fun getIpMaterialList(
        @Path("id") id: Long

    ): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("material/machine")
    fun getMachineList(

    ): Observable<BaseResponse<MutableList<IdNameData>>>

    @POST("ipqc/{id}/submit")
    fun submitIpQc(
        @Path("id") id: Long,
        @Body request: HashMap<String, Any>
    ): Observable<BaseResponse<Any>>

    @GET("serah-terima/seq")
    fun getSeq(
        @Query("wo_id") wo_id: String
    ): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("master/material/grade")
    fun getMaterialGrade(): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("work-order/")
    fun getWO(): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("work-order/detail")
    fun getWoDetail(
        @Query("wo_id") wo_id: String
    ): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("production/detail")
    fun getProductionData(
        @Query("id") wo_id: Int
    ): Observable<BaseResponse<ProductionData>>

    @GET("master/material/remark")
    fun getMaterialGradeRemark(
        @Query("material_id") materialId: String,
        @Query("grade_id") gradeId: String
    ): Observable<BaseResponse<MutableList<IdNameData>>>

    @POST("molding/add")
    fun submit(@Body request: HashMap<String, Any>): Observable<BaseResponse<Any>>

    @POST("production")
    fun submitProduction(@Body request: HashMap<String, Any>): Observable<BaseResponse<ProductionData>>

    @POST("setup/mold")
    fun submitSetupMold(@Body request: HashMap<String, Any>): Observable<BaseResponse<MoldDetail>>

    @GET("setup/mold/detail")
    fun getSetupMolDetail(
        @Query("wo_id") wo_id: String,
        @Query("wo_mold_id") wo_mold_id: Long,
    ): Observable<BaseResponse<MoldDetail>>

    @POST("setup/machine")
    fun submitSetupMachine(@Body request: HashMap<String, Any>): Observable<BaseResponse<Any>>

    @POST("material/pengisian")
    fun submitPengisian(@Body request: HashMap<String, Any>): Observable<BaseResponse<Any>>

    @GET("shifting")
    fun shiftingList(
        @Query("page") page: Int,
        @Query("tahun") tahun: Int,
        @Query("bulan") bulan: Int,
    ): Observable<BaseResponse<MutableList<ShiftingList>>>

    @GET("shifting/tanggal")
    fun getShiftHarian(

        @Query("tanggal") tanggal: String,
    ): Observable<BaseResponse<MutableList<ShiftingDetail>>>

    @POST("shifting/edit")
    fun updateShift(@Body request: HashMap<String, Any>): Observable<BaseResponse<ShiftingDetail>>

    @POST("shifting/generate")
    fun generateShift(@Body request: HashMap<String, Any>): Observable<BaseResponse<MutableList<ShiftingList>>>

}