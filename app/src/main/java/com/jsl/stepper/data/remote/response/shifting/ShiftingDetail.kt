package com.jsl.stepper.data.remote.response.shifting

class ShiftingDetail(
    val id: Int,
    val tanggal: String,
    val shift: Int,
    val jam_masuk: String,
    val jam_keluar: String,
    val total_jam_istirahat: String,
    val hari: String
) {

}