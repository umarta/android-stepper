package com.jsl.stepper.di

import android.content.Context
//import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
//import com.jsl.scanner.data.local.room.AppDatabase
//import com.jsl.scanner.data.local.room.HomeDao
//import com.jsl.scanner.data.local.room.NotifyDao
//import com.jsl.scanner.data.local.room.SearchDao
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object DatabaseModule {

//    @Singleton
//    @Provides
//    fun provideAppDatabase(@ApplicationContext applicationContext: Context): AppDatabase {
//        return Room.databaseBuilder(
//            applicationContext, AppDatabase::class.java, "AutolokaDb")
//            .build()
//    }
//
//    @Singleton
//    @Provides
//    fun provideSearchDao(appDatabase: AppDatabase): SearchDao = appDatabase.searchDao()
//
//    @Singleton
//    @Provides
//    fun provideHomeDao(appDatabase: AppDatabase): HomeDao = appDatabase.homeDao()
//
//    @Singleton
//    @Provides@@
//    fun provideNotifyDao(appDatabase: AppDatabase): NotifyDao = appDatabase.notifyDao()
}