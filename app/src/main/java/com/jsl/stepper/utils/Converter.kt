//package com.jsl.scanner.utils
//
//import androidx.lifecycle.LiveData
//import androidx.room.TypeConverter
//import com.google.common.reflect.TypeToken
//import com.google.gson.Gson
//import com.jsl.scanner.data.local.entity.EventEntity
//import com.jsl.scanner.data.remote.response.event.EventData
//
//object Converter {
//    @TypeConverter
//    fun fromEvent(value: EventData): String {
//        val gson = Gson()
//        val type = object : TypeToken<EventData>() {}.type
//        return gson.toJson(value, type)
//    }
//
//    @TypeConverter
//    fun toEvent(value: EventEntity): LiveData<EventData> {
//        val gson = Gson()
//        val type = object : TypeToken<EventData>() {}.type
//        return gson.fromJson(value.eventData, type)
//    }
//}