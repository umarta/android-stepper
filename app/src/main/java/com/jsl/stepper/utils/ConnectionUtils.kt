package com.jsl.stepper.utils

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.jsl.stepper.R

object ConnectionUtils {
    private fun isNetworkAvailable(context: Context?): Boolean {
        val connectivityManager = context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val nw      = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                //for other device how are able to connect with Ethernet
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                //for check internet over Bluetooth
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
                else -> false
            }
        } else {
            val nwInfo = connectivityManager.activeNetworkInfo ?: return false
            return nwInfo.isConnected
        }
    }

    fun onRetryConnection(activity: Activity?) {
        if (!isNetworkAvailable(activity)) {
            setSnacbar(activity)
        }
    }

    private fun setSnacbar(activity: Activity?) {
        val snackbar = Snackbar.make(activity?.window!!.decorView.rootView, "No Connection Internet", Snackbar.LENGTH_INDEFINITE)
            .setAction("Try Again") { onRetryConnection(activity) }
        val text = snackbar.view
        val textView = text.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
        text.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.black_tranparent))
        textView.setTextColor(Color.WHITE)
        snackbar.show()

    }
}