package com.jsl.stepper.utils

object Constants {
    const val APPS = "AutoLoka"
    const val TIMER_INTERVAL = 1000

    const val DATA_EXTRA = "data_extra"
    const val MOVE_TYPE = "data_extra"
    const val TRANSACTION_ID = "scan_id"

    const val WAREHOUSE_ID = "warehouse_id"
    const val WAREHOUSE_NAME = "warehouse_name"


    const val DATA_EXTRA2 = "data_extra2"
    const val DATA_EXTRA3 = "data_extra3"
    const val DATA_EXTRA4 = "data_extra4"
    const val DATA_EXTRA5 = "data_extra5"

    const val HTTPS = "https://"
    const val HTTP = "http://"

    const val PREF_TOKEN = "pref_token"
    const val PREF_TOKEN_FCM = "pref_token_fcm"
    const val PREF_ID = "pref_id"
    const val PREF_EMAIL = "pref_email"
    const val PREF_PHONE_NUMBER = "pref_phone_number"
    const val PREF_USERNAME = "pref_username"
    const val PREF_NAME = "pref_name"
    const val PREF_AVATAR = "pref_avatar"
    const val PREF_IS_LOGIN = "pref_is_login"

    const val DATA_BASE64 = " data:image/png;base64,"
    const val DATA_IMAGE_REAL = "data_image_real"
    const val DATA_IMAGE_CROP = "data_image_crop"
    const val DATA_IMAGE_POSITION = "data_image_position"

    //query_by
    const val BRAND = "brand"
    const val PRODUCT = "product"
    //reference
    const val CATEGORY = "category"
    const val NEW_ARRIVAL = "new_arrival"
    const val DISCOUNT = "discount"
    const val CHOSEN_ACCESSORIES = "chosen_accessories"
    const val MEREK = "merek"
    const val ALL = "all"

    const val OTP_FORGOT_PASS = "forgot_password"
    const val OTP_REGISTER = "register"

    const val NOTIFY_DEV = "devDevice"
    const val NOTIFY_PROD = "allDevice"
    const val NOTIFY_HOME = "0"
    const val NOTIFY_HOME2 = "1"
    const val NOTIFY_ORDER_DETAIL = "2"
    const val NOTIFY_BANNER_DETAIL = "3"
    const val NOTIFY_PRODUCT_DETAIL = "4"
    const val NOTIFY_CATEGORY_DETAIL = "5"
    const val NOTIFY_NEWS_DETAIL = "6"
    const val NOTIFY_VOUCHER_DETAIL = "7"

    const val MOVE_WAREHOUSE_TO_ID =  "pref_warehouse_to_id"
    const val MOVE_WAREHOUSE_TO =  "pref_warehouse_to"
    const val MOVE_WAREHOUSE_FROM_ID =  "pref_warehouse_from_id"
    const val MOVE_WAREHOUSE_FROM =  "pref_warehouse_from"
    const val MOVE_LOCATOR_FROM_ID =  "pref_locator_from_id"
    const val MOVE_LOCATOR_FROM =  "pref_locator_from"
    const val MOVE_LOCATOR_TO_ID =  "pref_locator_to_id"
    const val MOVE_LOCATOR_TO =  "pref_locator_to"
    const val MOVE_SCAN_ID =  "pref_move_scan_id"

    const val WO_ID =  "pref_move_scan_id"

}