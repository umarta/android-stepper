package com.jsl.stepper.utils

class BaseApp(private val listener: Listener) {
    fun set(){
        listener.getIntentData()
        listener.setOnClick()
        listener.setAdapter()
        listener.loadData()
        listener.setContent()
    }
    interface Listener {
        fun getIntentData()
        fun setOnClick()
        fun setAdapter()
        fun loadData()
        fun setContent()
    }

}