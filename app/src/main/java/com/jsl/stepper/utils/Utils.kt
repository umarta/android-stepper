package com.jsl.stepper.utils

//import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder
//import com.facebook.imagepipeline.request.ImageRequestBuilder
//import com.stfalcon.frescoimageviewer.ImageViewer
//import com.jsl.scanner.ui.banner.BannerActivity
//import com.jsl.scanner.ui.main.MainActivity
//import com.jsl.scanner.ui.order.orderdetail.OrderDetailActivity
//import com.jsl.scanner.ui.product.productdetail.ProductDetailActivity
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.text.InputType
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.util.concurrent.TimeUnit

object Utils {
    fun setGlide(context: Context, urlImage: String?, imageView: ImageView, placeholder: Int) {
        if (!urlImage.isNullOrEmpty()) {
            Glide.with(context)
                .asBitmap()
                .load(urlImage)
                .disallowHardwareConfig()
                .override(Resources.getSystem().getDisplayMetrics().widthPixels)
                .centerCrop().fitCenter()
                .dontTransform().dontAnimate()
                .placeholder(placeholder)
                .timeout(40 * 60 * 1000)
                .into(imageView)
        }
    }

//    fun imageOverLay(context: Context, imageFirst: String?, position: Int?, listImage: MutableList<String>) {
//        if (imageFirst.isNullOrEmpty()) {
//            Toast.makeText(context, "Gambar Tidak Ada", Toast.LENGTH_SHORT).show()
//        } else {
//            val imageOverlayView: ImageOverlayView? = null
//            val imageRequestBuilder: ImageRequestBuilder? = null
//            ImageRequestBuilder.newBuilderWithSource(Uri.parse(imageFirst)).isProgressiveRenderingEnabled = true
//            val hierarchyBuilder: GenericDraweeHierarchyBuilder = GenericDraweeHierarchyBuilder.newInstance(context.resources)
//                    .setFailureImage(R.drawable.ic_image_black)
//                    .setProgressBarImage(R.drawable.ic_image_black)
//                    .setPlaceholderImage(R.drawable.ic_image_black)
//            ImageViewer.Builder<Any?>(context, listImage as List<String>)
//                .setStartPosition(position!!)
//                .hideStatusBar(true)
//                .allowZooming(true)
//                .allowSwipeToDismiss(true)
//                .setOverlayView(imageOverlayView)
//                .setCustomImageRequestBuilder(imageRequestBuilder)
//                .setCustomDraweeHierarchyBuilder(hierarchyBuilder)
//                .show()
//        }
//    }

//    fun imageOverLay1(context: Context, urlImage: String?) {
//        if (urlImage.isNullOrEmpty()) {
//            Toast.makeText(context, "Gambar Tidak Ada", Toast.LENGTH_SHORT).show()
//        } else {
//            val imageOverlayView: ImageOverlayView? = null
//            val imageRequestBuilder: ImageRequestBuilder? = null
//            ImageRequestBuilder.newBuilderWithSource(Uri.parse(urlImage)).isProgressiveRenderingEnabled = true
//            val hierarchyBuilder: GenericDraweeHierarchyBuilder = GenericDraweeHierarchyBuilder.newInstance(context.resources)
//                    .setFailureImage(R.drawable.ic_image_black)
//                    .setProgressBarImage(R.drawable.ic_image_black)
//                    .setPlaceholderImage(R.drawable.ic_image_black)
//            val image = arrayOf<String?>(urlImage)
//            ImageViewer.Builder<Any?>(context, image)
//                .setStartPosition(0)
//                .hideStatusBar(true)
//                .allowZooming(true)
//                .allowSwipeToDismiss(true)
//                .setOverlayView(imageOverlayView)
//                .setCustomImageRequestBuilder(imageRequestBuilder)
//                .setCustomDraweeHierarchyBuilder(hierarchyBuilder)
//                .show()
//        }
//    }

    fun hideSoftKeyboard(act: Activity) {
        val imm = act.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = act.currentFocus
        if (view == null) {
            view = View(act)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


    fun showSoftKeyboard(view: View, act: Activity) {
        view.requestFocus()
        view.postDelayed({
            val inputMethodManager =
                act.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        }, 400)
    }

    fun formatOtpNumber(_number: String): String {
        var regex = if (_number.length <= 9) "(\\d{3})(\\d{3})(\\d+)"
        else "(\\d{3})(\\d{4})(\\d+)"
        return "+62" + _number.replaceFirst(regex.toRegex(), "$1-$2-$3")
    }

    fun getSizeFile(activity: Activity, uri: Uri?): Long {
        var fileSizeInBytes = 0L
        try {
            val cr = activity.contentResolver
            val `is` = cr.openInputStream(uri!!)
            fileSizeInBytes = `is`!!.available().toLong()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        val fileSizeInKB = fileSizeInBytes / 1024
        //  Convert the KB to MegaBytes (1 MB = 1024 KBytes)
        return fileSizeInKB / 1024
    }

    fun convertBase64(context: Context, mF: String?): String? {
        return try {
            val options = BitmapFactory.Options()
            val ist =
                context.contentResolver.openInputStream(Uri.parse(mF))
            var bm = BitmapFactory.decodeStream(ist, null, options)
            val byteArrayOutputStream =
                ByteArrayOutputStream()
            bm!!.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            Base64.encodeToString(byteArray, Base64.NO_WRAP)
        } catch (ex: Exception) {
            ex.message?.let { Log.e("err", it) }
            null
        }
    }

    fun openLink(link: String, context: Context) {
        var link = link
        link = link.replace("www.", "").replace(" ", "").replace("\n", "")
        if (!link.startsWith("http://") && !link.startsWith("https://")) {
            link = "http://$link"
        }
        val i = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        if (i.resolveActivity(context.packageManager) != null) {
            context.startActivity(i)
        }
    }

//    fun intentNotify(context: Context, type: String, id: String): Intent?{
//        val intent: Intent?
//        return when (type) {
//            Constants.NOTIFY_HOME -> {
//                intent = Intent(context, MainActivity::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or
//                            Intent.FLAG_ACTIVITY_CLEAR_TASK or
//                            Intent.FLAG_ACTIVITY_NEW_TASK)
//            }
//            Constants.NOTIFY_HOME2 -> {
//                intent = Intent(context, MainActivity::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or
//                            Intent.FLAG_ACTIVITY_CLEAR_TASK or
//                            Intent.FLAG_ACTIVITY_NEW_TASK)
//            }
//            Constants.NOTIFY_ORDER_DETAIL -> {
//                intent = Intent(context, OrderDetailActivity::class.java)
//                intent.putExtra(Constants.DATA_EXTRA, id)
//            }
//            Constants.NOTIFY_BANNER_DETAIL -> {
//                intent = Intent(context, BannerActivity::class.java)
//                intent.putExtra(Constants.DATA_EXTRA, id)
//            }
//            Constants.NOTIFY_PRODUCT_DETAIL -> {
//                intent = Intent(context, ProductDetailActivity::class.java)
//                intent.putExtra(Constants.DATA_EXTRA, id)
//            }
////            Constants.NOTIFY_CATEGORY_DETAIL -> {
////                intent = Intent(context, ProductDetailActivity::class.java)
////                intent.putExtra(Constants.DATA_EXTRA, id)
////            }
////            Constants.NOTIFY_NEWS_DETAIL -> {
////                intent = Intent(context, ProductDetailActivity::class.java)
////                intent.putExtra(Constants.DATA_EXTRA, id)
////            }
////            Constants.NOTIFY_VOUCHER_DETAIL -> {
////                intent = Intent(context, ProductDetailActivity::class.java)
////                intent.putExtra(Constants.DATA_EXTRA, id)
////            }
//            else -> { intent = Intent(context, MainActivity::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or
//                        Intent.FLAG_ACTIVITY_CLEAR_TASK or
//                        Intent.FLAG_ACTIVITY_NEW_TASK)
//            }
//        }
//    }


    fun disableEditText(editText: EditText) {
        editText.isFocusable = false
        editText.isFocusableInTouchMode = false
        editText.isCursorVisible = false
        editText.keyListener = null
        editText.inputType = InputType.TYPE_NULL
        editText.setBackgroundColor(Color.TRANSPARENT)
    }
    fun getFormattedStopWatch(ms: Long): String {
        var milliseconds = ms
        val hours = TimeUnit.MILLISECONDS.toHours(milliseconds)
        milliseconds -= TimeUnit.HOURS.toMillis(hours)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds)
        milliseconds -= TimeUnit.MINUTES.toMillis(minutes)
        val seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds)

        return "${if (hours < 10) "0" else ""}$hours:" +
                "${if (minutes < 10) "0" else ""}$minutes:" +
                "${if (seconds < 10) "0" else ""}$seconds"
    }

}