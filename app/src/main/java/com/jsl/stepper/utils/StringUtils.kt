package com.jsl.stepper.utils

import android.content.Context
import com.jsl.stepper.R
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

object StringUtils {
    fun toRp(context: Context, price: String?): String? {
        return if (!price.isNullOrEmpty()){
            val rupiahFormat = NumberFormat.getInstance(Locale.CANADA)
            (context.getString(R.string.rupiah, rupiahFormat.format(price.toDouble()).replace(",", ".")))
        } else ""
    }

    fun toDate(date: Long): String? {
        val sDate = date.toString()
        val fmt = SimpleDateFormat("dd MMMM yyyy, HH:mm")
        return fmt.format(Date(if (sDate.length == 10) date * 1000 else date))
    }
}