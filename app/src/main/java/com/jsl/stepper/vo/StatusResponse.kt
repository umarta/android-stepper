package com.jsl.stepper.vo

enum class StatusResponse {
    SUCCESS,
    EMPTY,
    ERROR
}
