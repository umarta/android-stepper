package com.jsl.stepper.vo

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
