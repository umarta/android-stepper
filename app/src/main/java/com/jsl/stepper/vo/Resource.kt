package com.jsl.stepper.vo
import com.jsl.stepper.vo.Status.SUCCESS
import com.jsl.stepper.vo.Status.ERROR
import com.jsl.stepper.vo.Status.LOADING


data class Resource<T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> = Resource(SUCCESS, data, null)

        fun <T> error(data: T?, msg: String?): Resource<T> = Resource(ERROR, data, msg)

        fun <T> loading(data: T?): Resource<T> = Resource(LOADING, data, null)
    }
}
