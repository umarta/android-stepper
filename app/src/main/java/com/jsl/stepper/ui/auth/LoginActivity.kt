package com.jsl.stepper.ui.auth

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.user.UserManager
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.utils.hide
import com.jsl.stepper.utils.show
import com.jsl.stepper.utils.toast
import com.jsl.stepper.vo.HttpCode
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

@AndroidEntryPoint
class LoginActivity : AppCompatActivity(), BaseApp.Listener {

    @Inject
    lateinit var userManager: UserManager
    private val viewModel: AuthenticationViewModel by viewModels()
    private lateinit var wh: IdNameData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (userManager.isLogin) {
            startActivity(Intent(this, HomeActivity::class.java))
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_login)
//        tv_signIn.setOnClickListener {
//            validation()
//        }

        BaseApp(this).set()
    }


    private fun validation() {
        val username = input_username.text.toString().trim()
        val password = input_password.text.toString()

        if (username.isEmpty()) {
            input_username.error = "Username Empty"
            return
        }
        if (password.isEmpty()) {
            input_password.error = "Password Empty"
            return
        }
//        toast("ass")

        login(username, password)
    }

    private fun login(username: String, password: String) {
//        Utils.hideSoftKeyboard(this)
        progress_bar.show()
        viewModel.login(username, password).observe(this, Observer {
            if (it.code == HttpCode.SUCCESS) {
                userManager.loginSaveUserData(it.data)
                finishAffinity()
                progress_bar.hide()
                startActivity(Intent(this, HomeActivity::class.java))
            } else{
                progress_bar.hide()
                toast(it.message?.get(0))
            }
        })
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        tv_signIn.setOnClickListener {
            validation()
        }
    }

    override fun setAdapter() {
    }

    override fun setContent() {
    }

    override fun loadData() {
    }

}