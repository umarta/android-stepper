package com.jsl.stepper.ui.stepper.stepperNav

interface StepperNavListener {
    fun onStepChanged(step: Int)

    /**
     * Callback for when the stepper has reached the end of the steps.
     */
    fun onCompleted()

}