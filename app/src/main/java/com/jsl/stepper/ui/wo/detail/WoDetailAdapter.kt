package com.jsl.stepper.ui.wo.detail

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.local.WoDetailObject
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.ui.injection.InjectionActivity
import com.jsl.stepper.ui.ipqc.IpQcActivity
import com.jsl.stepper.ui.production.ProductionReportActivity
import com.jsl.stepper.ui.setup.mesin.SetupMesinActivity
import com.jsl.stepper.ui.setup.mold.SetupMoldActivity
import com.jsl.stepper.utils.show
import kotlinx.android.synthetic.main.view_wo_detail_new.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.Serializable

class WoDetailAdapter(
    private val activity: Activity,
    private var data: MutableList<WoDetailData>,
    val listener: WoDetailAdapter.Listener,
    private var wo_id: String,
    private var jenis: String? = null


) : RecyclerView.Adapter<WoDetailAdapter.Holder>() {
    fun setItems(data: MutableList<WoDetailData>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<WoDetailData>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WoDetailAdapter.Holder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.view_wo_detail_new, parent, false)
        return Holder(view)

    }

    override fun onBindViewHolder(holder: WoDetailAdapter.Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    override fun getItemCount(): Int = data.size

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n", "ResourceAsColor")
        @ExperimentalCoroutinesApi
        fun bind(mData: WoDetailData, position: Int, listener: WoDetailAdapter.Listener) {
            with(itemView) {
                tv_ranking.text = mData.sequenceno
                tv_molding.text = mData.code
                tv_name.text = mData.product_name
                if (mData.is_created == true) {
                    wo_detail_card.setBackgroundResource(R.drawable.wo_list_bg)
                }
                if (wo_id == "molding" || wo_id == "machine") {
                    ll_no_mesin.show()
                    tv_no_mesin.text = mData.machine
                    tv_no_wo.text = mData.no_wo
                    ll_waktu_setup_mold.show()
                    tv_waktu_setup_mold.text = mData.duration.toString()
                    ll_kode_part.show()
                    divider_part.show()
                    tv_kode_part.text = mData.kode_part

                }


                if (wo_id == "production" || wo_id == "ipqc") {
                    ll_qty_wo.show()
                    ll_qty_output.show()
                    ll_qty_sisa.show()
                    v_qty1.show()
                    v_qty2.show()
                    tv_qty_wo.text = mData.qty_wo
                    tv_qty_output.text = mData.qty_output
                    tv_qty_sisa.text = mData.qty_sisa
                    tv_no_wo.text = mData.no_wo
                }
                if(wo_id == "production"){
                    ll_status.show()
                    tv_status.text = mData.last_remark
                }
                if(wo_id == "ipqc"){
                    ll_ipqc.show()
                    v_ipqc.show()
                    tv_ipqc.text = mData.ip_qc
                }

                if (wo_id == "molding") {
                    tv_waktu_setup_mold_title.text = "Waktu Setup Mold"
                    v_mold1.show()
                }
                if (wo_id == "machine") {
//                    v_machine1.show()
                    v_machine2.show()
                    tv_waktu_setup_mold_title.text = "Waktu Setup Mesin"
                }
//                if(wo_id == "machine"){
//                    tv_waktu_setup_mold_title.text = "Waktu Setup Mesin"
//                }


                if (mData.proses == 0) {

                }
                itemView.setOnClickListener {
                    var i = Intent()

                    if (mData.proses == 0 || jenis == "first") {
                        i = Intent(activity, SetupMoldActivity::class.java)

//                        val i = Intent(activity, InjectionActivity::class.java)
                    }
                    if (mData.proses == 1) {
                        i = Intent(activity, SetupMesinActivity::class.java)
                    }
//                    if (mData.proses == 3 || mData.proses == 4) {
//                        i = Intent(activity, ProductionReportActivity::class.java)
//                        i.putExtra("mapping_id", mData.mapping_id)
//                    }

                    if (wo_id == "production" && mData.proses!! >= 2) {
                        i = Intent(activity, ProductionReportActivity::class.java)
                        i.putExtra("mapping_id", mData.mapping_id)
                    }

                    if (wo_id == "ipqc" && mData.proses!! >= 2) {
                        i = Intent(activity, IpQcActivity::class.java)
                        i.putExtra("mapping_id", mData.mapping_id)
                        i.putExtra("l_molding_id", mData.id.toString())
                    }

                    if (wo_id == "molding" && mData.proses == 2) {
                        i = Intent(activity, SetupMesinActivity::class.java)
                        i.putExtra("mapping_id", mData.mapping_id)
                    }
                    if (wo_id == "machine" && mData.proses == 2) {
                        i = Intent(activity, InjectionActivity::class.java)
                        i.putExtra("mapping_id", mData.mapping_id)
                    }

                    val woDetailObject = WoDetailObject(
                        id = mData.id,
                        molding_id = mData.molding_id,
                        sequenceno = mData.sequenceno,
                        code = mData.code,
                        name = mData.name,
                        cavity = mData.cavity,
                        tonase = mData.tonase,
                        size = mData.size,
                        description = mData.description,
                        colour = mData.colour,
                        category = mData.category,
                        merek = mData.merek,
                        brand = mData.brand,
                        sub_brand = mData.sub_brand,
                        status = mData.status,
                        location = mData.location,
                        machine = mData.machine,
                        machine_id = mData.machine_id,
                        cycle_time = mData.cycle_time,
                        material = mData.material,
                        is_created = mData.is_created,
                        product_name = mData.product_name,
                        proses = mData.proses,
                        duration = mData.duration,
                        no_wo = mData.no_wo,
                        mapping_id = mData.mapping_id,
                        kode_part = mData.kode_part,
                        last_remark = mData.last_remark,
                        ip_qc = mData.ip_qc,
                    )

                    i.putExtra("object", woDetailObject as Serializable)
                    i.putExtra("wo_id", wo_id)
                    i.putExtra("first_load", "1")
                    val result = if (mData.is_created == true) "yes" else "no"

                    i.putExtra("created_check", result)

                    activity.startActivity(i)
                    activity.overridePendingTransition(R.xml.anim_fade_in, R.xml.anim_fade_out)

                }
            }
        }
    }

    interface Listener {
        fun onLoadWo(data: WoDetailData)
    }


}