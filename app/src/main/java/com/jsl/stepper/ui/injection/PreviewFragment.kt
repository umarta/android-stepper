package com.jsl.stepper.ui.injection

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.model.MoldSetting
import com.jsl.stepper.ui.stepper.InjectionSettingViewModel
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.utils.hide
import com.jsl.stepper.utils.show
import com.jsl.stepper.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_mold_setting.*
import kotlinx.android.synthetic.main.fragment_preview.*
import kotlinx.android.synthetic.main.fragment_preview.fab_next
import kotlinx.android.synthetic.main.fragment_step1.*

@AndroidEntryPoint
class PreviewFragment : Fragment(), BaseApp.Listener {
    private lateinit var viewModel: InjectionSettingViewModel
    lateinit var woDetailData: WoDetailData
    lateinit var moldSetting: MoldSetting
    private lateinit var grade: IdNameData
    private lateinit var materialDdl: IdNameData
    private lateinit var warnaDdl: IdNameData
    private lateinit var jenisProsesDdl: IdNameData
    private lateinit var gradeRemark: IdNameData
    var isCreated: Boolean = false
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(InjectionSettingViewModel::class.java)
        BaseApp(this).set()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_preview, container, false)
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        fab_next.setOnClickListener {

            val kode_part = tv_kode_part_value.text.toString();
            val no_mesin = tv_no_mesin_value.text.toString();
            val tonase = tv_tonase_value.text.toString();
            val jenis_proses = sp_jenis_proses.text.toString()
            val warna = sp_warna.text.toString()
            val material = sp_material.text.toString()
            val berat_part = input_berat_part.text.toString()
            val berat_runner = input_berat_runner.text.toString()
            val cycle_time = input_cycle_time.text.toString()

            if (!this::warnaDdl.isInitialized) {
                toast("Warna harus dipilih")
                return@setOnClickListener
            }
            if (!this::jenisProsesDdl.isInitialized) {
                toast("Jenis Proses harus dipilih")
                return@setOnClickListener
            }
            if (!this::materialDdl.isInitialized) {
                toast("Material harus dipilih")
                return@setOnClickListener
            }
            if (!this::grade.isInitialized) {
                toast("Grade harus dipilih")
                return@setOnClickListener
            }
            if (!this::gradeRemark.isInitialized) {
                toast("Remark harus dipilih")
                return@setOnClickListener
            }


            if (jenis_proses!!.isEmpty()) {
                sp_jenis_proses.error = "Jenis Proses tidak boleh kosong"
                return@setOnClickListener
            }

            if (warna!!.isEmpty()) {
                sp_warna.error = "Warna tidak boleh kosong"
                return@setOnClickListener
            }
            if (material!!.isEmpty()) {
                sp_material.error = "Material tidak boleh kosong"
                return@setOnClickListener
            }
            if (berat_part!!.isEmpty()) {
                input_berat_part.error = "Berat part tidak boleh kosong"
                return@setOnClickListener
            }

            if (berat_runner!!.isEmpty()) {
                input_berat_runner.error = "Berat runner tidak boleh kosong"
                return@setOnClickListener
            }
            if (cycle_time!!.isEmpty()) {
                input_cycle_time.error = "Cycle time tidak boleh kosong"
                return@setOnClickListener
            }

            if (!isCreated) {
                val previewData = WoDetailData(
                    id = woDetailData.id,
                    code = kode_part,
                    machine = no_mesin,
                    tonase = tonase,
                    jenis_proses = jenisProsesDdl.id,
                    cycle_time = cycle_time,
                    colour = warnaDdl.id,
                    berat_part = berat_part,
                    berat_runner = berat_runner,
                    material = materialDdl.id,
                    molding_id = woDetailData.molding_id,
                    sequenceno = woDetailData.sequenceno,
                    cavity = woDetailData.cavity,
                    name = woDetailData.name,
                    size = woDetailData.size,
                    description = woDetailData.description,
                    category = woDetailData.category,
                    merek = woDetailData.merek,
                    brand = woDetailData.brand,
                    sub_brand = woDetailData.sub_brand,
                    status = woDetailData.status,
                    location = woDetailData.location,
                    womolding_id = woDetailData.womolding_id,
                    product_name = woDetailData.product_name,
                    shot_weight = tv_shot_weight_value.text.toString(),
                    grade_material = grade.id,
                    grade_material_remark = gradeRemark.id,
                    mapping_id = woDetailData.mapping_id,
                    kode_part = woDetailData.kode_part
                )
                viewModel.setWoDetail(previewData)
            }
            viewModel.setPage(InjectionActivity.CLAMPING_UNIT_SETTING)


        }
    }

    override fun setAdapter() {
    }

    @SuppressLint("SetTextI18n")
    override fun setContent() {
        viewModel.woDetail.observe(this, Observer {
            woDetailData = it
            tv_kode_mold.text = it.code
            tv_kode_part_value.text = it.kode_part
            tv_no_mesin_value.text = it.machine
            tv_product_name.text = it.product_name
            tv_tonase_value.text = it.tonase
            tv_no_mesin_value.text.toString();
            Log.d("maping", woDetailData.mapping_id.toString())
            if (isCreated) {

                sp_jenis_proses.isEnabled = false
                sp_jenis_proses.setText("Jenis Proses : " + woDetailData.jenis_proses)

                disableEditText(input_berat_part)
                input_berat_part.setText(woDetailData.berat_part)

                disableEditText(input_berat_runner)
                input_berat_runner.setText(woDetailData.berat_runner)

                disableEditText(input_cycle_time)
                input_cycle_time.setText(woDetailData.cycle_time)
                tv_shot_weight_value.text = it.shot_weight

                sp_warna.isEnabled = false
                sp_warna.setText("Warna : " + woDetailData.colour)

                sp_material.isEnabled = false
                sp_material.setText("Material : " + woDetailData.material)

            }
        })
        viewModel.isCreated.observe(this, {
            isCreated = it
            getData()
        })
        sp_grade.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                grade = item
                materialGradeRemark()
            }
        )
        sp_warna.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                warnaDdl = item
            }
        )
        sp_jenis_proses.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                jenisProsesDdl = item
            }
        )
        sp_material.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                materialDdl = item
            }
        )



        sp_material.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                materialDdl = item
            }
        )


        sp_remark.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                gradeRemark = item
            }
        )

        input_berat_runner.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                val berat_part = input_berat_part.text.toString().toInt()
                val berat_runner = s.toString().toInt()

                tv_shot_weight_value.text = (berat_part + berat_runner).toString()
            }
        })

    }

    private fun disableEditText(editText: EditText) {
        editText.isFocusable = false
        editText.isFocusableInTouchMode = false
        editText.isCursorVisible = false
        editText.keyListener = null
        editText.inputType = InputType.TYPE_NULL
        editText.setBackgroundColor(Color.TRANSPARENT)
    }

    @SuppressLint("SetTextI18n")
    override fun loadData() {
        materialGrade()

    }

    @SuppressLint("SetTextI18n")
    private fun getData() {
        if (!isCreated) {
            viewModel.colour().observe(this, Observer {
                if (it.success!!) {
                    val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this.activity, it.data)
                    sp_warna.setAdapter(spinnerAdapter)
                    sp_warna.setText("Warna")
                    if(woDetailData.colour != null){
                        sp_warna.setText(woDetailData.colour)
                    }
                }
            })
            viewModel.material().observe(this, Observer {
                if (it.success!!) {
                    val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this.activity, it.data)
                    sp_material.setAdapter(spinnerAdapter)
                    sp_material.setText("Material")
                    if(woDetailData.material !== null){

                        sp_material.setText(woDetailData.material)
                    }

                }
            })
            viewModel.jenisProses().observe(this, Observer {
                if (it.success!!) {
                    val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this.activity, it.data)
                    sp_jenis_proses.setAdapter(spinnerAdapter)
                    sp_jenis_proses.setText("Jenis Proses")
                    if(woDetailData.jenis_proses !== null){

                        sp_jenis_proses.setText(woDetailData.jenis_proses)
                    }

                }
            })

        } else {
            sp_warna.setText("Warna : " + woDetailData.colour)
            sp_material.setText("Material : " + woDetailData.material)
            sp_jenis_proses.setText("Jenis Proses : " + woDetailData.jenis_proses)

        }
    }
    private fun materialGrade() {
        progress_bar.show()
        viewModel.getMaterialGrade().observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this.activity, it.data)
                sp_grade.setAdapter(spinnerAdapter)
            }
            progress_bar.hide()
        })
    }

    private fun materialGradeRemark() {
        progress_bar.show()
        viewModel.getMaterialGradeRemark(materialDdl.id,grade.id).observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this.activity, it.data)
                sp_remark.setAdapter(spinnerAdapter)
            }
            progress_bar.hide()
        })
    }

}