package com.jsl.stepper.ui.wo

import android.content.Intent
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.wo.WoData
import com.jsl.stepper.data.user.UserManager
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.utils.hide
import com.jsl.stepper.utils.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_wo_list.*
import javax.inject.Inject

@AndroidEntryPoint
class WoListActivity : AppCompatActivity(), BaseApp.Listener, WoListAdapter.Listener {
    @Inject
    lateinit var userManager: UserManager

    private val viewModel: WoLisViewModel by viewModels()
    private lateinit var woListAdapter: WoListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wo_list)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        iv_back.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))

        }
        this?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                val intent = Intent(this@WoListActivity, HomeActivity::class.java)
                startActivity(intent)
            }
        })

    }

    override fun setAdapter() {
        woListAdapter = WoListAdapter(this, ArrayList(), this)
        rv_wo_list.adapter = woListAdapter
        rv_wo_list.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

    }

    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) progress_bar.show()
            else progress_bar.hide()
        })
    }

    override fun loadData() {
        viewModel.list().observe(this, Observer {
            if (it.success!!) {
                woListAdapter.setItems(it.data)
            }
        })
    }

    override fun onLoadWo(data: WoData) {
        finish()
    }
}