package com.jsl.stepper.ui.control

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.jsl.stepper.data.remote.MainRepository
import com.jsl.stepper.data.remote.MainService
import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.production.ProductionControlData

class ProductionControlViewModel @ViewModelInject constructor(mainService: MainService) :
    ViewModel() {
    private val repoMain =
        MainRepository(mainService)
    val loading: LiveData<Boolean> get() = repoMain.loading

    fun getData(page: Int): LiveData<BaseResponse<MutableList<ProductionControlData>>> =
        repoMain.getProductionControlData(page)

}