package com.jsl.stepper.ui.shifting

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.serahterima.SerahTerimaListData
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.utils.hide
import com.jsl.stepper.utils.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_serah_terima_list.iv_back
import kotlinx.android.synthetic.main.activity_serah_terima_list.progress_bar
import kotlinx.android.synthetic.main.activity_serah_terima_list.rv_serahterima
import kotlinx.android.synthetic.main.activity_serah_terima_list.swipeContainer
import kotlinx.android.synthetic.main.activity_shifting.*
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class ShiftingActivity : AppCompatActivity(), BaseApp.Listener, ShiftingAdapter.Listener,
    GenerateShiftDialog.Listener {
    private val viewModel: ShiftingViewModel by viewModels()
    private var next_page = true
    private var loading = false
    private var is_sort = false
    private var page = 1
    private var tahun: Int = 0;
    private var bulan: Int = 0;

    private lateinit var shiftingAdapter: ShiftingAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shifting)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        iv_back.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }
        swipeContainer.setOnRefreshListener {
            page = 1
            shiftingAdapter.clear()
            fetchData()
            swipeContainer.setRefreshing(false)
        }

        swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        );
        iv_add.setOnClickListener {
            val fm: FragmentManager = supportFragmentManager
            val dialog = GenerateShiftDialog("Pilih bulan dan tahun untuk generate shift?", this)
            dialog.show(fm, GenerateShiftDialog::javaClass.name)

        }
        ly_filterSort.setOnClickListener {
            val fm: FragmentManager = supportFragmentManager
            val dialog = GenerateShiftDialog("Sort shift by tahun dan bulan", this, true)
            dialog.show(fm, GenerateShiftDialog::javaClass.name)

        }
    }

    override fun setAdapter() {
        shiftingAdapter = ShiftingAdapter(this, ArrayList(), this)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        rv_serahterima.adapter = shiftingAdapter
        rv_serahterima.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_serahterima.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val countItem = layoutManager.itemCount
                val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                if (!recyclerView.canScrollVertically(1)) {
                    if (next_page) {
                        if (!loading && isLastPosition) {
                            page++
                            loadData()
                        }
                    }
                }
            }

        })
    }

    override fun loadData() {
        fetchData()


    }

    private fun fetchData() {

        if (!is_sort) {
            val c = Calendar.getInstance()

            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)

            tahun = year
            bulan = (month+1)
        }
        viewModel.list(page, tahun, bulan).observe(this, Observer {
            if (it.success!!) {
                next_page = it.header.next_page
                if (page == 1) {
                    shiftingAdapter.setItems(it.data)
                } else {
                    shiftingAdapter.addItems(it.data)
                }
                is_sort = false
            }
        })
    }

    override fun setContent() {

        viewModel.loading.observe(this, Observer {
            if (it) progress_bar.show()
            else progress_bar.hide()
        })
    }

    override fun onLoadData(data: SerahTerimaListData) {
        finish()
    }

    override fun onCloseDialog(tahun_: Int, bulan_: Int) {
        val res = HashMap<String, Any>()
        res["tahun"] = tahun_
        res["bulan"] = bulan_
        viewModel.generateShift(res).observe(this, Observer {
            if (it.success!!) {
                is_sort = true
                tahun = tahun_
                bulan = bulan_
                fetchData()
            }
        })

    }

    override fun onSrot(tahun_: Int, bulan_: Int) {
        tahun = tahun_
        bulan = bulan_
        is_sort = true
        fetchData()
    }

    override fun cancelClose() {
    }
}