package com.jsl.stepper.ui.waste

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jsl.stepper.R
import com.jsl.stepper.utils.BaseApp

class WasteActivity : AppCompatActivity(), BaseApp.Listener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waste)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
    }

    override fun setAdapter() {
    }

    override fun loadData() {
    }

    override fun setContent() {
    }
}