package com.jsl.stepper.ui.setup.mesin

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.jsl.stepper.R
import com.jsl.stepper.data.local.WoDetailObject
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.ui.dialog.CloseDialog
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.utils.Constants.TIMER_INTERVAL
import com.jsl.stepper.utils.Utils.getFormattedStopWatch
import com.jsl.stepper.utils.hide
import com.jsl.stepper.utils.show
import com.jsl.stepper.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_setup_mesin.*
import kotlinx.android.synthetic.main.activity_setup_mesin.kode_mold
import kotlinx.android.synthetic.main.activity_setup_mesin.kode_part
import kotlinx.android.synthetic.main.activity_setup_mesin.no_wo
import kotlinx.android.synthetic.main.activity_setup_mesin.startOrStopTextView
import kotlinx.android.synthetic.main.activity_setup_mesin.textViewStopWatch

@AndroidEntryPoint
class SetupMesinActivity : AppCompatActivity(), BaseApp.Listener, CloseDialog.Listener {
    private val mInterval = TIMER_INTERVAL
    private var mHandler: Handler? = null
    private lateinit var wo_id: String
    private lateinit var woDetailData: WoDetailData

    private var timeInSeconds = 0L
    private var startButtonClicked = false
    private val viewModel: MesinViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup_mesin)
        BaseApp(this).set()
    }

    override fun getIntentData() {
        if (intent.hasExtra("object")) {
            val woDetailData = intent.getSerializableExtra("object") as? WoDetailObject
            val woData =
                WoDetailData(
                    id = woDetailData?.id,
                    sequenceno = woDetailData?.sequenceno,
                    molding_id = woDetailData?.molding_id,
                    code = woDetailData?.code,
                    name = woDetailData?.name,
                    cavity = woDetailData?.cavity,
                    tonase = woDetailData?.tonase,
                    size = woDetailData?.size,
                    description = woDetailData?.description,
                    colour = woDetailData?.colour,
                    category = woDetailData?.category,
                    merek = woDetailData?.merek,
                    brand = woDetailData?.brand,
                    sub_brand = woDetailData?.sub_brand,
                    status = woDetailData?.status,
                    location = woDetailData?.location,
                    machine = woDetailData?.machine,
                    machine_id = woDetailData?.machine_id,
                    jenis_proses = woDetailData?.jenis_proses,
                    cycle_time = woDetailData?.cycle_time,
                    material = woDetailData?.material,
                    womolding_id = woDetailData?.womolding_id,
                    is_created = woDetailData?.is_created,
                    product_name = woDetailData?.product_name,
                    no_wo = woDetailData?.no_wo,
                    kode_part = woDetailData?.kode_part
                )
            viewModel.setWoDetail(woData)
        }

        if (intent.hasExtra("wo_id")) {
            wo_id = intent.getStringExtra("wo_id").toString()
        }
    }

    override fun setOnClick() {
//        resetButton.setOnClickListener {
//            stopTimer()
//            resetTimerView()
//        }
    }

    override fun setAdapter() {
    }

    override fun loadData() {
    }

    override fun setContent() {
        viewModel.woDetail.observe(this, Observer {
            woDetailData = it
            kode_mold.text = woDetailData.code
            kode_part.text = woDetailData.kode_part
            tv_no_mesin.text = woDetailData.machine
            no_wo.text = woDetailData.no_wo
            tv_setup_mesin_title.text = woDetailData.product_name
        })

    }

    private fun resetTimerView() {
        showCloseDialog()

//        timeInSeconds = 0
//        startButtonClicked = false
//        startOrStopTextView?.setBackgroundColor(
//            ContextCompat.getColor(
//                this,
//                R.color.teal_700
//            )
//        )
//        startOrStopTextView?.setText(R.string.start)
//        initStopWatch()

    }

    private fun initStopWatch() {
        textViewStopWatch?.text = getString(R.string.init_stop_watch_value)
    }

    private fun startTimer() {
        mHandler = Handler(Looper.getMainLooper())

        mStatusChecker.run()
    }

    private var mStatusChecker: Runnable = object : Runnable {
        override fun run() {
            try {
                timeInSeconds += 1
                Log.e("timeInSeconds", timeInSeconds.toString())
                updateStopWatchView(timeInSeconds)
            } finally {
                mHandler!!.postDelayed(this, mInterval.toLong())

                // 100% guarantee that this always happens, even if
                // your update method throws an exception
            }
        }
    }

    private fun updateStopWatchView(timeInSeconds: Long) {
        val formattedTime = getFormattedStopWatch((timeInSeconds * 1000))
        Log.e("formattedTime", formattedTime)
        textViewStopWatch?.text = formattedTime
    }

    private fun startTimerView() {

        startOrStopTextView?.setBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.red
            )
        )
        startOrStopTextView?.setText(R.string.finish)
        startButtonClicked = true

    }

    private fun stopTimer() {
        mHandler?.removeCallbacks(mStatusChecker)

    }

    private fun stopTimerView() {
        startOrStopTextView?.setBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.teal_700
            )
        )
        startOrStopTextView?.setText(R.string.resume)
        startButtonClicked = false
    }

    fun startOrStopButtonClicked(v: View) {
        if (!startButtonClicked) {
            startTimer()
            startTimerView()
        } else {
            stopTimer()
            resetTimerView()
        }
    }

    private fun showCloseDialog() {
        val fm: FragmentManager = supportFragmentManager
        val dialog = CloseDialog("Setup mesin ini telah selesai ?", this)
        dialog.show(fm, CloseDialog::javaClass.name)
    }

    private fun submitData() {
        progress_bar.show()
        val res = HashMap<String, Any>()
        res["machine_id"] = woDetailData.machine_id.toString()
        res["wo_id"] = woDetailData.no_wo.toString()
        res["wo_mold_id"] = woDetailData.id.toString()
        res["part_no"] = woDetailData.code.toString()
        res["duration"] = timeInSeconds.toString()
        toast(timeInSeconds.toString())

        viewModel.submit(res).observe(this, Observer {
            progress_bar.hide()
            if (it.success!!) {
                toast(it.message?.first())
                val i = Intent(this, HomeActivity::class.java)
                this.startActivity(i)
            } else {
                toast(it.message?.first())
            }

        })
    }

    override fun onCloseDialog() {
        submitData()
        timeInSeconds = 0
        startButtonClicked = false
        startOrStopTextView?.setBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.teal_700
            )
        )
        startOrStopTextView?.setText(R.string.start)
        initStopWatch()
    }

    override fun cancelClose() {
        startTimer()
        startTimerView()
    }
}