 package com.jsl.stepper.ui.injection

import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jsl.stepper.R
import com.jsl.stepper.model.EjectorSetting
import com.jsl.stepper.ui.stepper.InjectionSettingViewModel
import com.jsl.stepper.utils.BaseApp
import kotlinx.android.synthetic.main.fragment_ejector_setting.*

 class EjectorSettingFragment : Fragment(), BaseApp.Listener {
     private lateinit var viewModel: InjectionSettingViewModel
     private lateinit var ejectorSetting: EjectorSetting
     var isCreated: Boolean = false

     override fun onActivityCreated(savedInstanceState: Bundle?) {
         super.onActivityCreated(savedInstanceState)
         viewModel = ViewModelProvider(requireActivity()).get(InjectionSettingViewModel::class.java)
         BaseApp(this).set()
     }

     override fun onCreateView(
         inflater: LayoutInflater, container: ViewGroup?,
         savedInstanceState: Bundle?
     ): View? {
         return inflater.inflate(R.layout.fragment_ejector_setting, container, false)
     }

     override fun getIntentData() {
     }

     override fun setOnClick() {
         fab_prev.setOnClickListener {
             viewModel.setPage(InjectionActivity.CLAMPING_UNIT_SETTING)
         }
         fab_next.setOnClickListener {
             validation()
         }

     }

     fun validation() {
         val eSeFP1 = input_eSeFP1.text.toString()
         val eSeFP2 = input_eSeFP2.text.toString()
         val eSeFS1 = input_eSeFS1.text.toString()
         val eSeFS2 = input_eSeFS2.text.toString()
         val eSeFSt1 = input_eSeFSt1.text.toString()
         val eSeFSt2 = input_eSeFSt2.text.toString()
         val eSeBP2 = input_eSeBP2.text.toString()
         val eSeBP1 = input_eSeBP1.text.toString()
         val eSeBS2 = input_eSeBS2.text.toString()
         val eSeBS1 = input_eSeBS1.text.toString()
         val eSeBSt2 = input_eSeBSt2.text.toString()
         val eSeBSt1 = input_eSeBSt1.text.toString()

         if (eSeFP1!!.isEmpty()) {
             input_eSeFP1.error = "Ejector Forward Press Press 1 tidak boleh kosong"
             return
         }
         if (eSeFP2!!.isEmpty()) {
             input_eSeFP2.error = "Ejector Forward Press Press 1 tidak boleh kosong"
             return
         }
         if (eSeFS1!!.isEmpty()) {
             input_eSeFS1.error = "Ejector Forward Press Press 1 tidak boleh kosong"
             return
         }
         if (eSeFS2!!.isEmpty()) {
             input_eSeFS2.error = "Ejector Forward Speed Press 1 tidak boleh kosong"
             return
         }
         if (eSeFSt1!!.isEmpty()) {
             input_eSeFSt1.error = "Ejector Forward Stroke Press 1 tidak boleh kosong"
             return
         }
         if (eSeFSt2!!.isEmpty()) {
             input_eSeFSt2.error = "Ejector Forward Stroke Press 1 tidak boleh kosong"
             return
         }
         if (eSeBP2!!.isEmpty()) {
             input_eSeBP2.error = "Ejector Backward Press Press 1 tidak boleh kosong"
             return
         }
         if (eSeBP1!!.isEmpty()) {
             input_eSeBP1.error = "Ejector Backward Press Press 1 tidak boleh kosong"
             return
         }
         if (eSeBS2!!.isEmpty()) {
             input_eSeBS2.error = "Ejector Backward Press Press 1 tidak boleh kosong"
             return
         }
         if (eSeBS1!!.isEmpty()) {
             input_eSeBS1.error = "Ejector Backward Speed Press 1 tidak boleh kosong"
             return
         }
         if (eSeBSt2!!.isEmpty()) {
             input_eSeBSt2.error = "Ejector Backward Stroke Press 1 tidak boleh kosong"
             return
         }
         if (eSeBSt1!!.isEmpty()) {
             input_eSeBSt1.error = "Ejector Backward Stroke Press 1 tidak boleh kosong"
             return
         }

         val efP = EjectorSetting.EjectorForward.Press(eSeFP1.toInt(), eSeFP2.toInt())
         val efS = EjectorSetting.EjectorForward.Speed(eSeFS1.toInt(), eSeFS2.toInt())
         val efSt = EjectorSetting.EjectorForward.Store(eSeFSt1.toInt(), eSeFSt1.toInt())
         val ef = EjectorSetting.EjectorForward(efP, efS, efSt)

         val ebP = EjectorSetting.EjectorBackward.Press(eSeBP1.toInt(), eSeBP2.toInt())
         val ebS = EjectorSetting.EjectorBackward.Speed(eSeBS1.toInt(), eSeBS2.toInt())
         val ebSt = EjectorSetting.EjectorBackward.Store(eSeBSt1.toInt(), eSeBSt2.toInt())
         val eb = EjectorSetting.EjectorBackward(ebP, ebS, ebSt)


         val setting = EjectorSetting(ef, eb)
         viewModel.setEjectorSetting(setting)
         viewModel.setPage(InjectionActivity.INJECTION_UNIT_SETTING)

     }

     override fun setAdapter() {
     }

     override fun setContent() {
         viewModel.ejectorSetting.observe(this, Observer {
             ejectorSetting = it
         })
         viewModel.isCreated.observe(this, {
             isCreated = it
             if(isCreated){
                 setData()
             }
         })


     }

     private fun disableEditText(editText: EditText) {
         editText.isFocusable = false
         editText.isFocusableInTouchMode = false
         editText.isCursorVisible = false
         editText.keyListener = null
         editText.inputType = InputType.TYPE_NULL
         editText.setBackgroundColor(Color.TRANSPARENT)
     }

     private fun setData() {
         input_eSeFP1.setText(ejectorSetting.ejectorForward.press.efp1.toString())
         input_eSeFP2.setText(ejectorSetting.ejectorForward.press.efp2.toString())
         input_eSeFS1.setText(ejectorSetting.ejectorForward.speed.efs1.toString())
         input_eSeFS2.setText(ejectorSetting.ejectorForward.speed.efs2.toString())
         input_eSeFSt1.setText(ejectorSetting.ejectorForward.stroke.efst1.toString())
         input_eSeFSt2.setText(ejectorSetting.ejectorForward.stroke.efst2.toString())
         input_eSeBP2.setText(ejectorSetting.ejectorBackward.press.ebp1.toString())
         input_eSeBP1.setText(ejectorSetting.ejectorBackward.press.ebp2.toString())
         input_eSeBS2.setText(ejectorSetting.ejectorBackward.speed.ebs1.toString())
         input_eSeBS1.setText(ejectorSetting.ejectorBackward.speed.ebs2.toString())
         input_eSeBSt2.setText(ejectorSetting.ejectorBackward.stroke.ebst1.toString())
         input_eSeBSt1.setText(ejectorSetting.ejectorBackward.stroke.ebst2.toString())

         disableEditText(input_eSeFP1)
         disableEditText(input_eSeFP2)
         disableEditText(input_eSeFS1)
         disableEditText(input_eSeFS2)
         disableEditText(input_eSeFSt1)
         disableEditText(input_eSeFSt2)
         disableEditText(input_eSeBP2)
         disableEditText(input_eSeBP1)
         disableEditText(input_eSeBS2)
         disableEditText(input_eSeBS1)
         disableEditText(input_eSeBSt2)
         disableEditText(input_eSeBSt1)

     }

     override fun loadData() {
     }

 }