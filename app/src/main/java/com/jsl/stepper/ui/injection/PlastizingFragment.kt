package com.jsl.stepper.ui.injection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jsl.stepper.R
import com.jsl.stepper.model.Plastizing
import com.jsl.stepper.ui.stepper.InjectionSettingViewModel
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.utils.Utils.disableEditText
import kotlinx.android.synthetic.main.fragment_plastizing.*

class PlastizingFragment : Fragment(), BaseApp.Listener {
    private lateinit var viewModel: InjectionSettingViewModel
    private lateinit var plastizing: Plastizing
    var isCreated: Boolean = false

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(InjectionSettingViewModel::class.java)
        BaseApp(this).set()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_plastizing, container, false)
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        fab_next.setOnClickListener {
            validate()
        }

        fab_prev.setOnClickListener {
            viewModel.setPage(InjectionActivity.INJECTION_UNIT_SETTING)


        }
    }

    private fun validate() {
        val pUcBP1 = input_pUcBP1.text.toString()
        val pUcBP2 = input_pUcBP2.text.toString()
        val pUcBP3 = input_pUcBP3.text.toString()
        val pUcBP4 = input_pUcBP4.text.toString()
        val pUcP1 = input_pUcP1.text.toString()
        val pUcP2 = input_pUcP2.text.toString()
        val pUcP3 = input_pUcP3.text.toString()
        val pUcP4 = input_pUcP4.text.toString()
        val pUcS1 = input_pUcS1.text.toString()
        val pUcS2 = input_pUcS2.text.toString()
        val pUcS3 = input_pUcS3.text.toString()
        val pUcS4 = input_pUcS4.text.toString()
        val pUcSt1 = input_pUcSt1.text.toString()
        val pUcSt2 = input_pUcSt2.text.toString()
        val pUcSt3 = input_pUcSt3.text.toString()
        val pUcSt4 = input_pUcSt4.text.toString()
        val pUsBP1 = input_pUsBP1.text.toString()
        val pUsBS1 = input_pUsBS1.text.toString()
        val pUsBSt1 = input_pUsBSt1.text.toString()
        val pUsBT1 = input_pUsBT1.text.toString()

        if (pUcBP1!!.isEmpty()) {
            input_pUcBP1.error = "Charging Back Press 1 tidak boleh kosong"
            return
        }
        if (pUcBP2!!.isEmpty()) {
            input_pUcBP2.error = "Charging Back Press 2 tidak boleh kosong"
            return
        }
        if (pUcBP3!!.isEmpty()) {
            input_pUcBP3.error = "Charging Back Press 3 tidak boleh kosong"
            return
        }
        if (pUcBP4!!.isEmpty()) {
            input_pUcBP4.error = "Charging Back Press 4 tidak boleh kosong"
            return
        }
        if (pUcP1!!.isEmpty()) {
            input_pUcP1.error = "Charging Press 1 tidak boleh kosong"
            return
        }
        if (pUcP2!!.isEmpty()) {
            input_pUcP2.error = "Charging Press 2 tidak boleh kosong"
            return
        }
        if (pUcP3!!.isEmpty()) {
            input_pUcP3.error = "Charging Press 3 tidak boleh kosong"
            return
        }
        if (pUcP4!!.isEmpty()) {
            input_pUcP4.error = "Charging Press 4 tidak boleh kosong"
            return
        }
        if (pUcS1!!.isEmpty()) {
            input_pUcS1.error = "Charging Speed 1 tidak boleh kosong"
            return
        }
        if (pUcS2!!.isEmpty()) {
            input_pUcS2.error = "Charging Speed 2 tidak boleh kosong"
            return
        }
        if (pUcS3!!.isEmpty()) {
            input_pUcS3.error = "Charging Speed 3 tidak boleh kosong"
            return
        }
        if (pUcS4!!.isEmpty()) {
            input_pUcS4.error = "Charging Speed 4 tidak boleh kosong"
            return
        }
        if (pUcSt1!!.isEmpty()) {
            input_pUcSt1.error = "Charging Stroke 1 tidak boleh kosong"
            return
        }
        if (pUcSt2!!.isEmpty()) {
            input_pUcSt2.error = "Charging Stroke 2 tidak boleh kosong"
            return
        }
        if (pUcSt3!!.isEmpty()) {
            input_pUcSt3.error = "Charging Stroke 3 tidak boleh kosong"
            return
        }
        if (pUcSt4!!.isEmpty()) {
            input_pUcSt4.error = "Charging Stroke 4 tidak boleh kosong"
            return
        }
        if (pUsBP1!!.isEmpty()) {
            input_pUsBP1.error = "Suck Back Press 1 tidak boleh kosong"
            return
        }
        if (pUsBS1!!.isEmpty()) {
            input_pUsBS1.error = "Suck Back Speed 1 tidak boleh kosong"
            return
        }
        if (pUsBSt1!!.isEmpty()) {
            input_pUsBSt1.error = "Suck Back Stroke 1 tidak boleh kosong"
            return
        }
        if (pUsBT1!!.isEmpty()) {
            input_pUsBT1.error = "Suck Back Time 1 tidak boleh kosong"
            return
        }


        val pUcBP = Plastizing.Charging.BackPress(
            pUcBP1.toInt(),
            pUcBP2.toInt(),
            pUcBP3.toInt(),
            pUcBP4.toInt()
        )

        val pUcP = Plastizing.Charging.Press(
            pUcP1.toInt(),
            pUcP2.toInt(),
            pUcP3.toInt(),
            pUcP4.toInt()
        )


        val pUcS = Plastizing.Charging.Speed(
            pUcS1.toInt(),
            pUcS2.toInt(),
            pUcS3.toInt(),
            pUcS4.toInt()
        )
        val pUcSt = Plastizing.Charging.Stroke(
            pUcSt1.toInt(),
            pUcSt2.toInt(),
            pUcSt3.toInt(),
            pUcSt4.toInt()
        )
        val pUc = Plastizing.Charging(pUcBP, pUcP, pUcS, pUcSt)
        val pUsBP = Plastizing.SuckBack.Press(pUsBP1.toInt())
        val pUsBS = Plastizing.SuckBack.Speed(pUsBS1.toInt())
        val pUsBSt = Plastizing.SuckBack.Stroke(pUsBSt1.toInt())
        val pUsBT = Plastizing.SuckBack.CoolTime(pUsBT1.toInt())

        val pUs = Plastizing.SuckBack(pUsBP, pUsBS, pUsBSt, pUsBT)

        val plastizing = Plastizing(pUc, pUs)

        viewModel.setPlastizing(plastizing)
        viewModel.setPage(InjectionActivity.TEMPERATURE_BARREL_SETTING)

    }

    override fun setAdapter() {
    }

    override fun loadData() {
    }

    override fun setContent() {

        viewModel.plastizing.observe(this, Observer {
            plastizing = it
        })
        viewModel.isCreated.observe(this, {
            isCreated = it
            if (isCreated) {
                setData()
            }
        })

    }

    private fun setData() {
        disableEditText(input_pUcBP1)
        disableEditText(input_pUcBP2)
        disableEditText(input_pUcBP3)
        disableEditText(input_pUcBP4)
        disableEditText(input_pUcP1)
        disableEditText(input_pUcP2)
        disableEditText(input_pUcP3)
        disableEditText(input_pUcP4)
        disableEditText(input_pUcS1)
        disableEditText(input_pUcS2)
        disableEditText(input_pUcS3)
        disableEditText(input_pUcS4)
        disableEditText(input_pUcSt1)
        disableEditText(input_pUcSt2)
        disableEditText(input_pUcSt3)
        disableEditText(input_pUcSt4)
        disableEditText(input_pUsBP1)
        disableEditText(input_pUsBS1)
        disableEditText(input_pUsBSt1)
        disableEditText(input_pUsBT1)

        input_pUcBP1.setText(plastizing.charging.backPress.pUcBP1.toString())
        input_pUcBP2.setText(plastizing.charging.backPress.pUcBP2.toString())
        input_pUcBP3.setText(plastizing.charging.backPress.pUcBP3.toString())
        input_pUcBP4.setText(plastizing.charging.backPress.pUcBP4.toString())
        input_pUcP1.setText(plastizing.charging.press.pUcP1.toString())
        input_pUcP2.setText(plastizing.charging.press.pUcP2.toString())
        input_pUcP3.setText(plastizing.charging.press.pUcP3.toString())
        input_pUcP4.setText(plastizing.charging.press.pUcP4.toString())
        input_pUcS1.setText(plastizing.charging.speed.pUcS1.toString())
        input_pUcS2.setText(plastizing.charging.speed.pUcS2.toString())
        input_pUcS3.setText(plastizing.charging.speed.pUcS3.toString())
        input_pUcS4.setText(plastizing.charging.speed.pUcS4.toString())
        input_pUcSt1.setText(plastizing.charging.stroke.pUcSt1.toString())
        input_pUcSt2.setText(plastizing.charging.stroke.pUcSt2.toString())
        input_pUcSt3.setText(plastizing.charging.stroke.pUcSt3.toString())
        input_pUcSt4.setText(plastizing.charging.stroke.pUcSt4.toString())
        input_pUsBP1.setText(plastizing.suckBack.press.pUsBP1.toString())
        input_pUsBS1.setText(plastizing.suckBack.speed.pUsBS1.toString())
        input_pUsBSt1.setText(plastizing.suckBack.stroke.pUsBSt1.toString())
        input_pUsBT1.setText(plastizing.suckBack.coolTime.pUsBT1.toString())

    }
}