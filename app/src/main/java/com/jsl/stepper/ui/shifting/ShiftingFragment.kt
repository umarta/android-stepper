package com.jsl.stepper.ui.shifting

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jsl.stepper.R
import com.jsl.stepper.utils.BaseApp

class ShiftingFragment : Fragment(), BaseApp.Listener {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shifting, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        BaseApp(this).set()
    }

    override fun getIntentData() {
        TODO("Not yet implemented")
    }

    override fun setOnClick() {
        TODO("Not yet implemented")
    }

    override fun setAdapter() {
        TODO("Not yet implemented")
    }

    override fun loadData() {
        TODO("Not yet implemented")
    }

    override fun setContent() {
        TODO("Not yet implemented")
    }


}