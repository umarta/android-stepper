package com.jsl.stepper.ui.control

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.production.ProductionControlData
import com.jsl.stepper.ui.material.pengisian.PengisianListAdapter
import kotlinx.android.synthetic.main.view_production_control.view.*

class ProductionControlAdapter(
    private val activity: Activity,
    private var data: MutableList<ProductionControlData>,
    val listener: ProductionControlAdapter.Listener
) : RecyclerView.Adapter<ProductionControlAdapter.Holder>() {
    fun setItems(data: MutableList<ProductionControlData>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<ProductionControlData>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(
            mData: ProductionControlData,
            position: Int,
            listener: ProductionControlAdapter.Listener
        ) {
            with(itemView) {
                tv_no_wo.text = mData.no_wo
                tv_kode_mold.text = mData.kode_mold
                tv_nama_part.text = mData.part_name
                tv_plan_mesin.text = mData.plan_machine
                tv_act_mesin.text =
                    if (mData.actual_machine?.length ?: 0 > 0) mData.actual_machine else "-"
                tv_prioritas.text = mData.production_remark

                when (mData.production_status) {
                    "open" -> {
                        fl_warna.setBackgroundResource(R.drawable.wo_list_bg)
                    }
                    "close" -> {
                        fl_warna.setBackgroundResource(R.drawable.bg_warning)
                    }
                    "halt" -> {
                        fl_warna.setBackgroundResource(R.drawable.bg_danger)
                    }
                    else -> {
                        fl_warna.setBackgroundResource(R.drawable.bg_primary)

                    }

                }
//                itemView.setOnClickListener {
//                    val i = Intent(activity, PengisianMaterialActivity::class.java)
//                    i.putExtra("id", mData.id)
//                    activity.startActivity(i)
//                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductionControlAdapter.Holder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.view_production_control, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: ProductionControlAdapter.Holder, position: Int) {
        holder.bind(data[position], position, listener)

    }

    interface Listener {
        fun onLoadList(data: ProductionControlData)
    }

    override fun getItemCount(): Int = data.size

}