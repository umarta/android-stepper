package com.jsl.stepper.ui.shifting

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.serahterima.SerahTerimaListData
import com.jsl.stepper.data.remote.response.shifting.ShiftingDetail
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.utils.hide
import com.jsl.stepper.utils.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_serah_terima.*
import kotlinx.android.synthetic.main.activity_shifting_detail.*
import kotlinx.android.synthetic.main.activity_shifting_detail.iv_back
import kotlinx.android.synthetic.main.activity_shifting_detail.progress_bar
import java.util.*

@AndroidEntryPoint
class ShiftingDetailActivity : AppCompatActivity(), BaseApp.Listener,
    ShiftingDetailAdapter.Listener {
    lateinit var hari: String
    lateinit var tanggal: String
    lateinit var list: MutableList<ShiftingDetail>
    private val viewModel: ShiftingViewModel by viewModels()
    private lateinit var shiftingDetailAdapter: ShiftingDetailAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shifting_detail)
        BaseApp(this).set()
    }

    override fun getIntentData() {
        if (intent.hasExtra("hari")) {
            hari = intent.getStringExtra("hari").toString()
        }
        if (intent.hasExtra("tanggal")) {
            tanggal = intent.getStringExtra("tanggal").toString()
        }
    }

    override fun setOnClick() {
        iv_back.setOnClickListener {
            startActivity(Intent(this, ShiftingActivity::class.java))
        }
    }

    override fun setAdapter() {
        shiftingDetailAdapter = ShiftingDetailAdapter(this, ArrayList(), this)
        rv_shifting.adapter = shiftingDetailAdapter
        rv_shifting.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }

    override fun loadData() {
        viewModel.getShiftHarian(tanggal).observe(this, Observer {
            if (it.success!!) {
                shiftingDetailAdapter.setItems(it.data)

//                val data = it.data
//                data.forEach {
//                    Log.d("tanggal", it.tanggal.toString())
//                    it.list.forEach {
//                        Log.d("jam_masuk", it.jam_masuk.toString())
//                    }
//                }
            }
        })
    }

    override fun setContent() {


    }

    override fun onLoadData(data: SerahTerimaListData) {

    }

    override fun onSave(res: HashMap<String, Any>) {
        Log.d("save", res.toString())
        progress_bar.show()

        viewModel.editShift(res).observe(this, Observer {
            if (it.success!!) {
                startActivity(Intent(this, ShiftingActivity::class.java))

                shiftingDetailAdapter.res.clear()
            }
        })
        progress_bar.hide()

    }
}