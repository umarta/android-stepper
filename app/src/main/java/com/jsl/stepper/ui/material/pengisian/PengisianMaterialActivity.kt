package com.jsl.stepper.ui.material.pengisian

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.utils.hide
import com.jsl.stepper.utils.show
import com.jsl.stepper.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_pengisian_material.*

@AndroidEntryPoint
class PengisianMaterialActivity : AppCompatActivity(), BaseApp.Listener {
    private val viewModel: MaterialViewModel by viewModels()
    private lateinit var machine: IdNameData
    private lateinit var material: IdNameData
    private lateinit var grade: IdNameData
    private lateinit var gradeRemark: IdNameData
    private lateinit var colour: IdNameData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pengisian_material)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        iv_back.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }
        sp_machine.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                machine = item
            }
        )
        sp_jenis_material.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                material = item
            }
        )
        sp_grade.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                grade = item
                materialGradeRemark()
            }
        )
        sp_remark.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                gradeRemark = item
            }
        )
        sp_warna.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                colour = item
            }
        )
        tv_save.setOnClickListener {
            submit()
        }
    }

    override fun setAdapter() {
    }

    override fun loadData() {
    }

    override fun setContent() {
        machine()
        material()
        materialGrade()
        colour()
    }

    fun machine() {
        progress_bar.show()
        viewModel.getMachine("").observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                sp_machine.setAdapter(spinnerAdapter)
            }
            progress_bar.hide()
        })
    }

    fun material() {
        progress_bar.show()
        viewModel.getMaterial().observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                sp_jenis_material.setAdapter(spinnerAdapter)
            }
            progress_bar.hide()
        })
    }

    private fun materialGrade() {
        progress_bar.show()
        viewModel.getMaterialGrade().observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                sp_grade.setAdapter(spinnerAdapter)
            }
            progress_bar.hide()
        })
    }

    private fun materialGradeRemark() {
        progress_bar.show()
        viewModel.getMaterialGradeRemark(material.id,grade.id).observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                sp_remark.setAdapter(spinnerAdapter)
            }
            progress_bar.hide()
        })
    }


    fun colour() {
        progress_bar.show()
        viewModel.getColour().observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                sp_warna.setAdapter(spinnerAdapter)
            }
            progress_bar.hide()
        })
    }

    private fun submit() {
        progress_bar.show()
        val res = HashMap<String, Any>()
        res["machine_id"] = machine.id
        res["material_id"] = material.id
        res["grade_id"] = grade.id
        res["grade_remark_id"] = gradeRemark.id
        res["color_id"] = colour.id
        val diisi = input_jumlah_diisi.text.toString()
        val diambil = input_jumlah_diambil.text.toString()

        if (!diisi.isEmpty() && !diambil.isEmpty()) {
            input_jumlah_diambil.error = "Hanya isi salah satu saja"
            input_jumlah_diisi.error = "Hanya isi salah satu saja"
            progress_bar.hide()

            return
        }
        if (diisi.isEmpty() && diambil.isEmpty()) {
            input_jumlah_diambil.error = "Hanya isi salah satu saja"
            input_jumlah_diisi.error = "Hanya isi salah satu saja"
            progress_bar.hide()
            return
        }
        res["diisi"] = diisi
        res["diambil"] = diambil
        viewModel.submit(res).observe(this, Observer {
            if (it.success!!) {
                toast(it.message?.first())
                val i = Intent(this, HomeActivity::class.java)
                this.startActivity(i)
            } else {
                toast(it.message?.first())
            }
        })
    }
}