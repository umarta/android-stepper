package com.jsl.stepper.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.jsl.stepper.R
import com.jsl.stepper.data.user.UserManager
import com.jsl.stepper.ui.auth.AuthenticationViewModel
import com.jsl.stepper.ui.auth.LoginActivity
import com.jsl.stepper.ui.control.ProductionControlActivity
import com.jsl.stepper.ui.material.opname.MaterialOpnameActivity
import com.jsl.stepper.ui.material.pengisian.PengisianListActivity
import com.jsl.stepper.ui.material.pengisian.PengisianListAdapter
import com.jsl.stepper.ui.material.pengisian.PengisianMaterialActivity
import com.jsl.stepper.ui.serahterima.SerahTerimaListActivity
import com.jsl.stepper.ui.shifting.ShiftingActivity
import com.jsl.stepper.ui.wo.WoListActivity
import com.jsl.stepper.ui.wo.detail.WoDetailActivity
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.vo.HttpCode
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

@AndroidEntryPoint
class HomeActivity : AppCompatActivity(), BaseApp.Listener {
    private val viewModel: AuthenticationViewModel by viewModels()

    @Inject
    lateinit var userManager: UserManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        work_order_ic.setOnClickListener {
            startActivity(Intent(this, WoListActivity::class.java))
        }
        ic_pengisian_material.setOnClickListener {
            startActivity(Intent(this, PengisianListActivity::class.java))
        }
        ic_opname_material.setOnClickListener {
            startActivity(Intent(this, MaterialOpnameActivity::class.java))
        }
        ic_setup_mold.setOnClickListener {
            val i = Intent(this, WoDetailActivity::class.java)
            i.putExtra("type", "molding")
            this.startActivity(i)
        }
        ic_setup_mesin.setOnClickListener {
            val i = Intent(this, WoDetailActivity::class.java)
            i.putExtra("type", "machine")
            this.startActivity(i)
        }
        ic_laporan_produksi.setOnClickListener {
            val i = Intent(this, WoDetailActivity::class.java)
            i.putExtra("type", "production")
            this.startActivity(i)
        }
        ic_ipql.setOnClickListener {
            val i = Intent(this, WoDetailActivity::class.java)
            i.putExtra("type", "ipqc")
            this.startActivity(i)
        }


        btn_serah_terima.setOnClickListener {
            val i = Intent(this, SerahTerimaListActivity::class.java)
            i.putExtra("type", "serah")
            startActivity(i)
        }
        btn_terima.setOnClickListener {
            val i = Intent(this, SerahTerimaListActivity::class.java)
            i.putExtra("type", "terima")
            startActivity(i)
        }
        ic_proccess_control.setOnClickListener {
            val i = Intent(this, ShiftingActivity::class.java)
            startActivity(i)
        }


    }

    override fun setAdapter() {
    }

    override fun setContent() {
    }

    override fun loadData() {
        viewModel.refreshToken().observe(this, Observer {
            Log.d("auth", HttpCode.SUCCESS.toString())
            if (it.code != HttpCode.SUCCESS) {
                userManager.logout()
                val i = Intent(this, LoginActivity::class.java)
                startActivity(i)

            }
        });
    }
}