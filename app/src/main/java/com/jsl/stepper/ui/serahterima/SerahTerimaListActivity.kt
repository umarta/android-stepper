package com.jsl.stepper.ui.serahterima

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.serahterima.SerahTerimaListData
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.utils.hide
import com.jsl.stepper.utils.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_serah_terima_list.*
import kotlinx.android.synthetic.main.activity_serah_terima_list.iv_back
import kotlinx.android.synthetic.main.activity_serah_terima_list.progress_bar
import kotlinx.android.synthetic.main.activity_serah_terima_list.swipeContainer
import kotlinx.android.synthetic.main.activity_wo_detail.*

@AndroidEntryPoint
class SerahTerimaListActivity : AppCompatActivity(), BaseApp.Listener,
    SerahTerimaListAdapter.Listener {
    private val viewModel: SerahTerimaViewModel by viewModels()
    private lateinit var serahTerimaListAdapter: SerahTerimaListAdapter
    private var next_page = true
    private var loading = false
    private var page = 1
    private lateinit var type: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_serah_terima_list)
        BaseApp(this).set()
    }

    override fun getIntentData() {
        if (intent.hasExtra("type")) {
            type = intent.getStringExtra("type").toString()
        }

    }

    override fun setOnClick() {
        iv_back.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }
        swipeContainer.setOnRefreshListener {
            serahTerimaListAdapter.clear()
            page = 1
            fetchData()
            swipeContainer.setRefreshing(false)
        }

        swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        );


    }

    override fun setAdapter() {
        serahTerimaListAdapter = SerahTerimaListAdapter(this, ArrayList(), this, type)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        rv_serahterima.adapter = serahTerimaListAdapter
        rv_serahterima.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_serahterima.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val countItem = layoutManager.itemCount
                val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                if (!recyclerView.canScrollVertically(1)) {
                    if (next_page) {
                        if (!loading && isLastPosition) {
                            page++
                            loadData()
                        }
                    }
                }
            }

        })
    }

    override fun loadData() {
        fetchData()
    }

    private fun fetchData() {
        if (type == "serah") {
            viewModel.list(page).observe(this, Observer {
                if (it.success!!) {
                    next_page = it.header.next_page
                    if (page == 1) {
                        serahTerimaListAdapter.setItems(it.data)
                    } else {
                        serahTerimaListAdapter.addItems(it.data)
                    }
                }
            })
        } else {
            viewModel.terima(page).observe(this, Observer {
                if (it.success!!) {
                    next_page = it.header.next_page
                    if (page == 1) {
                        serahTerimaListAdapter.setItems(it.data)
                    } else {
                        serahTerimaListAdapter.addItems(it.data)
                    }
                }
            })
        }
    }

    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) progress_bar.show()
            else progress_bar.hide()
        })
    }

    override fun onLoadData(data: SerahTerimaListData) {
        finish()
    }

}