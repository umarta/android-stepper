package com.jsl.stepper.ui.auth

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsl.stepper.data.remote.MainRepository
import com.jsl.stepper.data.remote.MainService
import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.auth.LoginData
import com.jsl.stepper.model.PersonModel

class AuthenticationViewModel @ViewModelInject constructor(mainService: MainService) : ViewModel() {
    private val repo = MainRepository(mainService)
    private val _page = MutableLiveData<Int>()
    private val _person = MutableLiveData<PersonModel>()

    val loading: LiveData<Boolean> get() = repo.loading
    val page: LiveData<Int> get() = _page
    val person: LiveData<PersonModel> get() = _person

    fun setPage(int: Int) { _page.value = int }
    fun setPerson(person: PersonModel) { _person.value = person}

    fun login(username: String, password: String): LiveData<BaseResponse<LoginData>> =
        repo.login(username, password)

    fun refreshToken(): MutableLiveData<BaseResponse<Any>> = repo.refreshToken()

    override fun onCleared() {
        super.onCleared()
        repo.onDestroy()
    }

}