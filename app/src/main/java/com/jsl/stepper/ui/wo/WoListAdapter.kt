package com.jsl.stepper.ui.wo

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.wo.WoData
import com.jsl.stepper.ui.wo.detail.WoDetailActivity
import com.jsl.stepper.utils.Constants
import kotlinx.android.synthetic.main.view_wo_list.view.*

class WoListAdapter(
    private val activity: Activity,
    private var data: MutableList<WoData>,
    val listener: Listener
) : RecyclerView.Adapter<WoListAdapter.Holder>() {
    fun setItems(data: MutableList<WoData>) {
        this.data = data
        notifyDataSetChanged()
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(mData: WoData, position: Int, listener: Listener) {
            with(itemView) {
                tv_status.text = mData.checked.toString() + "/" + mData.molding.toString()
                tv_wo_no.text = mData.documentno
                tv_date.text = mData.date
                tv_desc.text = mData.desc
                tv_no.text = (position + 1).toString()
                if (mData.checked == mData.molding) {
                    wo_card.setBackgroundResource(R.drawable.wo_list_bg)
                }

                itemView.setOnClickListener {
                    val i = Intent(activity, WoDetailActivity::class.java)
                    i.putExtra(Constants.WO_ID, mData.documentno)
                    i.putExtra("jenis", "first")
                    activity.startActivity(i)
                }
            }
        }
    }

    interface Listener {
        fun onLoadWo(data: WoData)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_wo_list, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position], position, listener)

    }

    override fun getItemCount(): Int = data.size
}