package com.jsl.stepper.ui.preview

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.jsl.stepper.R
import com.jsl.stepper.model.InjectionSetting
import com.jsl.stepper.model.SettingDetail
import com.jsl.stepper.ui.stepper.InjectionSettingViewModel
import com.jsl.stepper.utils.BaseApp

class PreviewActivity : AppCompatActivity(), BaseApp.Listener {
    private val viewModel: InjectionSettingViewModel by viewModels()
    private lateinit var settingDetail: SettingDetail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview)
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
    }

    override fun setAdapter() {
    }

    override fun setContent() {
    }

    override fun loadData() {
        viewModel.settingDetail(1000028).observe(this, {
            if (it.success!!) {
                settingDetail = it.data
            }
        })
    }
}