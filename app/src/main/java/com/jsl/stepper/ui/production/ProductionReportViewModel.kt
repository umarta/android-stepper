package com.jsl.stepper.ui.production

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsl.stepper.data.remote.MainRepository
import com.jsl.stepper.data.remote.MainService
import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.production.ProductionData
import com.jsl.stepper.data.remote.response.wo.WoDetailData

class ProductionReportViewModel @ViewModelInject constructor(mainService: MainService) : ViewModel() {
    private val repoMain =
        MainRepository(mainService)
    val loading: LiveData<Boolean> get() = repoMain.loading
    private val _woDetail = MutableLiveData<WoDetailData>()
    val woDetail: LiveData<WoDetailData> get() = _woDetail
    fun setWoDetail(woDetailData: WoDetailData) {
        _woDetail.value = woDetailData
    }

    fun submit(request: HashMap<String, Any>): MutableLiveData<BaseResponse<ProductionData>> =
        repoMain.submitProduction(request)

    fun getData(req:Int): MutableLiveData<BaseResponse<ProductionData>> =
        repoMain.getProductionData(req)
}