package com.jsl.stepper.ui.production

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.jsl.stepper.R
import com.jsl.stepper.data.local.WoDetailObject
import com.jsl.stepper.data.remote.response.production.ProductionData
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.ui.dialog.CloseDialog
import com.jsl.stepper.ui.dialog.RemarkDialog
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.ui.summary.SummaryProductionActivity
import com.jsl.stepper.ui.wo.WoListActivity
import com.jsl.stepper.ui.wo.detail.WoDetailActivity
import com.jsl.stepper.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_production_report.*


@AndroidEntryPoint
class ProductionReportActivity : AppCompatActivity(), BaseApp.Listener, RemarkDialog.Listener,
    CloseDialog.Listener {
    private var startButtonClicked = false

    private var TOTAL_SHOOT = 0;
    private var TOTAL_OK = 0;
    private var TOTAL_SHORT = 0;
    private var TOTAL_WELDING = 0;
    private var TOTAL_SILVER = 0;
    private var TOTAL_FLOW_MARK = 0;
    private var TOTAL_CRACK = 0;
    private var TOTAL_SCRATCH = 0;
    private var TOTAL_OTHER = 0;
    private var TOTAL_NG = 0;
    private var interupt = false
    private var isCounterRun = false
    private var isClose = false
    private var changeShift = false
    private var isHalt = false
    private val viewModel: ProductionReportViewModel by viewModels()
    private lateinit var wo_id: String
    private var remark: String? = null
    private lateinit var mapping_id: String
    private lateinit var productionData: ProductionData
    private lateinit var type: String
    private var timeInSeconds = 0L
    private val mInterval = 1000
    private var mHandler: Handler? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_production_report)
        BaseApp(this).set()
    }

    override fun getIntentData() {
        if (intent.hasExtra("object")) {
            val woDetailData = intent.getSerializableExtra("object") as? WoDetailObject
            val woData =
                WoDetailData(
                    id = woDetailData?.id,
                    sequenceno = woDetailData?.sequenceno,
                    molding_id = woDetailData?.molding_id,
                    code = woDetailData?.code,
                    name = woDetailData?.name,
                    cavity = woDetailData?.cavity,
                    tonase = woDetailData?.tonase,
                    size = woDetailData?.size,
                    description = woDetailData?.description,
                    colour = woDetailData?.colour,
                    category = woDetailData?.category,
                    merek = woDetailData?.merek,
                    brand = woDetailData?.brand,
                    sub_brand = woDetailData?.sub_brand,
                    status = woDetailData?.status,
                    location = woDetailData?.location,
                    machine = woDetailData?.machine,
                    machine_id = woDetailData?.machine_id,
                    jenis_proses = woDetailData?.jenis_proses,
                    cycle_time = woDetailData?.cycle_time,
                    material = woDetailData?.material,
                    womolding_id = woDetailData?.womolding_id,
                    is_created = woDetailData?.is_created,
                    product_name = woDetailData?.product_name,
                    no_wo = woDetailData?.no_wo
                )
            viewModel.setWoDetail(woData)
        }
        if (intent.hasExtra("wo_id")) {
            wo_id = intent.getStringExtra("wo_id").toString()
        }
        if (intent.hasExtra("mapping_id")) {
            mapping_id = intent.getStringExtra("mapping_id").toString()
        }
    }

    override fun setOnClick() {
        start.setOnClickListener {
            start.hide()
            startButtonClicked = true
            rest.show()
            stop.show()
            halt.show()
            close.show()
            if (interupt) {
                interupt = false
            }
            type = "start"
            nextAction()
        }

        stop.setOnClickListener {
            start.show()
            interupt = true
            stop.hide()
            rest.hide()
            halt.hide()
            close.hide()
            resume()
            type = "stop"
            changeShift = true
            nextAction()
        }
        halt.setOnClickListener {


            val fm: FragmentManager = supportFragmentManager
            val dialog = RemarkDialog("Ada masalah ?", this)
            dialog.show(fm, RemarkDialog::javaClass.name)

        }
        close.setOnClickListener {
            val fm: FragmentManager = supportFragmentManager
            val dialog =
                CloseDialog("Apakah anda yakin telah menyelesaikan seluruh WO proses ini?", this)
            dialog.show(fm, CloseDialog::javaClass.name)


        }
        rest.setOnClickListener {
            start.show()
            type = "rest"
            interupt = true
            stop.hide()
            halt.hide()
            close.hide()
            rest.hide()
            resume()
            nextAction()
        }

        summary.setOnClickListener {
            val i = Intent(this, SummaryProductionActivity::class.java)
            this.startActivity(i)
        }
        act_ok.setOnClickListener {
            if (!startButtonClicked || interupt) {
                toast("Aksi tidak valid, klik tombol start terlebih dahulu")
                return@setOnClickListener
            }
            TOTAL_OK += 1
            TOTAL_SHOOT += 1
            tv_total_shoot.text = TOTAL_SHOOT.toString()
            tv_ok.text = TOTAL_OK.toString()
            clickProduction()
        }

        act_short.setOnClickListener {
            if (!startButtonClicked || interupt) {
                toast("Aksi tidak valid, klik tombol start terlebih dahulu")
                return@setOnClickListener
            }
            TOTAL_SHOOT += 1
            TOTAL_SHORT += 1
            TOTAL_NG += 1
            tv_total_shoot.text = TOTAL_SHOOT.toString()
            tv_short.text = TOTAL_SHORT.toString()
            tv_total_ng.text = TOTAL_NG.toString()
            clickProduction()
        }
        act_welding.setOnClickListener {
            if (!startButtonClicked || interupt) {
                toast("Aksi tidak valid, klik tombol start terlebih dahulu")
                return@setOnClickListener
            }
            TOTAL_SHOOT += 1
            TOTAL_WELDING += 1
            TOTAL_NG += 1
            tv_total_shoot.text = TOTAL_SHOOT.toString()
            tv_welding.text = TOTAL_WELDING.toString()
            tv_total_ng.text = TOTAL_NG.toString()
            clickProduction()
        }
        act_welding.setOnClickListener {
            if (!startButtonClicked || interupt) {
                toast("Aksi tidak valid, klik tombol start terlebih dahulu")
                return@setOnClickListener
            }
            TOTAL_SHOOT += 1
            TOTAL_WELDING += 1
            TOTAL_NG += 1
            tv_total_shoot.text = TOTAL_SHOOT.toString()
            tv_welding.text = TOTAL_WELDING.toString()
            tv_total_ng.text = TOTAL_NG.toString()
            clickProduction()
        }

        act_silver.setOnClickListener {
            if (!startButtonClicked || interupt) {
                toast("Aksi tidak valid, klik tombol start terlebih dahulu")
                return@setOnClickListener
            }
            TOTAL_SHOOT += 1
            TOTAL_SILVER += 1
            TOTAL_NG += 1
            tv_total_shoot.text = TOTAL_SHOOT.toString()
            tv_silver.text = TOTAL_SILVER.toString()
            tv_total_ng.text = TOTAL_NG.toString()
            clickProduction()
        }
        act_fm.setOnClickListener {
            if (!startButtonClicked || interupt) {
                toast("Aksi tidak valid, klik tombol start terlebih dahulu")
                return@setOnClickListener
            }
            TOTAL_SHOOT += 1
            TOTAL_FLOW_MARK += 1
            TOTAL_NG += 1
            tv_total_shoot.text = TOTAL_SHOOT.toString()
            tv_fm.text = TOTAL_FLOW_MARK.toString()
            tv_total_ng.text = TOTAL_NG.toString()
            clickProduction()
        }
        act_crack.setOnClickListener {
            if (!startButtonClicked || interupt) {
                toast("Aksi tidak valid, klik tombol start terlebih dahulu")
                return@setOnClickListener
            }
            TOTAL_SHOOT += 1
            TOTAL_CRACK += 1
            TOTAL_NG += 1
            tv_total_shoot.text = TOTAL_SHOOT.toString()
            tv_crack.text = TOTAL_CRACK.toString()
            tv_total_ng.text = TOTAL_NG.toString()
            clickProduction()
        }
        act_scratch.setOnClickListener {
            if (!startButtonClicked || interupt) {
                toast("Aksi tidak valid, klik tombol start terlebih dahulu")
                return@setOnClickListener
            }
            TOTAL_SHOOT += 1
            TOTAL_SCRATCH += 1
            TOTAL_NG += 1
            tv_total_shoot.text = TOTAL_SHOOT.toString()
            tv_scratch.text = TOTAL_SCRATCH.toString()
            tv_total_ng.text = TOTAL_NG.toString()
            clickProduction()
        }
        act_other.setOnClickListener {
            if (!startButtonClicked || interupt) {
                toast("Aksi tidak valid, klik tombol start terlebih dahulu")
                return@setOnClickListener
            }
            TOTAL_SHOOT += 1
            TOTAL_OTHER += 1
            TOTAL_NG += 1
            tv_total_shoot.text = TOTAL_SHOOT.toString()
            tv_other.text = TOTAL_OTHER.toString()
            tv_total_ng.text = TOTAL_NG.toString()
            clickProduction()
        }
        iv_back.setOnClickListener {
            val intent = Intent(this@ProductionReportActivity, WoDetailActivity::class.java)
            intent.putExtra("type", "production")
            startActivity(intent)
        }
        this?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val intent = Intent(this@ProductionReportActivity, WoDetailActivity::class.java)
                intent.putExtra("type", "production")
                startActivity(intent)
            }
        })
    }

    fun checkStartIsClicked() {
        if (!startButtonClicked) {
            toast("Aksi tidak valid, klik tombol start terlebih dahulu")
            return
        }
    }

    fun clickAction(type: String, isOk: Boolean, inputData: TextView) {
        if (isOk) {
            TOTAL_OK += 1
        } else {
            TOTAL_NG += 1
        }
        TOTAL_SHOOT += 1
    }

    private fun nextAction() {
        Log.d("started", mapping_id)
        if (isCounterRun) {
            mHandler?.removeCallbacks(mStatusChecker)
            submitData()
        }
        headerClick()

    }

    @SuppressLint("SetTextI18n")
    fun resume() {
        if (interupt) {
            start.text = "resume"
        } else {
            start.hide()
            stop.hide()
            halt.hide()
            close.hide()
            rest.hide()
        }
    }

    override fun setAdapter() {
    }

    override fun loadData() {
        progress_bar.show()

        viewModel.getData(mapping_id.toInt()).observe(this, Observer {
            if (it.success!!) {
                productionData = it.data
                toast(it.message?.first())
                TOTAL_OK = productionData.total_ok!!
                TOTAL_SHOOT = productionData.total_shot!!
                TOTAL_NG = productionData.total_ng!!
                TOTAL_SHORT = productionData.total_short!!
                TOTAL_WELDING = productionData.total_welding!!
                TOTAL_SILVER = productionData.total_silver!!
                TOTAL_FLOW_MARK = productionData.total_flow_mark!!
                TOTAL_CRACK = productionData.total_crack!!
                TOTAL_SCRATCH = productionData.total_scratch!!
                TOTAL_OTHER = productionData.total_other!!
                tv_ok.text = productionData.total_ok.toString()
                tv_total_ng.text = productionData.total_ng.toString()
                tv_short.text = productionData.total_short.toString()
                tv_total_shoot.text = productionData.total_shot.toString()
                tv_welding.text = productionData.total_welding.toString()
                tv_silver.text = productionData.total_silver.toString()
                tv_fm.text = productionData.total_flow_mark.toString()
                tv_crack.text = productionData.total_crack.toString()
                tv_scratch.text = productionData.total_scratch.toString()
                tv_other.text = productionData.total_other.toString()

                when (productionData.status) {
                    1 -> {
                        start.hide()
                        startButtonClicked = true
                        rest.show()
                        stop.show()
                        halt.show()
                        close.show()
                        if (interupt) {
                            interupt = false
                        }
                        type = "start"
                    }
                    2 -> {
                        start.show()
                        rest.hide()
                        stop.hide()
                        halt.hide()
                        close.hide()
                        if (interupt) {
                            interupt = true
                        }
                        type = "stop"
                    }
                    3 -> {
                        start.show()
                        rest.hide()
                        stop.hide()
                        halt.hide()
                        close.hide()
                        if (interupt) {
                            interupt = true
                        }
                        type = "halt"
                    }
                    4 -> {
                        start.hide()
                        rest.hide()
                        stop.hide()
                        halt.hide()
                        close.hide()

                        type = "close"
                    }
                }
//                if (productionData.status == 1) {
//
//                } else {
//                    tv_ok.text = productionData.total_ok.toString()
//                    tv_total_ng.text = productionData.total_ng.toString()
//                    tv_short.text = productionData.total_short.toString()
//                    tv_total_shoot.text = productionData.total_shot.toString()
//                    tv_welding.text = productionData.total_welding.toString()
//                    tv_silver.text = productionData.total_silver.toString()
//                    tv_fm.text = productionData.total_flow_mark.toString()
//                    tv_crack.text = productionData.total_crack.toString()
//                    tv_scratch.text = productionData.total_scratch.toString()
//                    tv_other.text = productionData.total_other.toString()
//                }
            } else {
                toast(it.message?.first())
            }
            progress_bar.hide()

        })


    }

    override fun setContent() {
        if (!startButtonClicked) {
            start.show()
            stop.hide()
            halt.hide()
            close.hide()
            rest.hide()
        }
    }

    private fun headerClick() {
        val res = HashMap<String, Any>()
        res["mapping_id"] = mapping_id
        res["is_header"] = true
        res["type"] = type
        res["remarks"] = if (remark != null) remark.toString() else "-"
        viewModel.submit(res).observe(this, Observer {
            if (it.success!!) {
                toast(it.message?.first())
            } else {
                toast(it.message?.first())
            }
        })
    }

    private var mStatusChecker: Runnable = object : Runnable {
        override fun run() {
            try {
                timeInSeconds += 1
                Log.e("timeInSeconds", timeInSeconds.toString())
            } finally {
                mHandler!!.postDelayed(this, mInterval.toLong())
                if (timeInSeconds.toInt() == 2) {
                    Log.e("s", "stoped")
                    mHandler?.removeCallbacks(this)
                    submitData()
                    isCounterRun = false
                }
            }
        }
    }

    private fun resetCounter() {
        timeInSeconds = 0
    }

    private fun runCounter() {
        isCounterRun = true
        mHandler = Handler(Looper.getMainLooper())
        mStatusChecker.run()
    }

    private fun submitData() {
        val res = HashMap<String, Any>()
        res["mapping_id"] = mapping_id
        res["is_header"] = false
        res["total_ok"] = TOTAL_OK
        res["total_ng"] = TOTAL_NG
        res["total_shot"] = TOTAL_SHOOT
        res["total_welding"] = TOTAL_WELDING
        res["total_short"] = TOTAL_SHORT
        res["total_silver"] = TOTAL_SILVER
        res["total_flow_mark"] = TOTAL_FLOW_MARK
        res["total_crack"] = TOTAL_CRACK
        res["total_scratch"] = TOTAL_SCRATCH
        res["total_other"] = TOTAL_OTHER
        viewModel.submit(res).observe(this, Observer {
            if (it.success!!) {
                toast(it.message?.first())
            } else {
                toast(it.message?.first())
            }
        })
    }

    private fun clickProduction() {
        resetCounter()
        if (isCounterRun) {
            mHandler?.removeCallbacks(mStatusChecker)
            runCounter()
        } else {
            runCounter()
        }
    }

    fun afterOnMain() {
        Handler().postDelayed({
            clickProduction()
        }, 5000)
    }

    override fun onCloseRemarkDialog(string: String) {
        start.show()
        interupt = true
        stop.hide()
        halt.hide()
        close.hide()
        rest.hide()
        type = "halt"
        resume()
        remark = string
        nextAction()
    }

    override fun cancelRemarkClose() {
    }

    override fun onCloseDialog() {
        start.hide()
        isClose = true
        interupt = true
        stop.hide()
        halt.hide()
        close.hide()
        rest.hide()
        type = "close"
        resume()
        nextAction()
    }

    override fun cancelClose() {
    }
}