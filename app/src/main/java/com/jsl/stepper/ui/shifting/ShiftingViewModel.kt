package com.jsl.stepper.ui.shifting

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsl.stepper.data.remote.MainRepository
import com.jsl.stepper.data.remote.MainService
import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.shifting.ShiftingDetail
import com.jsl.stepper.data.remote.response.shifting.ShiftingList

class ShiftingViewModel @ViewModelInject constructor(mainService: MainService) : ViewModel() {
    private val repoMain =
        MainRepository(mainService)
    val loading: LiveData<Boolean> get() = repoMain.loading
    fun list(
        page: Int,
        tahun: Int,
        bulan: Int
    ): MutableLiveData<BaseResponse<MutableList<ShiftingList>>> =
        repoMain.shiftingList(page, tahun, bulan)

    fun getShiftHarian(
        tanggal: String
    ): MutableLiveData<BaseResponse<MutableList<ShiftingDetail>>> =
        repoMain.getShiftHarian(tanggal)


    fun editShift(
        res: HashMap<String, Any>
    ): MutableLiveData<BaseResponse<ShiftingDetail>> =
        repoMain.editShift(res)

    fun generateShift(
        request: HashMap<String, Any>
    ): MutableLiveData<BaseResponse<MutableList<ShiftingList>>> =
        repoMain.generateShift(request)


}