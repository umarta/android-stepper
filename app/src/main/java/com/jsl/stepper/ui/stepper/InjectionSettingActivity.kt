package com.jsl.stepper.ui.stepper

import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.aceinteract.android.stepper.StepperNavListener
import com.aceinteract.android.stepper.StepperNavigationView
import com.jsl.stepper.R
import com.jsl.stepper.data.local.SharedPreferencesStorage
import com.jsl.stepper.model.StepperSettings
import com.jsl.stepper.model.stepper.*
import com.jsl.stepper.utils.BaseApp
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_injection_setting.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_step1.*
import kotlinx.android.synthetic.main.fragment_step2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import ng.softcom.android.utils.ui.findNavControllerFromFragmentContainer
import org.json.JSONObject
import javax.inject.Inject


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class InjectionSettingActivity : AppCompatActivity(), StepperNavListener, BaseApp.Listener,
    FragmentListener {
    private val viewModel: StepperViewModel by lazy { ViewModelProvider(this)[StepperViewModel::class.java] }
    private val injectionModel: InjectionSettingViewModel by viewModels()
    private lateinit var isMoP: IsMoP
    private lateinit var isMoS: IsMoS
    private lateinit var isMoSt: IsMoSt
    private lateinit var isMcS: IsMcS
    private lateinit var isMcP: IsMcP
    private lateinit var isMcSt: IsMcSt
    @Inject
    lateinit var sharedPreferencesStorage: SharedPreferencesStorage
    lateinit var navController:NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        super.onCreate(savedInstanceState)
        BaseApp(this).set()
        setContentView(R.layout.activity_injection_setting)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.frame_stepper) as NavHostFragment
        navController  = navHostFragment.navController
        stepper.initializeStepper(navController)

        setupActionBarWithNavController(
            navController,
            AppBarConfiguration.Builder(
                R.id.step_1_dest,
                R.id.step_2_dest,
                R.id.step_3_dest,
                R.id.step_4_dest
            ).build()
        )
        setupActionBarWithNavController(navController)

        button_previous.setOnClickListener {

            when (stepper.currentStep) {
                2 -> stepper.goToPreviousStep()
                1 -> {
                    fillMoldSetting()
                }
            }
            refreshCurrentFragment()

        }

        button_next.setOnClickListener {

            when (stepper.currentStep) {
                0 -> {
                    moldSettingValidation()
                }
                1 -> injectionSetting1()
            }
            refreshCurrentFragment()
        }
        collectStateFlow()
    }
    private fun refreshCurrentFragment(){
        val id = navController.currentDestination?.id
        navController.popBackStack(id!!,true)
        navController.navigate(id)
    }


    private fun StepperNavigationView.initializeStepper(navController:NavController) {
        viewModel.updateStepper(
            StepperSettings(
                widgetColor,
                textColor,
                textSize,
                iconSize
            )
        )

        stepperNavListener = this@InjectionSettingActivity
        setupWithNavController(navController)
    }

    private fun collectStateFlow() {
        viewModel.stepperSettings.onEach {
            stepper.widgetColor = it.iconColor
            stepper.textColor = it.textColor
            stepper.textSize = it.textSize
            stepper.iconSize = it.iconSize
        }.launchIn(lifecycleScope)
    }

    override fun onStepChanged(step: Int) {
        button_previous.isVisible = step != 0

        if (step == 3) {
            button_next.setImageResource(R.drawable.ic_check)
        } else {
            button_next.setImageResource(R.drawable.ic_right)
        }
    }
    override fun onSupportNavigateUp(): Boolean =
        findNavController(R.id.frame_stepper).navigateUp()

    override fun onBackPressed() {
        Log.d("test","ss")
        if (stepper.currentStep == 0) {
            super.onBackPressed()
        } else {
            findNavController(R.id.frame_stepper).navigateUp()
        }
    }


    override fun onCompleted() {
        val rootObject = JSONObject()

    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
    }

    override fun setAdapter() {
    }

    override fun setContent() {

        injectionModel.isMoP.observe(this, Observer {
            isMoP = it
        })
        injectionModel.isMoS.observe(this, Observer {
            isMoS = it
        })
        injectionModel.isMoSt.observe(this, Observer {
            isMoSt = it
        })
        injectionModel.isMcP.observe(this, Observer {
            isMcP = it
        })
        injectionModel.isMcS.observe(this, Observer {
            isMcS = it
        })
        injectionModel.isMcSt.observe(this, Observer {
            isMcSt = it
        })
    }

    override fun loadData() {
    }

    private fun moldSettingValidation() {
        val moP1 = input_moP1.text.toString();
        val moP2 = input_moP2.text.toString();
        val moP3 = input_moP3.text.toString();
        val moP4 = input_moP4.text.toString();
        val moP5 = input_moP5.text.toString();

        val moS1 = input_moS1.text.toString();
        val moS2 = input_moS2.text.toString();
        val moS3 = input_moS3.text.toString();
        val moS4 = input_moS4.text.toString();
        val moS5 = input_moS5.text.toString();

        val moSt1 = input_moSt1.text.toString();
        val moSt2 = input_moSt2.text.toString();
        val moSt3 = input_moSt3.text.toString();
        val moSt4 = input_moSt4.text.toString();
        val moSt5 = input_moSt5.text.toString();

        val mcP1 = input_mcP1.text.toString();
        val mcP2 = input_mcP2.text.toString();
        val mcP3 = input_mcP3.text.toString();
        val mcP4 = input_mcP4.text.toString();
        val mcP5 = input_mcP5.text.toString();

        val mcS1 = input_mcS1.text.toString();
        val mcS2 = input_mcS2.text.toString();
        val mcS3 = input_mcS3.text.toString();
        val mcS4 = input_mcS4.text.toString();
        val mcS5 = input_mcS5.text.toString();

        val mcSt1 = input_mcSt1.text.toString();
        val mcSt2 = input_mcSt2.text.toString();
        val mcSt3 = input_mcSt3.text.toString();
        val mcSt4 = input_mcSt4.text.toString();
        val mcSt5 = input_mcSt5.text.toString();

        if (moP1!!.isEmpty()) {
            input_moP1.error = "Mold Open Press 1 tidak boleh kosong"
            return
        }
        if (moP2!!.isEmpty()) {
            input_moP2.error = "Mold Open Press 2 tidak boleh kosong"
            return
        }
        if (moP3!!.isEmpty()) {
            input_moP3.error = "Mold Open Press 3 tidak boleh kosong"
            return
        }
        if (moP4!!.isEmpty()) {
            input_moP4.error = "Mold Open Press 4 tidak boleh kosong"
            return
        }
        if (moP5!!.isEmpty()) {
            input_moP5.error = "Mold Open Press 5 tidak boleh kosong"
            return
        }
        if (moS1!!.isEmpty()) {
            input_moS1.error = "Mold Open Speed 1 tidak boleh kosong"
            return
        }
        if (moS2!!.isEmpty()) {
            input_moS2.error = "Mold Open Speed 2 tidak boleh kosong"
            return
        }
        if (moS3!!.isEmpty()) {
            input_moS3.error = "Mold Open Speed 3 tidak boleh kosong"
            return
        }
        if (moS4!!.isEmpty()) {
            input_moS4.error = "Mold Open Speed 4 tidak boleh kosong"
            return
        }
        if (moS5!!.isEmpty()) {
            input_moS5.error = "Mold Open Speed 5 tidak boleh kosong"
            return
        }
        if (moSt1!!.isEmpty()) {
            input_moSt1.error = "Mold Open Stroke 1 tidak boleh kosong"
            return
        }
        if (moSt2!!.isEmpty()) {
            input_moSt2.error = "Mold Open Stroke 2 tidak boleh kosong"
            return
        }
        if (moSt3!!.isEmpty()) {
            input_moSt3.error = "Mold Open Stroke 3 tidak boleh kosong"
            return
        }
        if (moSt4!!.isEmpty()) {
            input_moSt4.error = "Mold Open Stroke 4 tidak boleh kosong"
            return
        }
        if (moSt5!!.isEmpty()) {
            input_moSt5.error = "Mold Open Stroke 5 tidak boleh kosong"
            return
        }
        if (mcP1!!.isEmpty()) {
            input_mcP1.error = "Mold Close Press 1 tidak boleh kosong"
            return
        }
        if (mcP2!!.isEmpty()) {
            input_mcP2.error = "Mold Close Press 2 tidak boleh kosong"
            return
        }
        if (mcP3!!.isEmpty()) {
            input_mcP3.error = "Mold Close Press 3 tidak boleh kosong"
            return
        }
        if (mcP4!!.isEmpty()) {
            input_mcP4.error = "Mold Close Press 4 tidak boleh kosong"
            return
        }
        if (mcP5!!.isEmpty()) {
            input_mcP5.error = "Mold Close Press 5 tidak boleh kosong"
            return
        }
        if (mcS1!!.isEmpty()) {
            input_mcS1.error = "Mold Close Speed 1 tidak boleh kosong"
            return
        }
        if (mcS2!!.isEmpty()) {
            input_mcS2.error = "Mold Close Speed 2 tidak boleh kosong"
            return
        }
        if (mcS3!!.isEmpty()) {
            input_mcS3.error = "Mold Close Speed 3 tidak boleh kosong"
            return
        }
        if (mcS4!!.isEmpty()) {
            input_mcS4.error = "Mold Close Speed 4 tidak boleh kosong"
            return
        }
        if (mcS5!!.isEmpty()) {
            input_mcS5.error = "Mold Close Speed 5 tidak boleh kosong"
            return
        }
        if (mcSt1!!.isEmpty()) {
            input_mcSt1.error = "Mold Close Stroke 1 tidak boleh kosong"
            return
        }
        if (mcSt2!!.isEmpty()) {
            input_mcSt2.error = "Mold Close Stroke 2 tidak boleh kosong"
            return
        }
        if (mcSt3!!.isEmpty()) {
            input_mcSt3.error = "Mold Close Stroke 3 tidak boleh kosong"
            return
        }
        if (mcSt4!!.isEmpty()) {
            input_mcSt4.error = "Mold Close Stroke 4 tidak boleh kosong"
            return
        }
        if (mcSt5!!.isEmpty()) {
            input_mcSt5.error = "Mold Close Stroke 5 tidak boleh kosong"
            return
        }
        val isMoP = IsMoP(moP1.toInt(), moP2.toInt(), moP3.toInt(), moP4.toInt(), moP5.toInt())
        val isMos = IsMoS(moS1.toInt(), moS2.toInt(), moS3.toInt(), moS4.toInt(), moS5.toInt())
        val isMoSt =
            IsMoSt(moSt1.toInt(), moSt2.toInt(), moSt3.toInt(), moSt4.toInt(), moSt5.toInt())
        val isMcP =
            IsMcP(mcP1.toInt(), mcP2.toInt(), mcP3.toInt(), mcP4.toInt(), mcP5.toInt())
        val isMcS =
            IsMcS(mcS1.toInt(), mcS2.toInt(), mcS3.toInt(), mcS4.toInt(), mcS5.toInt())
        val isMcSt =
            IsMcSt(mcSt1.toInt(), mcSt2.toInt(), mcSt3.toInt(), mcSt4.toInt(), mcSt5.toInt())

        injectionModel.setIsMoP(isMoP)
        injectionModel.setIsMoS(isMos)
        injectionModel.setIsMoSt(isMoSt)
        injectionModel.setIsMcP(isMcP)
        injectionModel.setIsMcS(isMcS)
        injectionModel.setIsMcSt(isMcSt)
        stepper.goToNextStep()
    }

    private fun injectionSetting1() {
        val hP1 = input_iShP1.text
        val hP2 = input_iShP2.text
        val hP3 = input_iShP3.text

        val hS1 = input_iShS1.text
        val hS2 = input_iShS2.text
        val hS3 = input_iShS3.text

        val hT1 = input_iShT1.text
        val hT2 = input_iShT2.text
        val hT3 = input_iShT3.text

        val iP1 = input_iSiP1.text
        val iP2 = input_iSiP2.text
        val iP3 = input_iSiP3.text
        val iP4 = input_iSiP4.text
        val iP5 = input_iSiP5.text
        val iP6 = input_iSiP6.text

        val iS1 = input_iSiS1.text
        val iS2 = input_iSiS2.text
        val iS3 = input_iSiS3.text
        val iS4 = input_iSiS4.text
        val iS5 = input_iSiS5.text
        val iS6 = input_iSiS6.text

        val iSt1 = input_iSiSt1.text
        val iSt2 = input_iSiSt2.text
        val iSt3 = input_iSiSt3.text
        val iSt4 = input_iSiSt4.text
        val iSt5 = input_iSiSt5.text
        val iSt6 = input_iSiSt6.text


        if (hP1!!.isEmpty()) {
            input_iShP1.error = "Injection Setting Press 1 tidak boleh kosong"
            return
        }
        if (hP2!!.isEmpty()) {
            input_iShP2.error = "Injection Setting Press 2 tidak boleh kosong"
            return
        }
        if (hP3!!.isEmpty()) {
            input_iShP3.error = "Injection Setting Press 3 tidak boleh kosong"
            return
        }
        if (hS1!!.isEmpty()) {
            input_iShS1.error = "Injection Setting Press 1 tidak boleh kosong"
            return
        }
        if (hS2!!.isEmpty()) {
            input_iShS2.error = "Injection Setting Press 2 tidak boleh kosong"
            return
        }
        if (hS3!!.isEmpty()) {
            input_iShS3.error = "Injection Setting Speed 3 tidak boleh kosong"
            return
        }
        if (hT1!!.isEmpty()) {
            input_iShT1.error = "Injection Setting Speed 1 tidak boleh kosong"
            return
        }
        if (hT2!!.isEmpty()) {
            input_iShT2.error = "Injection Setting Speed 2 tidak boleh kosong"
            return
        }
        if (hT3!!.isEmpty()) {
            input_iShT3.error = "Injection Setting Speed 3 tidak boleh kosong"
            return
        }
        if (iP1!!.isEmpty()) {
            input_iSiP1.error = "Injection Setting Speed 1 tidak boleh kosong"
            return
        }
        if (iP2!!.isEmpty()) {
            input_iSiP2.error = "Injection Setting Stroke 2 tidak boleh kosong"
            return
        }
        if (iP3!!.isEmpty()) {
            input_iSiP3.error = "Injection Setting Stroke 3 tidak boleh kosong"
            return
        }
        if (iP4!!.isEmpty()) {
            input_iSiP4.error = "Injection Setting Stroke 4 tidak boleh kosong"
            return
        }
        if (iP5!!.isEmpty()) {
            input_iSiP5.error = "Injection Setting Stroke 5 tidak boleh kosong"
            return
        }
        if (iP6!!.isEmpty()) {
            input_iSiP6.error = "Injection Setting Stroke 6 tidak boleh kosong"
            return
        }
        if (iS1!!.isEmpty()) {
            input_iSiS1.error = "Injection Setting Press 1 tidak boleh kosong"
            return
        }
        if (iS2!!.isEmpty()) {
            input_iSiS2.error = "Injection Setting Press 2 tidak boleh kosong"
            return
        }
        if (iS3!!.isEmpty()) {
            input_iSiS3.error = "Injection Setting Press 3 tidak boleh kosong"
            return
        }
        if (iS4!!.isEmpty()) {
            input_iSiS4.error = "Injection Setting Press 4 tidak boleh kosong"
            return
        }
        if (iS5!!.isEmpty()) {
            input_iSiS5.error = "Injection Setting Press 5 tidak boleh kosong"
            return
        }
        if (iS6!!.isEmpty()) {
            input_iSiS6.error = "Injection Setting Speed 6 tidak boleh kosong"
            return
        }
        if (iSt1!!.isEmpty()) {
            input_iSiSt1.error = "Injection Setting Speed 1 tidak boleh kosong"
            return
        }
        if (iSt2!!.isEmpty()) {
            input_iSiSt2.error = "Injection Setting Speed 2 tidak boleh kosong"
            return
        }
        if (iSt3!!.isEmpty()) {
            input_iSiSt3.error = "Injection Setting Speed 3 tidak boleh kosong"
            return
        }
        if (iSt4!!.isEmpty()) {
            input_iSiSt4.error = "Injection Setting Speed 4 tidak boleh kosong"
            return
        }
        if (iSt5!!.isEmpty()) {
            input_iSiSt5.error = "Injection Setting Stroke 5 tidak boleh kosong"
            return
        }
        if (iSt6!!.isEmpty()) {
            input_iSiSt6.error = "Injection Setting Stroke 6 tidak boleh kosong"
            return
        }
    }

    private fun fillMoldSetting() {
        stepper.goToPreviousStep()
    }

    override fun handleOnClick() {
    }


}