package com.jsl.stepper.ui.wo

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.jsl.stepper.data.remote.MainRepository
import com.jsl.stepper.data.remote.MainService
import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.wo.WoData
import com.jsl.stepper.data.remote.response.wo.WoDetailData

class WoLisViewModel @ViewModelInject constructor(mainService: MainService) : ViewModel() {
    private val repoMain =
        MainRepository(mainService)
    val loading: LiveData<Boolean> get() = repoMain.loading

    fun list(): LiveData<BaseResponse<MutableList<WoData>>> =
        repoMain.woList()

    fun detail(woId: String,page:Int): LiveData<BaseResponse<MutableList<WoDetailData>>> =
        repoMain.woDetail(woId,page)

    fun detailOther(page:Int,type: String): LiveData<BaseResponse<MutableList<WoDetailData>>> =
        repoMain.woDetailOther(page,type)


}