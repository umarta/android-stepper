package com.jsl.stepper.ui.setup.mold

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsl.stepper.data.remote.MainRepository
import com.jsl.stepper.data.remote.MainService
import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.setup.MoldDetail
import com.jsl.stepper.data.remote.response.wo.WoDetailData

class MoldViewModel @ViewModelInject constructor(mainService: MainService) : ViewModel() {
    private val repoMain =
        MainRepository(mainService)
    val loading: LiveData<Boolean> get() = repoMain.loading
    fun getWoList(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getWO()

    fun getWoDetail(query: String): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getWoDetail(query)
    private val _woDetail = MutableLiveData<WoDetailData>()
    val woDetail: LiveData<WoDetailData> get() = _woDetail

    fun submit(request: HashMap<String, Any>): MutableLiveData<BaseResponse<MoldDetail>> =
        repoMain.submitSetupMold(request)
    fun setWoDetail(woDetailData: WoDetailData) {
        _woDetail.value = woDetailData
    }


}