package com.jsl.stepper.ui.stepper

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsl.stepper.data.remote.MainRepository
import com.jsl.stepper.data.remote.MainService
import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.model.*
import com.jsl.stepper.model.stepper.*

class InjectionSettingViewModel @ViewModelInject constructor(mainService: MainService) :
    ViewModel() {
    private val repo =
        MainRepository(mainService)
    private val _isCreated = MutableLiveData<Boolean>()


    private val _isMop = MutableLiveData<IsMoP>()
    val isMoP: LiveData<IsMoP> get() = _isMop

    private val _isMos = MutableLiveData<IsMoS>()
    val isMoS: LiveData<IsMoS> get() = _isMos

    private val _isMosT = MutableLiveData<IsMoSt>()
    val isMoSt: LiveData<IsMoSt> get() = _isMosT

    private val _isMcP = MutableLiveData<IsMcP>()
    val isMcP: LiveData<IsMcP> get() = _isMcP

    private val _isMcS = MutableLiveData<IsMcS>()
    val isMcS: LiveData<IsMcS> get() = _isMcS

    private val _isMcSt = MutableLiveData<IsMcSt>()
    val isMcSt: LiveData<IsMcSt> get() = _isMcSt

    private val _woDetail = MutableLiveData<WoDetailData>()
    val woDetail: LiveData<WoDetailData> get() = _woDetail


    private val _moldSetting = MutableLiveData<MoldSetting>()
    val moldSetting: LiveData<MoldSetting> get() = _moldSetting
    val loading: LiveData<Boolean> get() = repo.loading
    val isCreated: LiveData<Boolean> get() = _isCreated

    private val _ejectorSetting = MutableLiveData<EjectorSetting>()
    val ejectorSetting: LiveData<EjectorSetting> get() = _ejectorSetting

    private val _injectionSetting = MutableLiveData<InjectionSetting>()
    val injectionSetting: LiveData<InjectionSetting> get() = _injectionSetting

    private val _plastizing = MutableLiveData<Plastizing>()
    val plastizing: LiveData<Plastizing> get() = _plastizing

    private val _temperatureSetting = MutableLiveData<TemperatureSetting>()
    val temperatureSetting: LiveData<TemperatureSetting> get() = _temperatureSetting


    private val _page = MutableLiveData<Int>()
    val page: LiveData<Int> get() = _page
    fun setPage(int: Int) {
        _page.value = int
    }

    fun setIsMoP(isMoP: IsMoP) {
        _isMop.value = isMoP
    }

    fun setIsMoS(isMoS: IsMoS) {
        _isMos.value = isMoS
    }

    fun setIsMoSt(isMoSt: IsMoSt) {
        _isMosT.value = isMoSt
    }

    fun setIsMcP(isMcP: IsMcP) {
        _isMcP.value = isMcP
    }

    fun setIsMcS(isMcS: IsMcS) {
        _isMcS.value = isMcS
    }

    fun setIsMcSt(isMcSt: IsMcSt) {
        _isMcSt.value = isMcSt
    }

    fun setWoDetail(woDetailData: WoDetailData) {
        _woDetail.value = woDetailData
    }


    fun setMoldSetting(moldSetting: MoldSetting) {
        _moldSetting.value = moldSetting
    }

    fun setEjectorSetting(ejectorSetting: EjectorSetting) {
        _ejectorSetting.value = ejectorSetting
    }

    fun setInjectionSetting(injectionSetting: InjectionSetting) {
        _injectionSetting.value = injectionSetting
    }

    fun setPlastizing(plastizing: Plastizing) {
        _plastizing.value = plastizing
    }

    fun setTemperatureSetting(temperatureSetting: TemperatureSetting) {
        _temperatureSetting.value = temperatureSetting
    }

    fun submit(request: HashMap<String, Any>): LiveData<BaseResponse<Any>> = repo.submit(request)


    fun getWoDetail() {
    }

    fun colour(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repo.colour()

    fun settingDetail(settingId: Int): LiveData<BaseResponse<SettingDetail>> =
        repo.settingDetail(settingId)


    fun jenisProses(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repo.jenisProses()

    fun material(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repo.material()

    fun setIsCreated(boolean: Boolean) {
        _isCreated.value = boolean
    }

    fun getMaterial(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repo.material()
    fun getMaterialGrade(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repo.getMaterialGrade()
    fun getMaterialGradeRemark(materianId:String,gradeId: String): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repo.getMaterialGradeRemark(materianId,gradeId)

}