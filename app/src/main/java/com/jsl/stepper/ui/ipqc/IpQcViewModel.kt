package com.jsl.stepper.ui.ipqc

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsl.stepper.data.remote.MainRepository
import com.jsl.stepper.data.remote.MainService
import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.ipqc.IpQcData

class IpQcViewModel @ViewModelInject constructor(mainService: MainService) : ViewModel() {
    private val repoMain =
        MainRepository(mainService)
    val loading: LiveData<Boolean> get() = repoMain.loading
    fun getMachine(query: String): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getMachine()

    private val _ipqcDetail = MutableLiveData<IpQcData>()
    private val imageCaptured = MutableLiveData<String>()
    val ipqcDetail: LiveData<IpQcData> get() = _ipqcDetail

    fun setIpQcDetailData(ipQcData: IpQcData) {
        _ipqcDetail.value = ipQcData
    }

    fun getDetail(id: Long): LiveData<BaseResponse<IpQcData>> =
        repoMain.getIpQcDetail(id)

    fun getMaterialGrade(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getMaterialGrade()

    fun getMaterialGradeRemark(materialId:String,gradeId: String): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getMaterialGradeRemark(materialId,gradeId)

    fun getColour(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.colour()

    fun submit(id:Long,request: HashMap<String, Any>): MutableLiveData<BaseResponse<Any>> =
        repoMain.submitIpQc(id,request)


}