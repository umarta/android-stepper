package com.jsl.stepper.ui.injection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jsl.stepper.R
import com.jsl.stepper.model.EjectorSetting
import com.jsl.stepper.model.TemperatureSetting
import com.jsl.stepper.ui.stepper.InjectionSettingViewModel
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.utils.Utils.disableEditText
import kotlinx.android.synthetic.main.fragment_temperature.*

class TemperatureFragment : Fragment(), BaseApp.Listener {
    private lateinit var viewModel: InjectionSettingViewModel
    private lateinit var temperatureSetting: TemperatureSetting
    var isCreated: Boolean = false

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(InjectionSettingViewModel::class.java)
        BaseApp(this).set()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_temperature, container, false)
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        fab_next.setOnClickListener {
            validate()
        }
        fab_prev.setOnClickListener {
            viewModel.setPage(InjectionActivity.PLASTIZING_SETTING)

        }
    }

    override fun setAdapter() {
    }

    override fun loadData() {
    }

    override fun setContent() {
        viewModel.temperatureSetting.observe(this, Observer {
            temperatureSetting = it
        })
        viewModel.isCreated.observe(this, {
            isCreated = it
            if (isCreated) {
                setData()
            }
        })

    }

    private fun setData(){
        disableEditText(input_tSTbAHn)
        disableEditText(input_tSTbAH1)
        disableEditText(input_tSTbAH2)
        disableEditText(input_tSTbAH3)
        disableEditText(input_tSTbAH4)
        disableEditText(input_tSTbAH5)
        disableEditText(input_tSTbAH6)
        disableEditText(input_tSTbSHn)
        disableEditText(input_tSTbSH1)
        disableEditText(input_tSTbSH2)
        disableEditText(input_tSTbSH3)
        disableEditText(input_tSTbSH4)
        disableEditText(input_tSTbSH5)
        disableEditText(input_tSTbSH6)
        disableEditText(input_tStHA)
        disableEditText(input_tStMA)
        disableEditText(input_tStHS)
        disableEditText(input_tStMS)

        input_tSTbAHn.setText(temperatureSetting.temperatureBarrel.actual.tSTbAHn.toString())
        input_tSTbAH1.setText(temperatureSetting.temperatureBarrel.actual.tSTbAH1.toString())
        input_tSTbAH2.setText(temperatureSetting.temperatureBarrel.actual.tSTbAH2.toString())
        input_tSTbAH3.setText(temperatureSetting.temperatureBarrel.actual.tSTbAH3.toString())
        input_tSTbAH4.setText(temperatureSetting.temperatureBarrel.actual.tSTbAH4.toString())
        input_tSTbAH5.setText(temperatureSetting.temperatureBarrel.actual.tSTbAH5.toString())
        input_tSTbAH6.setText(temperatureSetting.temperatureBarrel.actual.tSTbAH6.toString())
        input_tSTbSHn.setText(temperatureSetting.temperatureBarrel.setting.tSTbSHn.toString())
        input_tSTbSH1.setText(temperatureSetting.temperatureBarrel.setting.tSTbSH1.toString())
        input_tSTbSH2.setText(temperatureSetting.temperatureBarrel.setting.tSTbSH2.toString())
        input_tSTbSH3.setText(temperatureSetting.temperatureBarrel.setting.tSTbSH3.toString())
        input_tSTbSH4.setText(temperatureSetting.temperatureBarrel.setting.tSTbSH4.toString())
        input_tSTbSH5.setText(temperatureSetting.temperatureBarrel.setting.tSTbSH5.toString())
        input_tSTbSH6.setText(temperatureSetting.temperatureBarrel.setting.tSTbSH6.toString())
        input_tStHA.setText(temperatureSetting.temperatureHopper.actual.tStHA.toString())
        input_tStMA.setText(temperatureSetting.temperatureMtc.actual.tStMA.toString())
        input_tStHS.setText(temperatureSetting.temperatureHopper.setting.tStHS.toString())
        input_tStMS.setText(temperatureSetting.temperatureMtc.setting.tStMS.toString())


    }

    private fun validate() {
        val tSTbAHn = input_tSTbAHn.text.toString()
        val tSTbAH1 = input_tSTbAH1.text.toString()
        val tSTbAH2 = input_tSTbAH2.text.toString()
        val tSTbAH3 = input_tSTbAH3.text.toString()
        val tSTbAH4 = input_tSTbAH4.text.toString()
        val tSTbAH5 = input_tSTbAH5.text.toString()
        val tSTbAH6 = input_tSTbAH6.text.toString()
        val tSTbSHn = input_tSTbSHn.text.toString()
        val tSTbSH1 = input_tSTbSH1.text.toString()
        val tSTbSH2 = input_tSTbSH2.text.toString()
        val tSTbSH3 = input_tSTbSH3.text.toString()
        val tSTbSH4 = input_tSTbSH4.text.toString()
        val tSTbSH5 = input_tSTbSH5.text.toString()
        val tSTbSH6 = input_tSTbSH6.text.toString()
        val tStHA = input_tStHA.text.toString()
        val tStMA = input_tStMA.text.toString()
        val tStHS = input_tStHS.text.toString()
        val tStMS = input_tStMS.text.toString()
        if (tSTbAHn!!.isEmpty()) {
            input_tSTbAHn.error = "Actual Hn n tidak boleh kosong"
            return
        }
        if (tSTbAH1!!.isEmpty()) {
            input_tSTbAH1.error = "Actual H1 1 tidak boleh kosong"
            return
        }
        if (tSTbAH2!!.isEmpty()) {
            input_tSTbAH2.error = "Actual H2 2 tidak boleh kosong"
            return
        }
        if (tSTbAH3!!.isEmpty()) {
            input_tSTbAH3.error = "Actual H3 3 tidak boleh kosong"
            return
        }
        if (tSTbAH4!!.isEmpty()) {
            input_tSTbAH4.error = "Actual H4 4 tidak boleh kosong"
            return
        }
        if (tSTbAH5!!.isEmpty()) {
            input_tSTbAH5.error = "Actual H5 5 tidak boleh kosong"
            return
        }
        if (tSTbAH6!!.isEmpty()) {
            input_tSTbAH6.error = "Actual H6 6 tidak boleh kosong"
            return
        }
        if (tSTbSHn!!.isEmpty()) {
            input_tSTbSHn.error = "Setting Hn n tidak boleh kosong"
            return
        }
        if (tSTbSH1!!.isEmpty()) {
            input_tSTbSH1.error = "Setting H1 1 tidak boleh kosong"
            return
        }
        if (tSTbSH2!!.isEmpty()) {
            input_tSTbSH2.error = "Setting H2 2 tidak boleh kosong"
            return
        }
        if (tSTbSH3!!.isEmpty()) {
            input_tSTbSH3.error = "Setting H3 3 tidak boleh kosong"
            return
        }
        if (tSTbSH4!!.isEmpty()) {
            input_tSTbSH4.error = "Setting H4 4 tidak boleh kosong"
            return
        }
        if (tSTbSH5!!.isEmpty()) {
            input_tSTbSH5.error = "Setting H5 5 tidak boleh kosong"
            return
        }
        if (tSTbSH6!!.isEmpty()) {
            input_tSTbSH6.error = "Setting H6 6 tidak boleh kosong"
            return
        }
        if (tStHA!!.isEmpty()) {
            input_tStHA.error = "Actual Hopper A tidak boleh kosong"
            return
        }
        if (tStMA!!.isEmpty()) {
            input_tStMA.error = "Actual MTC A tidak boleh kosong"
            return
        }
        if (tStHS!!.isEmpty()) {
            input_tStHS.error = "Setting Hopper S tidak boleh kosong"
            return
        }
        if (tStMS!!.isEmpty()) {
            input_tStMS.error = "Setting MTC S tidak boleh kosong"
            return
        }
        val tSTbAH = TemperatureSetting.TemperatureBarrel.Actual(
            tSTbAHn.toInt(),
            tSTbAH1.toInt(),
            tSTbAH2.toInt(),
            tSTbAH3.toInt(),
            tSTbAH4.toInt(),
            tSTbAH5.toInt(),
            tSTbAH6.toInt()
        )

        val tSTbSH = TemperatureSetting.TemperatureBarrel.Setting(
            tSTbSHn.toInt(),
            tSTbSH1.toInt(),
            tSTbSH2.toInt(),
            tSTbSH3.toInt(),
            tSTbSH4.toInt(),
            tSTbSH5.toInt(),
            tSTbSH6.toInt()
        )
        val tB = TemperatureSetting.TemperatureBarrel(tSTbAH, tSTbSH)

        val tStHAA = TemperatureSetting.TemperatureHopper.Actual(tStHA.toInt())
        val tStHSS = TemperatureSetting.TemperatureHopper.Setting(tStHS.toInt())

        val tStMAA = TemperatureSetting.TemperatureMtc.Actual(tStMA.toInt())
        val tStMSS = TemperatureSetting.TemperatureMtc.Setting(tStMS.toInt())

        val tH = TemperatureSetting.TemperatureHopper(tStHAA, tStHSS)
        val tM = TemperatureSetting.TemperatureMtc(tStMAA, tStMSS)

        val setting = TemperatureSetting(tM, tB, tH)
        viewModel.setTemperatureSetting(setting)
        viewModel.setPage(InjectionActivity.REVIEW)
    }
}