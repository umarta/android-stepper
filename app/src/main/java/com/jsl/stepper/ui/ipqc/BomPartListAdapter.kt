package com.jsl.stepper.ui.ipqc

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.ipqc.BomPartData
import kotlinx.android.synthetic.main.view_bom_part_list.view.*
import java.util.*

class BomPartListAdapter(
    private val activity: Activity,
    private var data: MutableList<BomPartData>,
    val listener: BomPartListAdapter.Listener,


    ) : RecyclerView.Adapter<BomPartListAdapter.Holder>() {
    val res = HashMap<String, Any>()

    fun setItems(data: MutableList<BomPartData>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<BomPartData>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BomPartListAdapter.Holder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.view_bom_part_list, parent, false)
        return Holder(view)

    }

    override fun onBindViewHolder(holder: BomPartListAdapter.Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    override fun getItemCount(): Int = data.size

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(mData: BomPartData, position: Int, listener: BomPartListAdapter.Listener) {
            with(itemView) {
                tv_kode_part.setText(mData.kode_part)
                input_berat.setHint(mData.kode_part.toString())
                input_berat.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable) {
                        Log.d(mData.kode_part, s.toString())
                    }

                    override fun beforeTextChanged(
                        s: CharSequence, start: Int,
                        count: Int, after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence, start: Int,
                        before: Int, count: Int
                    ) {
                        val kode_part = mData.kode_part.toString()
                        res[kode_part] = s.toString()
                    }
                })
            }
        }
    }

    interface Listener {
        fun onLoadBom(data: BomPartData)
    }

}