package com.jsl.stepper.ui.injection

import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jsl.stepper.R
import com.jsl.stepper.model.EjectorSetting
import com.jsl.stepper.model.InjectionSetting
import com.jsl.stepper.ui.stepper.InjectionSettingViewModel
import com.jsl.stepper.utils.BaseApp
import kotlinx.android.synthetic.main.fragment_injection_setting.*

class InjectionSettingFragment : Fragment(), BaseApp.Listener {
    private lateinit var viewModel: InjectionSettingViewModel
    private lateinit var injectionSetting: InjectionSetting
    private lateinit var ejectorSetting: EjectorSetting
    var isCreated: Boolean = false

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(InjectionSettingViewModel::class.java)
        BaseApp(this).set()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_injection_setting, container, false)
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        fab_next.setOnClickListener {
            validate()
        }

        fab_prev.setOnClickListener {
            viewModel.setPage(InjectionActivity.PREVIEW)

        }

    }

    private fun validate() {
        val iSiP6 = input_iSiP6.text.toString()
        val iSiP5 = input_iSiP5.text.toString()
        val iSiP4 = input_iSiP4.text.toString()
        val iSiP3 = input_iSiP3.text.toString()
        val iSiP2 = input_iSiP2.text.toString()
        val iSiP1 = input_iSiP1.text.toString()
        val iSiS6 = input_iSiS6.text.toString()
        val iSiS5 = input_iSiS5.text.toString()
        val iSiS4 = input_iSiS4.text.toString()
        val iSiS3 = input_iSiS3.text.toString()
        val iSiS2 = input_iSiS2.text.toString()
        val iSiS1 = input_iSiS1.text.toString()
        val iSiSt6 = input_iSiSt6.text.toString()
        val iSiSt5 = input_iSiSt5.text.toString()
        val iSiSt4 = input_iSiSt4.text.toString()
        val iSiSt3 = input_iSiSt3.text.toString()
        val iSiSt2 = input_iSiSt2.text.toString()
        val iSiSt1 = input_iSiSt1.text.toString()
        val iSiT1 = input_iSiT1.text.toString()
        val iShP1 = input_iShP1.text.toString()
        val iShP2 = input_iShP2.text.toString()
        val iShP3 = input_iShP3.text.toString()
        val iShP4 = input_iShP4.text.toString()
        val iShP5 = input_iShP5.text.toString()
        val iShS1 = input_iShS1.text.toString()
        val iShS2 = input_iShS2.text.toString()
        val iShS3 = input_iShS3.text.toString()
        val iShS4 = input_iShS4.text.toString()
        val iShS5 = input_iShS5.text.toString()
        val iShT1 = input_iShT1.text.toString()
        val iShT2 = input_iShT2.text.toString()
        val iShT3 = input_iShT3.text.toString()
        val iShT4 = input_iShT4.text.toString()
        val iShT5 = input_iShT5.text.toString()

        if (iSiP6!!.isEmpty()) {
            input_iSiP6.error = "Injection Press  1 tidak boleh kosong"
            return
        }
        if (iSiP5!!.isEmpty()) {
            input_iSiP5.error = "Injection Press  1 tidak boleh kosong"
            return
        }
        if (iSiP4!!.isEmpty()) {
            input_iSiP4.error = "Injection Press  1 tidak boleh kosong"
            return
        }
        if (iSiP3!!.isEmpty()) {
            input_iSiP3.error = "Injection Press  1 tidak boleh kosong"
            return
        }
        if (iSiP2!!.isEmpty()) {
            input_iSiP2.error = "Injection Press  1 tidak boleh kosong"
            return
        }
        if (iSiP1!!.isEmpty()) {
            input_iSiP1.error = "Injection Press  1 tidak boleh kosong"
            return
        }
        if (iSiS6!!.isEmpty()) {
            input_iSiS6.error = "Injection Speed  1 tidak boleh kosong"
            return
        }
        if (iSiS5!!.isEmpty()) {
            input_iSiS5.error = "Injection Speed  1 tidak boleh kosong"
            return
        }
        if (iSiS4!!.isEmpty()) {
            input_iSiS4.error = "Injection Speed  1 tidak boleh kosong"
            return
        }
        if (iSiS3!!.isEmpty()) {
            input_iSiS3.error = "Injection Speed  1 tidak boleh kosong"
            return
        }
        if (iSiS2!!.isEmpty()) {
            input_iSiS2.error = "Injection Speed  1 tidak boleh kosong"
            return
        }
        if (iSiS1!!.isEmpty()) {
            input_iSiS1.error = "Injection Speed  1 tidak boleh kosong"
            return
        }
        if (iSiSt6!!.isEmpty()) {
            input_iSiSt6.error = "Injection Stroke  1 tidak boleh kosong"
            return
        }
        if (iSiSt5!!.isEmpty()) {
            input_iSiSt5.error = "Injection Stroke  1 tidak boleh kosong"
            return
        }
        if (iSiSt4!!.isEmpty()) {
            input_iSiSt4.error = "Injection Stroke  1 tidak boleh kosong"
            return
        }
        if (iSiSt3!!.isEmpty()) {
            input_iSiSt3.error = "Injection Stroke  1 tidak boleh kosong"
            return
        }
        if (iSiSt2!!.isEmpty()) {
            input_iSiSt2.error = "Injection Stroke  1 tidak boleh kosong"
            return
        }
        if (iSiSt1!!.isEmpty()) {
            input_iSiSt1.error = "Injection Stroke  1 tidak boleh kosong"
            return
        }
        if (iSiT1!!.isEmpty()) {
            input_iSiT1.error = "Injection Time  1 tidak boleh kosong"
            return
        }
        if (iShP1!!.isEmpty()) {
            input_iShP1.error = "Holding Press  1 tidak boleh kosong"
            return
        }
        if (iShP2!!.isEmpty()) {
            input_iShP2.error = "Holding Press  1 tidak boleh kosong"
            return
        }
        if (iShP3!!.isEmpty()) {
            input_iShP3.error = "Holding Press  1 tidak boleh kosong"
            return
        }
        if (iShP4!!.isEmpty()) {
            input_iShP4.error = "Holding Press  1 tidak boleh kosong"
            return
        }
        if (iShP5!!.isEmpty()) {
            input_iShP5.error = "Holding Press  1 tidak boleh kosong"
            return
        }
        if (iShS1!!.isEmpty()) {
            input_iShS1.error = "Holding Speed  1 tidak boleh kosong"
            return
        }
        if (iShS2!!.isEmpty()) {
            input_iShS2.error = "Holding Speed  1 tidak boleh kosong"
            return
        }
        if (iShS3!!.isEmpty()) {
            input_iShS3.error = "Holding Speed  1 tidak boleh kosong"
            return
        }
        if (iShS4!!.isEmpty()) {
            input_iShS4.error = "Holding Speed  1 tidak boleh kosong"
            return
        }
        if (iShS5!!.isEmpty()) {
            input_iShS5.error = "Holding Speed  1 tidak boleh kosong"
            return
        }
        if (iShT1!!.isEmpty()) {
            input_iShT1.error = "Holding Time  1 tidak boleh kosong"
            return
        }
        if (iShT2!!.isEmpty()) {
            input_iShT2.error = "Holding Time  1 tidak boleh kosong"
            return
        }
        if (iShT3!!.isEmpty()) {
            input_iShT3.error = "Holding Time  1 tidak boleh kosong"
            return
        }
        if (iShT4!!.isEmpty()) {
            input_iShT4.error = "Holding Time  1 tidak boleh kosong"
            return
        }
        if (iShT5!!.isEmpty()) {
            input_iShT5.error = "Holding   1 tidak boleh kosong"
            return
        }

        val hP = InjectionSetting.Holding.Press(
            iShP1.toInt(),
            iShP2.toInt(),
            iShP3.toInt(),
            iShP4.toInt(),
            iShP5.toInt()
        )
        val hS = InjectionSetting.Holding.Speed(
            iShS1.toInt(),
            iShS2.toInt(),
            iShS3.toInt(),
            iShS4.toInt(),
            iShS5.toInt()
        )
        val hT = InjectionSetting.Holding.Time(
            iShT1.toInt(),
            iShT2.toInt(),
            iShT3.toInt(),
            iShT4.toInt(),
            iShT5.toInt()
        )

        val iP = InjectionSetting.Injection.Press(
            iSiP1.toInt(),
            iSiP2.toInt(),
            iSiP3.toInt(),
            iSiP4.toInt(),
            iSiP5.toInt(),
            iSiP6.toInt()
        )
        val iS = InjectionSetting.Injection.Speed(
            iSiS1.toInt(),
            iSiS2.toInt(),
            iSiS3.toInt(),
            iSiS4.toInt(),
            iSiS5.toInt(),
            iSiS6.toInt()
        )
        val iSt = InjectionSetting.Injection.Stroke(
            iSiSt1.toInt(),
            iSiSt2.toInt(),
            iSiSt3.toInt(),
            iSiSt4.toInt(),
            iSiSt5.toInt(),
            iSiSt6.toInt()
        )
        val iT = InjectionSetting.Injection.Time(iSiT1.toInt())

        val h = InjectionSetting.Holding(hP, hS, hT)
        val i = InjectionSetting.Injection(iP, iS, iSt, iT)

        val setting = InjectionSetting(h, i)
        viewModel.setInjectionSetting(setting)
        viewModel.setPage(InjectionActivity.PLASTIZING_SETTING)
    }

    override fun setAdapter() {
    }

    override fun setContent() {
        viewModel.injectionSetting.observe(this, Observer {
            injectionSetting = it
            disableEditText(input_iSiP6)
            disableEditText(input_iSiP5)
            disableEditText(input_iSiP4)
            disableEditText(input_iSiP3)
            disableEditText(input_iSiP2)
            disableEditText(input_iSiP1)
            disableEditText(input_iSiS6)
            disableEditText(input_iSiS5)
            disableEditText(input_iSiS4)
            disableEditText(input_iSiS3)
            disableEditText(input_iSiS2)
            disableEditText(input_iSiS1)
            disableEditText(input_iSiSt6)
            disableEditText(input_iSiSt5)
            disableEditText(input_iSiSt4)
            disableEditText(input_iSiSt3)
            disableEditText(input_iSiSt2)
            disableEditText(input_iSiSt1)
            disableEditText(input_iSiT1)
            disableEditText(input_iShP1)
            disableEditText(input_iShP2)
            disableEditText(input_iShP3)
            disableEditText(input_iShP4)
            disableEditText(input_iShP5)
            disableEditText(input_iShS1)
            disableEditText(input_iShS2)
            disableEditText(input_iShS3)
            disableEditText(input_iShS4)
            disableEditText(input_iShS5)
            disableEditText(input_iShT1)
            disableEditText(input_iShT2)
            disableEditText(input_iShT3)
            disableEditText(input_iShT4)
            disableEditText(input_iShT5)
            input_iSiP6.setText(injectionSetting.injection.press.Isip1.toString())
            input_iSiP5.setText(injectionSetting.injection.press.Isip2.toString())
            input_iSiP4.setText(injectionSetting.injection.press.Isip3.toString())
            input_iSiP3.setText(injectionSetting.injection.press.Isip4.toString())
            input_iSiP2.setText(injectionSetting.injection.press.Isip5.toString())
            input_iSiP1.setText(injectionSetting.injection.press.Isip6.toString())
            input_iSiS6.setText(injectionSetting.injection.speed.Issp1.toString())
            input_iSiS5.setText(injectionSetting.injection.speed.Issp2.toString())
            input_iSiS4.setText(injectionSetting.injection.speed.Issp3.toString())
            input_iSiS3.setText(injectionSetting.injection.speed.Issp4.toString())
            input_iSiS2.setText(injectionSetting.injection.speed.Issp5.toString())
            input_iSiS1.setText(injectionSetting.injection.speed.Issp6.toString())
            input_iSiSt6.setText(injectionSetting.injection.stroke.Isstp1.toString())
            input_iSiSt5.setText(injectionSetting.injection.stroke.Isstp2.toString())
            input_iSiSt4.setText(injectionSetting.injection.stroke.Isstp3.toString())
            input_iSiSt3.setText(injectionSetting.injection.stroke.Isstp4.toString())
            input_iSiSt2.setText(injectionSetting.injection.stroke.Isstp5.toString())
            input_iSiSt1.setText(injectionSetting.injection.stroke.Isstp6.toString())
            input_iSiT1.setText(injectionSetting.injection.time.Istp1.toString())
            input_iShP1.setText(injectionSetting.holding.press.Ishp1.toString())
            input_iShP2.setText(injectionSetting.holding.press.Ishp2.toString())
            input_iShP3.setText(injectionSetting.holding.press.Ishp3.toString())
            input_iShP4.setText(injectionSetting.holding.press.Ishp4.toString())
            input_iShP5.setText(injectionSetting.holding.press.Ishp5.toString())
            input_iShS1.setText(injectionSetting.holding.speed.Ishs1.toString())
            input_iShS2.setText(injectionSetting.holding.speed.Ishs2.toString())
            input_iShS3.setText(injectionSetting.holding.speed.Ishs3.toString())
            input_iShS4.setText(injectionSetting.holding.speed.Ishs4.toString())
            input_iShS5.setText(injectionSetting.holding.speed.Ishs5.toString())
            input_iShT1.setText(injectionSetting.holding.time.Isht1.toString())
            input_iShT2.setText(injectionSetting.holding.time.Isht2.toString())
            input_iShT3.setText(injectionSetting.holding.time.Isht3.toString())
            input_iShT4.setText(injectionSetting.holding.time.Isht4.toString())
            input_iShT5.setText(injectionSetting.holding.time.Isht5.toString())

        })

        viewModel.isCreated.observe(this, {
            isCreated = it
            if (isCreated) {
                setData()
            }
        })

    }

    private fun disableEditText(editText: EditText) {
        editText.isFocusable = false
        editText.isFocusableInTouchMode = false
        editText.isCursorVisible = false
        editText.keyListener = null
        editText.inputType = InputType.TYPE_NULL
        editText.setBackgroundColor(Color.TRANSPARENT)
    }

    private fun setData() {

    }

    override fun loadData() {
    }

}