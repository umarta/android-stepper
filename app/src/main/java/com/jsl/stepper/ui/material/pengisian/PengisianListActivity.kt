package com.jsl.stepper.ui.material.pengisian

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.material.PengisianListData
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.ui.wo.WoListAdapter
import com.jsl.stepper.utils.BaseApp
import com.jsl.stepper.utils.hide
import com.jsl.stepper.utils.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_pengisian_list.*
import kotlinx.android.synthetic.main.activity_pengisian_list.iv_back
import kotlinx.android.synthetic.main.activity_pengisian_list.progress_bar
import kotlinx.android.synthetic.main.activity_pengisian_list.rv_wo_detail_list
import kotlinx.android.synthetic.main.activity_pengisian_list.swipeContainer
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@AndroidEntryPoint
class PengisianListActivity : AppCompatActivity(), BaseApp.Listener, PengisianListAdapter.Listener,
    FilterBSheet.Listener {
    private val viewModel: MaterialViewModel by viewModels()
    private var page: Int = 1;
    private var next_page = true
    private var loading = false
    private var filtered = false
    private var machineId: Long? = null
    private var materialId: Int? = null
    private var startFrom: String? = null
    private var endAt: String? = null
    private lateinit var filterBSheet: FilterBSheet
    private lateinit var mesinList: MutableList<IdNameData>;

    private lateinit var pengisianListAdapter: PengisianListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pengisian_list)
        BaseApp(this).set()
    }

    override fun getIntentData() {

    }

    override fun setOnClick() {
        iv_back.setOnClickListener {
            val i = Intent(this, HomeActivity::class.java)
            startActivity(i)
        }
        swipeContainer.setOnRefreshListener {
            pengisianListAdapter.clear()
            page = 1
            loadData()
            swipeContainer.setRefreshing(false)
        }

        swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        );

        iv_add.setOnClickListener {
            val i = Intent(this, PengisianMaterialActivity::class.java)
            startActivity(i)
        }

        ly_sort.setOnClickListener {
            filterBSheet = FilterBSheet(this, mesinList, ArrayList())
            filterBSheet.show(supportFragmentManager, FilterBSheet::class.java.name)
        }

    }

    override fun setAdapter() {
        pengisianListAdapter = PengisianListAdapter(this, ArrayList(), this)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        rv_wo_detail_list.adapter = pengisianListAdapter
        rv_wo_detail_list.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_wo_detail_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val countItem = layoutManager.itemCount
                val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                if (!recyclerView.canScrollVertically(1)) {
                    if (next_page) {
                        if (!loading && isLastPosition) {
                            page++
                            loadData()
                        }
                    }
                }
            }
        })
    }

    fun fetchData() {
        if (!filtered) {
            viewModel.getPengisianList(page).observe(this, Observer {
                if (it.success!!) {
                    next_page = it.header.next_page
                    if (page == 1) {
                        pengisianListAdapter.setItems(it.data)
                    } else {
                        pengisianListAdapter.addItems(it.data)
                    }
                }
            })
        } else {
            viewModel.getPengisianList(page, machineId, materialId, startFrom, endAt)
                .observe(this, Observer {
                    if (it.success!!) {
                        next_page = it.header.next_page
                        if (page == 1) {
                            pengisianListAdapter.setItems(it.data)
                        } else {
                            pengisianListAdapter.addItems(it.data)
                        }
                    }
                })
        }

    }

    override fun loadData() {

        fetchData()
        viewModel.getMachineList().observe(this, Observer {
            if (it.success!!) {
                mesinList = it.data
            }
        })
    }

    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) progress_bar.show()
            else progress_bar.hide()
        })
    }

    override fun onLoadList(data: PengisianListData) {
        finish()
    }

    override fun onDate(string: String) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in TextView
                var monthFinal: String;
                month = (month + 1)
                if (month.toString().length == 1) {
                    monthFinal = "0" + month.toString()
                } else {
                    monthFinal = month.toString()

                }
                if (string === "start") {
                    filterBSheet.setStart("$monthFinal-$dayOfMonth-$year")
                } else {
                    filterBSheet.setEnd("$monthFinal-$dayOfMonth-$year")
                }
            },
            year,
            month,
            day
        )
        dpd.show()
    }

    override fun onMesin(id: Long) {
        viewModel.getMaterialByMachine(id).observe(this, Observer {
            if (it.success!!) {
                filterBSheet.setMaterial(it.data)
            }
        })
    }

    override fun onAction(machine: String, material: String, start: String, end: String) {
        filtered = true
        machineId = machine.toLong()
        materialId = material.toInt()
        startFrom = start
        endAt = end
        fetchData()
        Log.d("sa", "$machine-$material-$start-$end")

    }
}