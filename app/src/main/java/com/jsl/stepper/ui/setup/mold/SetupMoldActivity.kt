package com.jsl.stepper.ui.setup.mold

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.stepper.R
import com.jsl.stepper.data.local.WoDetailObject
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.ui.dialog.CloseDialog
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.ui.material.pengisian.MaterialViewModel
import com.jsl.stepper.ui.wo.detail.WoDetailActivity
import com.jsl.stepper.utils.*
import com.jsl.stepper.utils.Constants.TIMER_INTERVAL
import com.jsl.stepper.utils.Utils.getFormattedStopWatch
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_setup_mold.*

@AndroidEntryPoint
class SetupMoldActivity : AppCompatActivity(), BaseApp.Listener, CloseDialog.Listener {
    private val mInterval = TIMER_INTERVAL
    private lateinit var wo_id: String

    private var mHandler: Handler? = null
    private lateinit var wo: IdNameData
    private lateinit var woDetail: IdNameData
    private lateinit var machine: IdNameData
    private lateinit var woDetailData: WoDetailData

    private val viewModel: MoldViewModel by viewModels()
    private val viewModelMaterial: MaterialViewModel by viewModels()

    private var timeInSeconds = 0L
    private var startButtonClicked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup_mold)
        initStopWatch()
        BaseApp(this).set()
    }

    override fun getIntentData() {
        if (intent.hasExtra("object")) {
            val woDetailData = intent.getSerializableExtra("object") as? WoDetailObject
            val woData =
                WoDetailData(
                    id = woDetailData?.id,
                    sequenceno = woDetailData?.sequenceno,
                    molding_id = woDetailData?.molding_id,
                    code = woDetailData?.code,
                    name = woDetailData?.name,
                    cavity = woDetailData?.cavity,
                    tonase = woDetailData?.tonase,
                    size = woDetailData?.size,
                    description = woDetailData?.description,
                    colour = woDetailData?.colour,
                    category = woDetailData?.category,
                    merek = woDetailData?.merek,
                    brand = woDetailData?.brand,
                    sub_brand = woDetailData?.sub_brand,
                    status = woDetailData?.status,
                    location = woDetailData?.location,
                    machine = woDetailData?.machine,
                    jenis_proses = woDetailData?.jenis_proses,
                    cycle_time = woDetailData?.cycle_time,
                    material = woDetailData?.material,
                    womolding_id = woDetailData?.womolding_id,
                    is_created = woDetailData?.is_created,
                    product_name = woDetailData?.product_name,
                    kode_part = woDetailData?.kode_part
                )
            viewModel.setWoDetail(woData)

        }

        if (intent.hasExtra("wo_id")) {
            wo_id = intent.getStringExtra("wo_id").toString()
        }


    }

    override fun setOnClick() {
//        resetButton.setOnClickListener {
//            stopTimer()
//            resetTimerView()
//        }
    }

    override fun setAdapter() {
    }

    override fun loadData() {

    }

    @SuppressLint("SetTextI18n")
    override fun setContent() {
        viewModel.woDetail.observe(this, Observer {
            woDetailData = it
            kode_mold.text = woDetailData.code
            input_kode_part.setText(woDetailData.code.toString())
            tv_setup_mold_title.text = "Setup Mold - " + woDetailData.product_name
            checkStatus()

        })

        no_wo.text = wo_id
        machine()
        sp_no_mesin.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                machine = item
            }
        )
    }

    private fun resetTimerView() {
        showCloseDialog()
    }

    private fun initStopWatch() {
        textViewStopWatch?.text = getString(R.string.init_stop_watch_value)
    }

    private fun startTimer() {
        mHandler = Handler(Looper.getMainLooper())
        mStatusChecker.run()
    }

    private fun showCloseDialog() {
        val fm: FragmentManager = supportFragmentManager
        val dialog = CloseDialog("Setup telah selesai ?", this)
        dialog.show(fm, CloseDialog::javaClass.name)
    }

    private var mStatusChecker: Runnable = object : Runnable {
        override fun run() {
            try {
                timeInSeconds += 1
                Log.e("timeInSeconds", timeInSeconds.toString())
                updateStopWatchView(timeInSeconds)
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler!!.postDelayed(this, mInterval.toLong())
            }
        }
    }

    private fun updateStopWatchView(timeInSeconds: Long) {
        val formattedTime = getFormattedStopWatch((timeInSeconds * 1000))
        Log.e("formattedTime", formattedTime)
        textViewStopWatch?.text = formattedTime
    }

    private fun startTimerView() {

        startOrStopTextView?.setBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.red
            )
        )
        startOrStopTextView?.setText(R.string.finish)
        startButtonClicked = true

    }

    private fun stopTimer() {
        mHandler?.removeCallbacks(mStatusChecker)
    }

    private fun stopTimerView() {
        startOrStopTextView?.setBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.teal_700
            )
        )
        startOrStopTextView?.setText(R.string.resume)
        startButtonClicked = false
    }

    fun startOrStopButtonClicked(v: View) {
        if (!startButtonClicked) {
            startTimer()
            startTimerView()
            submitData()
        } else {
            stopTimer()
            resetTimerView()
        }
    }


    fun machine() {
        progress_bar.show()
        viewModelMaterial.getMachine("").observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                sp_no_mesin.setAdapter(spinnerAdapter)
            }
            progress_bar.hide()
        })
    }

    private fun checkStatus() {
        viewModelMaterial.getMoldDetail(wo_id, woDetailData.id.toString()).observe(this, Observer {
            if (it.success!!) {
                if (it.data != null) {
                    timeInSeconds = it.data.duration!!
                    startTimer()
                    startTimerView()
                    machine = it.data.machine!!
                    input_kode_part.isEnabled = false
                    sp_no_mesin.isEnabled = false
                    sp_no_mesin.setText(it.data.machine!!.name)
                }
            }
        })
    }

    private fun submitData() {
        progress_bar.show()
        val res = HashMap<String, Any>()
        res["machine_id"] = machine.id
        res["wo_id"] = wo_id
        res["wo_mold_id"] = woDetailData.id.toString()
        res["part_no"] = input_kode_part.text.toString()
        res["duration"] = timeInSeconds.toString()

        viewModel.submit(res).observe(this, Observer {
            progress_bar.hide()
            if (it.success!!) {
                if (it.data.status == "old") {
                    val i = Intent(this, WoDetailActivity::class.java)
                    i.putExtra(Constants.WO_ID, wo_id)
                    this.startActivity(i)
                }
            } else {
                toast(it.message?.first())
            }

        })
    }

    override fun onCloseDialog() {
        submitData()
        timeInSeconds = 0
        startButtonClicked = false
        startOrStopTextView?.setBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.teal_700
            )
        )
        startOrStopTextView?.setText(R.string.start)
        initStopWatch()
    }

    override fun cancelClose() {
        startTimer()
        startTimerView()
    }

}