package com.jsl.stepper.ui.history

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jsl.stepper.R
import com.jsl.stepper.utils.BaseApp

class HistoryActivity : AppCompatActivity(), BaseApp.Listener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
    }

    override fun setAdapter() {
    }

    override fun loadData() {

    }

    override fun setContent() {
    }
}