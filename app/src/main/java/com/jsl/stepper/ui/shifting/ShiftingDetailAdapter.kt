package com.jsl.stepper.ui.shifting

import android.annotation.SuppressLint
import android.app.Activity
import android.app.TimePickerDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.serahterima.SerahTerimaListData
import com.jsl.stepper.data.remote.response.shifting.ShiftingDetail
import kotlinx.android.synthetic.main.view_shifting.view.*
import java.text.SimpleDateFormat
import java.util.*


class ShiftingDetailAdapter(
    private val activity: Activity,
    private var data: MutableList<ShiftingDetail>,
    val listener: ShiftingDetailActivity,
) : RecyclerView.Adapter<ShiftingDetailAdapter.Holder>() {
    val res = HashMap<String, Any>()
    var outerArr = ArrayList<Array<String>>()


    fun setItems(data: MutableList<ShiftingDetail>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<ShiftingDetail>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ShiftingDetailAdapter.Holder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_shifting, parent, false)
        return Holder(view)
    }

    interface Listener {
        fun onLoadData(data: SerahTerimaListData)
        fun onSave(res: HashMap<String, Any>)
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(
            mData: ShiftingDetail,
            position: Int,
            listener: ShiftingDetailAdapter.Listener
        ) {
            with(itemView) {
//                tv_hari.text = mData.hari
//                tv_tanggal.text = mData.tanggal
//
                val shift = HashMap<String, Any>()
                res["jam_masuk"] = mData.jam_masuk
                res["jam_keluar"] = mData.jam_keluar
                res["total_jam_istirahat"] = mData.total_jam_istirahat
                input_jam_masuk.setText(mData.jam_masuk)
                input_jam_Keluar.setText(mData.jam_keluar)
                input_total_jam_istirahat.setText(mData.total_jam_istirahat)
                tv_shift.text = "Shift" + mData.shift
                input_jam_masuk.isFocusable = false
                input_jam_masuk.isClickable = true
                input_jam_Keluar.isFocusable = false
                input_jam_Keluar.isClickable = true
                input_total_jam_istirahat.isFocusable = false
                input_total_jam_istirahat.isClickable = true
                input_jam_masuk.setOnClickListener {
                    val cal = Calendar.getInstance()
                    val timeSetListener =
                        TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                            cal.set(Calendar.HOUR_OF_DAY, hour)
                            cal.set(Calendar.MINUTE, minute)
                            input_jam_masuk.setText(SimpleDateFormat("HH:mm:ss").format(cal.time))
                            res["jam_masuk"] = SimpleDateFormat("HH:mm:ss").format(cal.time)
                        }
                    TimePickerDialog(
                        context,
                        timeSetListener,
                        cal.get(Calendar.HOUR_OF_DAY),
                        cal.get(Calendar.MINUTE),
                        true
                    ).show()
                }
                input_jam_Keluar.setOnClickListener {
                    val cal = Calendar.getInstance()
                    val timeSetListener =
                        TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                            cal.set(Calendar.HOUR_OF_DAY, hour)
                            cal.set(Calendar.MINUTE, minute)
                            input_jam_Keluar.setText(SimpleDateFormat("HH:mm:ss").format(cal.time))
                            res["jam_keluar"] = SimpleDateFormat("HH:mm:ss").format(cal.time)

                        }
                    TimePickerDialog(
                        context,
                        timeSetListener,
                        cal.get(Calendar.HOUR_OF_DAY),
                        cal.get(Calendar.MINUTE),
                        true
                    ).show()
                }
                input_total_jam_istirahat.setOnClickListener {
                    val cal = Calendar.getInstance()
                    val timeSetListener =
                        TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                            cal.set(Calendar.HOUR_OF_DAY, hour)
                            cal.set(Calendar.MINUTE, minute)
                            input_total_jam_istirahat.setText(
                                SimpleDateFormat("HH:mm:ss").format(
                                    cal.time
                                )
                            )
                            res["total_jam_istirahat"] =
                                SimpleDateFormat("HH:mm:ss").format(cal.time)
                        }
                    TimePickerDialog(
                        context,
                        timeSetListener,
                        cal.get(Calendar.HOUR_OF_DAY),
                        cal.get(Calendar.MINUTE),
                        true
                    ).show()
                }
                save.setOnClickListener {
                    res["id"] = mData.id
                    res["tanggal"] = mData.tanggal
                    res["shift"] = mData.shift
                    Log.d("test", res.toString())
                    listener.onSave(res)

                }
            }

            //        pickTimeBtn.setOnClickListener {
//            val cal = Calendar.getInstance()
//            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
//                cal.set(Calendar.HOUR_OF_DAY, hour)
//                cal.set(Calendar.MINUTE, minute)
//                timeTv.text = SimpleDateFormat("HH:mm").format(cal.time)
//            }
//            TimePickerDialog(
//                this,
//                timeSetListener,
//                cal.get(Calendar.HOUR_OF_DAY),
//                cal.get(Calendar.MINUTE),
//                true
//            ).show()
//        }
//            itemView.setOnClickListener {
//                val i = Intent(activity, ShiftingDetailActivity::class.java)
//                val extra = Bundle()
//                i.putExtra("tanggal", mData.tanggal);
//                i.putExtra("hari", mData.hari);
//
//                activity.startActivity(i)
//                activity.overridePendingTransition(R.xml.anim_fade_in, R.xml.anim_fade_out)
//
//            }
        }
    }

    override fun onBindViewHolder(holder: ShiftingDetailAdapter.Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    override fun getItemCount(): Int = data.size
}

