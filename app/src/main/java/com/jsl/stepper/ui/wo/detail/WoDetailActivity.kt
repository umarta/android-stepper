package com.jsl.stepper.ui.wo.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.ui.wo.WoLisViewModel
import com.jsl.stepper.ui.wo.WoListActivity
import com.jsl.stepper.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_wo_detail.*

@AndroidEntryPoint
class WoDetailActivity : AppCompatActivity(), BaseApp.Listener, WoDetailAdapter.Listener {
    private val viewModel: WoLisViewModel by viewModels()
    private lateinit var woDetailAdapter: WoDetailAdapter
    private lateinit var wo_id: String
    private lateinit var type: String
    private var jenis: String? = null
    private var next_page = true
    private var loading = false
    private var page = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wo_detail)
        BaseApp(this).set()
    }

    override fun getIntentData() {
        if (intent.hasExtra(Constants.WO_ID)) {
            wo_id = intent.getStringExtra(Constants.WO_ID).toString()
        }
        if (intent.hasExtra("type")) {
            type = intent.getStringExtra("type").toString()
        }
        if (intent.hasExtra("jenis")) {
            jenis = intent.getStringExtra("jenis").toString()
        }


    }

    override fun setOnClick() {
        iv_back.setOnClickListener {
            startActivity(Intent(this, WoListActivity::class.java))
        }

        this?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (this@WoDetailActivity::wo_id.isInitialized) {
                    val intent = Intent(this@WoDetailActivity, WoListActivity::class.java)
                    startActivity(intent)
                } else {
                    if (type == "machine" || type == "molding" || type == "production" || type == "ipqc") {
                        val intent = Intent(this@WoDetailActivity, HomeActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        })
        swipeContainer.setOnRefreshListener {
            woDetailAdapter.clear()
            page = 1
            fetchData()
            swipeContainer.setRefreshing(false)
        }

        swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        );


    }

    override fun setAdapter() {
        if (this::wo_id.isInitialized) {
            woDetailAdapter = WoDetailAdapter(this, ArrayList(), this, wo_id, jenis)
            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

            rv_wo_detail_list.adapter = woDetailAdapter
            rv_wo_detail_list.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            rv_wo_detail_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    val countItem = layoutManager.itemCount
                    val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
                    val isLastPosition = countItem.minus(1) == lastVisiblePosition

                    if (!recyclerView.canScrollVertically(1)) {
                        if (next_page) {
                            if (!loading && isLastPosition) {
                                page++
                                loadData()
                            }
                        }
                    }


//
                }
            })
        } else {
            woDetailAdapter = WoDetailAdapter(this, ArrayList(), this, type)
            rv_wo_detail_list.adapter = woDetailAdapter
            rv_wo_detail_list.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        }


    }

    @SuppressLint("SetTextI18n")
    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) progress_bar.show()
            else progress_bar.hide()
        })
        if (this::wo_id.isInitialized) {
            tv_title.text = "Molding List - $wo_id"
        } else {
            if (type == "molding") {
                tv_title.text = "LIST MOLD SET UP"
            }

            if (type == "machine") {
                tv_title.text = "LIST MESIN SET UP"
            }
            if (type == "production") {
                tv_title.text = "LIST PRODUCTION TODO"
            }
            if (type == "ipqc") {
                tv_title.text = "LIST IPQC"
            }


        }

    }

    override fun loadData() {
        fetchData()

    }

    private fun fetchData() {
        if (this::wo_id.isInitialized) {
            viewModel.detail(wo_id, page).observe(this, Observer {
                if (it.success!!) {
                    next_page = it.header.next_page
                    if (page == 1) {
                        woDetailAdapter.setItems(it.data)
                    } else {
                        woDetailAdapter.addItems(it.data)
                    }
                }
            })
        } else {
            viewModel.detailOther(page, type).observe(this, Observer {
                if (it.success!!) {
                    next_page = it.header.next_page
                    if (page == 1) {
                        woDetailAdapter.setItems(it.data)
                    } else {
                        woDetailAdapter.addItems(it.data)
                    }
                }
            })
        }
    }

    override fun onLoadWo(data: WoDetailData) {
        finish()
    }

}