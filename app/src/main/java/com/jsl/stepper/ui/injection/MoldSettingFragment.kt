package com.jsl.stepper.ui.injection

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.model.MoldSetting
import com.jsl.stepper.model.mold_setting.MoldClose
import com.jsl.stepper.model.mold_setting.MoldOpen
import com.jsl.stepper.model.mold_setting.mold_open.Press
import com.jsl.stepper.model.mold_setting.mold_open.Speed
import com.jsl.stepper.model.mold_setting.mold_open.Stroke
import com.jsl.stepper.ui.stepper.InjectionSettingViewModel
import com.jsl.stepper.utils.BaseApp
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_mold_setting.*
import com.jsl.stepper.model.mold_setting.mold_close.Press as mcPress
import com.jsl.stepper.model.mold_setting.mold_close.Speed as mcSpeed
import com.jsl.stepper.model.mold_setting.mold_close.Stroke as mcStroke

@AndroidEntryPoint
class MoldSettingFragment : Fragment(), BaseApp.Listener {
    private lateinit var viewModel: InjectionSettingViewModel
    lateinit var moldSetting: MoldSetting
    lateinit var woDetailData: WoDetailData
    var isCreated: Boolean = false

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(InjectionSettingViewModel::class.java)
        BaseApp(this).set()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_mold_setting, container, false)
    }

    override fun getIntentData() {
    }

    @SuppressLint("ResourceAsColor")
    override fun setOnClick() {
        fab_prev.setOnClickListener {
            viewModel.setPage(InjectionActivity.PREVIEW)
//            rl_mold_setting.setBackgroundResource(R.drawable.bg_circle_grey)
//            tv_mold_setting_no.setBackgroundResource(R.drawable.bg_circle_grey)
////                tv_mold_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)
//            tv_mold_setting_no.setTextColor(R.color.md_grey_600)
        }
        fab_next.setOnClickListener {
            validation()
        }
    }

    override fun setAdapter() {
    }

    override fun setContent() {
        viewModel.woDetail.observe(this, Observer {
            woDetailData = it

        })
        viewModel.moldSetting.observe(this, Observer {
            moldSetting = it
            if (isCreated) {
                input_moP1.setText(moldSetting.mold_open.press.moP1.toString())
                input_moP2.setText(moldSetting.mold_open.press.moP2.toString())
                input_moP3.setText(moldSetting.mold_open.press.moP3.toString())
                input_moP4.setText(moldSetting.mold_open.press.moP4.toString())
                input_moP5.setText(moldSetting.mold_open.press.moP5.toString())

                input_moS1.setText(moldSetting.mold_open.speed.moS1.toString())
                input_moS2.setText(moldSetting.mold_open.speed.moS2.toString())
                input_moS3.setText(moldSetting.mold_open.speed.moS3.toString())
                input_moS4.setText(moldSetting.mold_open.speed.moS4.toString())
                input_moS5.setText(moldSetting.mold_open.speed.moS5.toString())

                input_moSt1.setText(moldSetting.mold_open.stroke.moSt1.toString())
                input_moSt2.setText(moldSetting.mold_open.stroke.moSt2.toString())
                input_moSt3.setText(moldSetting.mold_open.stroke.moSt3.toString())
                input_moSt4.setText(moldSetting.mold_open.stroke.moSt4.toString())
                input_moSt5.setText(moldSetting.mold_open.stroke.moSt5.toString())

                input_mcP1.setText(moldSetting.mold_close.press.mcP1.toString())
                input_mcP2.setText(moldSetting.mold_close.press.mcP2.toString())
                input_mcP3.setText(moldSetting.mold_close.press.mcP3.toString())
                input_mcP4.setText(moldSetting.mold_close.press.mcP4.toString())
                input_mcP5.setText(moldSetting.mold_close.press.mcP5.toString())

                input_mcS1.setText(moldSetting.mold_close.speed.mcS1.toString())
                input_mcS2.setText(moldSetting.mold_close.speed.mcS2.toString())
                input_mcS3.setText(moldSetting.mold_close.speed.mcS3.toString())
                input_mcS4.setText(moldSetting.mold_close.speed.mcS4.toString())
                input_mcS5.setText(moldSetting.mold_close.speed.mcS5.toString())

                input_mcSt1.setText(moldSetting.mold_close.stroke.mcSt1.toString())
                input_mcSt2.setText(moldSetting.mold_close.stroke.mcSt2.toString())
                input_mcSt3.setText(moldSetting.mold_close.stroke.mcSt3.toString())
                input_mcSt4.setText(moldSetting.mold_close.stroke.mcSt4.toString())
                input_mcSt5.setText(moldSetting.mold_close.stroke.mcSt5.toString())

                disableEditText(input_moP1)
                disableEditText(input_moP2)
                disableEditText(input_moP3)
                disableEditText(input_moP4)
                disableEditText(input_moP5)
                disableEditText(input_moS1)
                disableEditText(input_moS2)
                disableEditText(input_moS3)
                disableEditText(input_moS4)
                disableEditText(input_moS5)
                disableEditText(input_moSt1)
                disableEditText(input_moSt2)
                disableEditText(input_moSt3)
                disableEditText(input_moSt4)
                disableEditText(input_moSt5)
                disableEditText(input_mcP1)
                disableEditText(input_mcP2)
                disableEditText(input_mcP3)
                disableEditText(input_mcP4)
                disableEditText(input_mcP5)
                disableEditText(input_mcS1)
                disableEditText(input_mcS2)
                disableEditText(input_mcS3)
                disableEditText(input_mcS4)
                disableEditText(input_mcS5)
                disableEditText(input_mcSt1)
                disableEditText(input_mcSt2)
                disableEditText(input_mcSt3)
                disableEditText(input_mcSt4)
                disableEditText(input_mcSt5)

            }

        })
        viewModel.isCreated.observe(this, {
            isCreated = it


        })


    }

    private fun disableEditText(editText: EditText) {
        editText.isFocusable = false
        editText.isFocusableInTouchMode = false
        editText.isCursorVisible = false
        editText.keyListener = null
        editText.inputType = InputType.TYPE_NULL
        editText.setBackgroundColor(Color.TRANSPARENT)
    }

    override fun loadData() {
    }

    private fun validation() {
        val moP1 = input_moP1.text.toString();
        val moP2 = input_moP2.text.toString();
        val moP3 = input_moP3.text.toString();
        val moP4 = input_moP4.text.toString();
        val moP5 = input_moP5.text.toString();

        val moS1 = input_moS1.text.toString();
        val moS2 = input_moS2.text.toString();
        val moS3 = input_moS3.text.toString();
        val moS4 = input_moS4.text.toString();
        val moS5 = input_moS5.text.toString();

        val moSt1 = input_moSt1.text.toString();
        val moSt2 = input_moSt2.text.toString();
        val moSt3 = input_moSt3.text.toString();
        val moSt4 = input_moSt4.text.toString();
        val moSt5 = input_moSt5.text.toString();

        val mcP1 = input_mcP1.text.toString();
        val mcP2 = input_mcP2.text.toString();
        val mcP3 = input_mcP3.text.toString();
        val mcP4 = input_mcP4.text.toString();
        val mcP5 = input_mcP5.text.toString();

        val mcS1 = input_mcS1.text.toString();
        val mcS2 = input_mcS2.text.toString();
        val mcS3 = input_mcS3.text.toString();
        val mcS4 = input_mcS4.text.toString();
        val mcS5 = input_mcS5.text.toString();

        val mcSt1 = input_mcSt1.text.toString();
        val mcSt2 = input_mcSt2.text.toString();
        val mcSt3 = input_mcSt3.text.toString();
        val mcSt4 = input_mcSt4.text.toString();
        val mcSt5 = input_mcSt5.text.toString();

        if (moP1!!.isEmpty()) {
            input_moP1.error = "Mold Open Press 1 tidak boleh kosong"
            return
        }
        if (moP2!!.isEmpty()) {
            input_moP2.error = "Mold Open Press 2 tidak boleh kosong"
            return
        }
        if (moP3!!.isEmpty()) {
            input_moP3.error = "Mold Open Press 3 tidak boleh kosong"
            return
        }
        if (moP4!!.isEmpty()) {
            input_moP4.error = "Mold Open Press 4 tidak boleh kosong"
            return
        }
        if (moP5!!.isEmpty()) {
            input_moP5.error = "Mold Open Press 5 tidak boleh kosong"
            return
        }
        if (moS1!!.isEmpty()) {
            input_moS1.error = "Mold Open Speed 1 tidak boleh kosong"
            return
        }
        if (moS2!!.isEmpty()) {
            input_moS2.error = "Mold Open Speed 2 tidak boleh kosong"
            return
        }
        if (moS3!!.isEmpty()) {
            input_moS3.error = "Mold Open Speed 3 tidak boleh kosong"
            return
        }
        if (moS4!!.isEmpty()) {
            input_moS4.error = "Mold Open Speed 4 tidak boleh kosong"
            return
        }
        if (moS5!!.isEmpty()) {
            input_moS5.error = "Mold Open Speed 5 tidak boleh kosong"
            return
        }
        if (moSt1!!.isEmpty()) {
            input_moSt1.error = "Mold Open Stroke 1 tidak boleh kosong"
            return
        }
        if (moSt2!!.isEmpty()) {
            input_moSt2.error = "Mold Open Stroke 2 tidak boleh kosong"
            return
        }
        if (moSt3!!.isEmpty()) {
            input_moSt3.error = "Mold Open Stroke 3 tidak boleh kosong"
            return
        }
        if (moSt4!!.isEmpty()) {
            input_moSt4.error = "Mold Open Stroke 4 tidak boleh kosong"
            return
        }
        if (moSt5!!.isEmpty()) {
            input_moSt5.error = "Mold Open Stroke 5 tidak boleh kosong"
            return
        }
        if (mcP1!!.isEmpty()) {
            input_mcP1.error = "Mold Close Press 1 tidak boleh kosong"
            return
        }
        if (mcP2!!.isEmpty()) {
            input_mcP2.error = "Mold Close Press 2 tidak boleh kosong"
            return
        }
        if (mcP3!!.isEmpty()) {
            input_mcP3.error = "Mold Close Press 3 tidak boleh kosong"
            return
        }
        if (mcP4!!.isEmpty()) {
            input_mcP4.error = "Mold Close Press 4 tidak boleh kosong"
            return
        }
        if (mcP5!!.isEmpty()) {
            input_mcP5.error = "Mold Close Press 5 tidak boleh kosong"
            return
        }
        if (mcS1!!.isEmpty()) {
            input_mcS1.error = "Mold Close Speed 1 tidak boleh kosong"
            return
        }
        if (mcS2!!.isEmpty()) {
            input_mcS2.error = "Mold Close Speed 2 tidak boleh kosong"
            return
        }
        if (mcS3!!.isEmpty()) {
            input_mcS3.error = "Mold Close Speed 3 tidak boleh kosong"
            return
        }
        if (mcS4!!.isEmpty()) {
            input_mcS4.error = "Mold Close Speed 4 tidak boleh kosong"
            return
        }
        if (mcS5!!.isEmpty()) {
            input_mcS5.error = "Mold Close Speed 5 tidak boleh kosong"
            return
        }
        if (mcSt1!!.isEmpty()) {
            input_mcSt1.error = "Mold Close Stroke 1 tidak boleh kosong"
            return
        }
        if (mcSt2!!.isEmpty()) {
            input_mcSt2.error = "Mold Close Stroke 2 tidak boleh kosong"
            return
        }
        if (mcSt3!!.isEmpty()) {
            input_mcSt3.error = "Mold Close Stroke 3 tidak boleh kosong"
            return
        }
        if (mcSt4!!.isEmpty()) {
            input_mcSt4.error = "Mold Close Stroke 4 tidak boleh kosong"
            return
        }
        if (mcSt5!!.isEmpty()) {
            input_mcSt5.error = "Mold Close Stroke 5 tidak boleh kosong"
            return
        }

        val mSmoPT = Press(moP1.toInt(), moP2.toInt(), moP3.toInt(), moP4.toInt(), moP5.toInt())
        val mSmoST = Speed(moS1.toInt(), moS2.toInt(), moS3.toInt(), moS4.toInt(), moS4.toInt())
        val mSmoStT =
            Stroke(moSt1.toInt(), moSt2.toInt(), moSt3.toInt(), moSt4.toInt(), moSt5.toInt())
        val mSmo = MoldOpen(mSmoPT, mSmoST, mSmoStT)

        val mSmcPT = mcPress(mcP1.toInt(), mcP2.toInt(), mcP3.toInt(), mcP4.toInt(), mcP5.toInt())
        val mSmcST = mcSpeed(mcS1.toInt(), mcS2.toInt(), mcS3.toInt(), mcS4.toInt(), mcS5.toInt())
        val mSmcStT =
            mcStroke(mcSt1.toInt(), mcSt3.toInt(), mcSt3.toInt(), mcSt4.toInt(), mcSt5.toInt())
        val mSmc = MoldClose(mSmcPT, mSmcST, mSmcStT)

        val setting = MoldSetting(mSmo, mSmc)
        viewModel.setMoldSetting(setting)
        viewModel.setPage(InjectionActivity.EJECTOR_SETTING)

    }
}