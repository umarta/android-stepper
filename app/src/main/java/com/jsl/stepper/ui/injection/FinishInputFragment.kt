package com.jsl.stepper.ui.injection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.model.*
import com.jsl.stepper.ui.stepper.InjectionSettingViewModel
import com.jsl.stepper.utils.BaseApp
import kotlinx.android.synthetic.main.fragment_finish_input.*

class FinishInputFragment : Fragment(),BaseApp.Listener {
    private lateinit var viewModel: InjectionSettingViewModel
    private lateinit var injectionSetting: InjectionSetting
    private lateinit var ejectorSetting: EjectorSetting
    private lateinit var plastizing: Plastizing
    private lateinit var temperatureSetting: TemperatureSetting
    private lateinit var moldSetting: MoldSetting
    private lateinit var woDetailData: WoDetailData

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(InjectionSettingViewModel::class.java)
        BaseApp(this).set()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_finish_input, container, false)
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        fab_finish.setOnClickListener {
            viewModel.setPage(InjectionActivity.FINISH)

        }
        fab_prev.setOnClickListener {
            viewModel.setPage(InjectionActivity.TEMPERATURE_BARREL_SETTING)

        }
    }

    override fun setAdapter() {
    }

    override fun loadData() {
    }

    override fun setContent() {

        viewModel.woDetail.observe(this, Observer {
            woDetailData = it

            tv_kode_mold.text = woDetailData.code
            tv_kode_part.text = woDetailData.code
            tv_tonase.text = woDetailData.shot_weight
            tv_jenis_proses.text = woDetailData.jenis_proses
            tv_material.text = woDetailData.material
            tv_berat_part.text = woDetailData.berat_part
            tv_berat_runer.text = woDetailData.berat_runner

        })
        viewModel.moldSetting.observe(this, Observer {
            moldSetting = it
            tv_moP1.text = moldSetting.mold_open.press.moP1.toString()
            tv_moP2.text = moldSetting.mold_open.press.moP2.toString()
            tv_moP3.text = moldSetting.mold_open.press.moP3.toString()
            tv_moP4.text = moldSetting.mold_open.press.moP4.toString()
            tv_moP5.text = moldSetting.mold_open.press.moP5.toString()

            tv_moS1.text = moldSetting.mold_open.speed.moS1.toString()
            tv_moS2.text = moldSetting.mold_open.speed.moS2.toString()
            tv_moS3.text = moldSetting.mold_open.speed.moS3.toString()
            tv_moS4.text = moldSetting.mold_open.speed.moS4.toString()
            tv_moS5.text = moldSetting.mold_open.speed.moS5.toString()

            tv_moSt1.text = moldSetting.mold_open.stroke.moSt1.toString()
            tv_moSt2.text = moldSetting.mold_open.stroke.moSt2.toString()
            tv_moSt3.text = moldSetting.mold_open.stroke.moSt3.toString()
            tv_moSt4.text = moldSetting.mold_open.stroke.moSt4.toString()
            tv_moSt5.text = moldSetting.mold_open.stroke.moSt5.toString()

            tv_mcP1.text = moldSetting.mold_close.press.mcP1.toString()
            tv_mcP2.text = moldSetting.mold_close.press.mcP2.toString()
            tv_mcP3.text = moldSetting.mold_close.press.mcP3.toString()
            tv_mcP4.text = moldSetting.mold_close.press.mcP4.toString()
            tv_mcP5.text = moldSetting.mold_close.press.mcP5.toString()

            tv_mcS1.text = moldSetting.mold_close.speed.mcS1.toString()
            tv_mcS2.text = moldSetting.mold_close.speed.mcS2.toString()
            tv_mcS3.text = moldSetting.mold_close.speed.mcS3.toString()
            tv_mcS4.text = moldSetting.mold_close.speed.mcS4.toString()
            tv_mcS5.text = moldSetting.mold_close.speed.mcS5.toString()

            tv_mcSt1.text = moldSetting.mold_close.stroke.mcSt1.toString()
            tv_mcSt2.text = moldSetting.mold_close.stroke.mcSt2.toString()
            tv_mcSt3.text = moldSetting.mold_close.stroke.mcSt3.toString()
            tv_mcSt4.text = moldSetting.mold_close.stroke.mcSt4.toString()
            tv_mcSt5.text = moldSetting.mold_close.stroke.mcSt5.toString()


        })
        viewModel.injectionSetting.observe(this, Observer {
            injectionSetting = it
            tv_iShP1.text = injectionSetting.holding.press.Ishp1.toString()
            tv_iShP2.text = injectionSetting.holding.press.Ishp2.toString()
            tv_iShP3.text = injectionSetting.holding.press.Ishp3.toString()

            tv_iShS1.text = injectionSetting.holding.speed.Ishs1.toString()
            tv_iShS2.text = injectionSetting.holding.speed.Ishs2.toString()
            tv_iShS3.text = injectionSetting.holding.speed.Ishs3.toString()

            tv_iShT1.text = injectionSetting.holding.time.Isht1.toString()
            tv_iShT2.text = injectionSetting.holding.time.Isht2.toString()
            tv_iShT3.text = injectionSetting.holding.time.Isht3.toString()

            tv_iSiP1.text = injectionSetting.injection.press.Isip1.toString()
            tv_iSiP2.text = injectionSetting.injection.press.Isip2.toString()
            tv_iSiP3.text = injectionSetting.injection.press.Isip3.toString()
            tv_iSiP4.text = injectionSetting.injection.press.Isip4.toString()
            tv_iSiP5.text = injectionSetting.injection.press.Isip5.toString()
            tv_iSiP6.text = injectionSetting.injection.press.Isip6.toString()

            tv_iSiS1.text = injectionSetting.injection.speed.Issp1.toString()
            tv_iSiS2.text = injectionSetting.injection.speed.Issp2.toString()
            tv_iSiS3.text = injectionSetting.injection.speed.Issp3.toString()
            tv_iSiS4.text = injectionSetting.injection.speed.Issp4.toString()
            tv_iSiS5.text = injectionSetting.injection.speed.Issp5.toString()
            tv_iSiS6.text = injectionSetting.injection.speed.Issp6.toString()

            tv_iSiSt1.text = injectionSetting.injection.stroke.Isstp1.toString()
            tv_iSiSt2.text = injectionSetting.injection.stroke.Isstp2.toString()
            tv_iSiSt3.text = injectionSetting.injection.stroke.Isstp3.toString()
            tv_iSiSt4.text = injectionSetting.injection.stroke.Isstp4.toString()
            tv_iSiSt5.text = injectionSetting.injection.stroke.Isstp5.toString()
            tv_iSiSt6.text = injectionSetting.injection.stroke.Isstp6.toString()

            tv_iSiT1.text = injectionSetting.injection.time.Istp1.toString()


        })
        viewModel.ejectorSetting.observe(this, Observer {
            ejectorSetting = it

            tv_eSeBP1.text = ejectorSetting.ejectorBackward.press.ebp1.toString()
            tv_eSeBP2.text = ejectorSetting.ejectorBackward.press.ebp2.toString()

            tv_eSeBS1.text = ejectorSetting.ejectorBackward.speed.ebs1.toString()
            tv_eSeBS2.text = ejectorSetting.ejectorBackward.speed.ebs2.toString()

            tv_eSeBSt1.text = ejectorSetting.ejectorBackward.stroke.ebst1.toString()
            tv_eSeBSt2.text = ejectorSetting.ejectorBackward.stroke.ebst2.toString()

            tv_eSeFP1.text = ejectorSetting.ejectorForward.press.efp1.toString()
            tv_eSeFP2.text = ejectorSetting.ejectorForward.press.efp2.toString()

            tv_eSeFS1.text = ejectorSetting.ejectorForward.speed.efs1.toString()
            tv_eSeFS2.text = ejectorSetting.ejectorForward.speed.efs2.toString()

            tv_eSeFSt1.text = ejectorSetting.ejectorForward.stroke.efst1.toString()
            tv_eSeFSt2.text = ejectorSetting.ejectorForward.stroke.efst2.toString()


        })
        viewModel.plastizing.observe(this, Observer {
            plastizing = it
            tv_pUcBP1.text = plastizing.charging.backPress.pUcBP1.toString()
            tv_pUcBP2.text = plastizing.charging.backPress.pUcBP2.toString()
            tv_pUcBP3.text = plastizing.charging.backPress.pUcBP3.toString()
            tv_pUcBP4.text = plastizing.charging.backPress.pUcBP4.toString()

            tv_pUcP1.text = plastizing.charging.press.pUcP1.toString()
            tv_pUcP2.text = plastizing.charging.press.pUcP2.toString()
            tv_pUcP3.text = plastizing.charging.press.pUcP3.toString()
            tv_pUcP4.text = plastizing.charging.press.pUcP4.toString()

            tv_pUcS1.text = plastizing.charging.speed.pUcS1.toString()
            tv_pUcS2.text = plastizing.charging.speed.pUcS2.toString()
            tv_pUcS3.text = plastizing.charging.speed.pUcS3.toString()
            tv_pUcS4.text = plastizing.charging.speed.pUcS4.toString()

            tv_pUcSt1.text = plastizing.charging.stroke.pUcSt1.toString()
            tv_pUcSt2.text = plastizing.charging.stroke.pUcSt2.toString()
            tv_pUcSt3.text = plastizing.charging.stroke.pUcSt3.toString()
            tv_pUcSt4.text = plastizing.charging.stroke.pUcSt4.toString()

            tv_pUsBP1.text = plastizing.suckBack.press.pUsBP1.toString()
            tv_pUsBS1.text = plastizing.suckBack.speed.pUsBS1.toString()
            tv_pUsBSt1.text = plastizing.suckBack.stroke.pUsBSt1.toString()
            tv_pUsBT1.text = plastizing.suckBack.coolTime.pUsBT1.toString()


        })
        viewModel.temperatureSetting.observe(this, Observer {
            temperatureSetting = it

            tv_tSTbSHn.text = temperatureSetting.temperatureBarrel.setting.tSTbSHn.toString()
            tv_tSTbSH1.text = temperatureSetting.temperatureBarrel.setting.tSTbSH1.toString()
            tv_tSTbSH2.text = temperatureSetting.temperatureBarrel.setting.tSTbSH2.toString()
            tv_tSTbSH3.text = temperatureSetting.temperatureBarrel.setting.tSTbSH3.toString()
            tv_tSTbSH4.text = temperatureSetting.temperatureBarrel.setting.tSTbSH4.toString()
            tv_tSTbSH5.text = temperatureSetting.temperatureBarrel.setting.tSTbSH5.toString()
            tv_tSTbSH6.text = temperatureSetting.temperatureBarrel.setting.tSTbSH6.toString()

            tv_tSTbAHn.text = temperatureSetting.temperatureBarrel.actual.tSTbAHn.toString()
            tv_tSTbAH1.text = temperatureSetting.temperatureBarrel.actual.tSTbAH1.toString()
            tv_tSTbAH2.text = temperatureSetting.temperatureBarrel.actual.tSTbAH2.toString()
            tv_tSTbAH3.text = temperatureSetting.temperatureBarrel.actual.tSTbAH3.toString()
            tv_tSTbAH4.text = temperatureSetting.temperatureBarrel.actual.tSTbAH4.toString()
            tv_tSTbAH5.text = temperatureSetting.temperatureBarrel.actual.tSTbAH5.toString()
            tv_tSTbAH6.text = temperatureSetting.temperatureBarrel.actual.tSTbAH6.toString()

            tv_tStHS.text = temperatureSetting.temperatureHopper.setting.tStHS.toString()
            tv_tStHA.text = temperatureSetting.temperatureHopper.actual.tStHA.toString()

            tv_tStMS.text = temperatureSetting.temperatureMtc.setting.tStMS.toString()
            tv_tStMSA.text = temperatureSetting.temperatureMtc.actual.tStMA.toString()


        })
    }


}