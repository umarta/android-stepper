package com.jsl.stepper.ui.serahterima

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsl.stepper.data.remote.MainRepository
import com.jsl.stepper.data.remote.MainService
import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.serahterima.SerahTerimaListData
import com.jsl.stepper.data.remote.response.serahterima.SerahterimaData

class SerahTerimaViewModel @ViewModelInject constructor(mainService: MainService) : ViewModel() {
    private val repoMain =
        MainRepository(mainService)
    val loading: LiveData<Boolean> get() = repoMain.loading
    fun getWo(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getWoSerahterima()

    fun getSeq(id: String): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getSeqSerahterima(id)

    fun getWoSerahterimaDetail(id: String, type: String): LiveData<BaseResponse<SerahterimaData>> =
        repoMain.getWoSerahterimaDetail(id, type)

    fun terimaAct(request: HashMap<String, Any>): MutableLiveData<BaseResponse<Any>> =
        repoMain.terimaAct(request)

    fun submit(request: HashMap<String, Any>): MutableLiveData<BaseResponse<Any>> =
        repoMain.submitSerah(request)

    fun list(page: Int): MutableLiveData<BaseResponse<MutableList<SerahTerimaListData>>> =
        repoMain.serahterimaList(page)

    fun terima(page: Int): MutableLiveData<BaseResponse<MutableList<SerahTerimaListData>>> =
        repoMain.terima(page)

    private val _detail = MutableLiveData<SerahterimaData>()
    val detail: LiveData<SerahterimaData> get() = _detail

    fun setDetail(serahTerima: SerahterimaData) {
        _detail.value = serahTerima
    }

}