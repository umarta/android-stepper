package com.jsl.stepper.ui.shifting

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.serahterima.SerahTerimaListData
import com.jsl.stepper.data.remote.response.shifting.ShiftingList
import com.jsl.stepper.utils.hide
import com.jsl.stepper.utils.show
import kotlinx.android.synthetic.main.view_shifting_list.view.*


class ShiftingAdapter(
    private val activity: Activity,
    private var data: MutableList<ShiftingList>,
    val listener: ShiftingActivity,
) : RecyclerView.Adapter<ShiftingAdapter.Holder>() {
    fun setItems(data: MutableList<ShiftingList>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<ShiftingList>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ShiftingAdapter.Holder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_shifting_list, parent, false)
        return Holder(view)
    }

    interface Listener {
        fun onLoadData(data: SerahTerimaListData)
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(
            mData: ShiftingList,
            position: Int,
            listener: ShiftingAdapter.Listener
        ) {
            with(itemView) {
                tv_hari.text = mData.hari
                tv_tanggal.text = mData.tanggal
                if (mData.list.size == 1) {
                    val check_shift = mData.list[0].shift
                    if (check_shift == 1) {
                        ll_shift_2.visibility = View.INVISIBLE
                        ll_shift_1.show()
                    }
                    if (check_shift == 2) {
                        ll_shift_1.visibility = View.INVISIBLE
                        ll_shift_2.show()
                    }
                } else {
                    ll_shift_2.visibility = View.VISIBLE
                    ll_shift_1.visibility = View.VISIBLE
                }



                mData.list.forEach {
                    if (it.shift == 1) {
                        tv_jam_masuk_shift_1.text = it.jam_masuk
                        tv_jam_keluar_shift_1.text = it.jam_keluar
                        tv_total_jam_istirahat_shift_1.text = it.total_jam_istirahat
                    }
                    if (it.shift == 2) {
                        tv_jam_masuk_shift_2.text = it.jam_masuk
                        tv_jam_keluar_shift_2.text = it.jam_keluar
                        tv_total_jam_istirahat_shift_2.text = it.total_jam_istirahat
                    }
                }

                itemView.setOnClickListener {
                    val i = Intent(activity, ShiftingDetailActivity::class.java)
                    val extra = Bundle()
                    i.putExtra("tanggal", mData.tanggal);
                    i.putExtra("hari", mData.hari);

                    activity.startActivity(i)
                    activity.overridePendingTransition(R.xml.anim_fade_in, R.xml.anim_fade_out)

                }
            }
        }
    }

    override fun onBindViewHolder(holder: ShiftingAdapter.Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    override fun getItemCount(): Int = data.size
}