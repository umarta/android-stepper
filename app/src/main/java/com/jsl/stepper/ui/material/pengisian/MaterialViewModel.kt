package com.jsl.stepper.ui.material.pengisian

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsl.stepper.data.remote.MainRepository
import com.jsl.stepper.data.remote.MainService
import com.jsl.stepper.data.remote.response.BaseResponse
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.material.PengisianListData
import com.jsl.stepper.data.remote.response.setup.MoldDetail

class MaterialViewModel @ViewModelInject constructor(mainService: MainService) : ViewModel() {
    private val repoMain =
        MainRepository(mainService)
    val loading: LiveData<Boolean> get() = repoMain.loading
    fun getMachine(query: String): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getMachine()

    fun getMaterial(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.material()

    fun getMoldDetail(wo_id: String, wo_mold_id: String): LiveData<BaseResponse<MoldDetail>> =
        repoMain.setupMoldDetail(wo_id, wo_mold_id.toLong())

    fun getMaterialByMachine(id: Long): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getIpMaterialList(id)

    fun getPengisianList(
        page: Int,
        machineId: Long? = null,
        materialId: Int? = null,
        startFrom: String? = null,
        endAt: String? = null
    ): LiveData<BaseResponse<MutableList<PengisianListData>>> =
        repoMain.getPengisianList(page, machineId, materialId, startFrom, endAt)

    fun getMachineList(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getMachineList()

    fun getMaterialGrade(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getMaterialGrade()

    fun getMaterialGradeRemark(materialId:String,gradeId: String): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.getMaterialGradeRemark(materialId,gradeId)

    fun getColour(): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.colour()

    fun submit(request: HashMap<String, Any>): MutableLiveData<BaseResponse<Any>> =
        repoMain.submitPengisian(request)


}