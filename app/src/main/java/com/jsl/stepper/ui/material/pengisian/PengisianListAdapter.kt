package com.jsl.stepper.ui.material.pengisian

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.material.PengisianListData
import com.jsl.stepper.ui.wo.detail.WoDetailActivity
import com.jsl.stepper.utils.Constants
import kotlinx.android.synthetic.main.view_pengisian_list.view.*

class PengisianListAdapter(
    private val activity: Activity,
    private var data: MutableList<PengisianListData>,
    val listener: Listener
) : RecyclerView.Adapter<PengisianListAdapter.Holder>() {
    fun setItems(data: MutableList<PengisianListData>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<PengisianListData>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(mData: PengisianListData, position: Int, listener: PengisianListAdapter.Listener) {
            with(itemView) {
                tv_no.text = (position + 1).toString()
                tv_no_mesin.text = mData.machine
                tv_material.text = mData.material
                tv_warna.text = mData.color
                tv_grade.text = mData.grade
                tv_remark.text = mData.grade_remark
                tv_diisi.text = mData.diisi.toString()
                tv_diambil.text = mData.diambil.toString()
//                itemView.setOnClickListener {
//                    val i = Intent(activity, PengisianMaterialActivity::class.java)
//                    i.putExtra("id", mData.id)
//                    activity.startActivity(i)
//                }
            }
        }
    }


    interface Listener {
        fun onLoadList(data: PengisianListData)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.view_pengisian_list, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position], position, listener)

    }

    override fun getItemCount(): Int = data.size
}