package com.jsl.stepper.ui.serahterima

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.data.remote.response.serahterima.SerahterimaData
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_serah_terima.*

@AndroidEntryPoint
class SerahTerimaActivity : AppCompatActivity(), BaseApp.Listener {
    private val viewModel: SerahTerimaViewModel by viewModels()
    private lateinit var wo: IdNameData
    private lateinit var seq: IdNameData
    private lateinit var detail: SerahterimaData
    private lateinit var id: String
    private lateinit var type: String
    private var isRevisied: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_serah_terima)
        BaseApp(this).set()
    }

    override fun getIntentData() {
        if (intent.hasExtra(Constants.DATA_EXTRA)) {
            id = intent.getStringExtra(Constants.DATA_EXTRA).toString()
        }
        if (intent.hasExtra("type")) {
            type = intent.getStringExtra("type").toString()
        }


    }

    @SuppressLint("SetTextI18n")
    override fun setOnClick() {
        sp_no_wo.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                wo = item
                seq()
                tv_kode_part.setText("Kode Part")
                tv_kode_mold.setText("Kode Mold")
                tv_qty_wo.setText("QTY WO")
                tv_qty_sisa_wo.setText("QTY SISA WO")
                tv_qty_ok.setText("QTY OK")

            }
        )
        sp_sequence.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                seq = item
                fetchDetail()
            }
        )

        tv_save.setOnClickListener {
            submit()
        }
        iv_back.setOnClickListener {
            if (type == "serah") {
                val i = Intent(this, SerahTerimaListActivity::class.java)
                i.putExtra("type", "serah")
                startActivity(i)
            } else {
                val i = Intent(this, SerahTerimaListActivity::class.java)
                i.putExtra("type", "terima")
                startActivity(i)
            }
        }

    }

    private fun wo() {
        progress_bar.show()
        viewModel.getWo().observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                sp_no_wo.setAdapter(spinnerAdapter)
            }
        })
        progress_bar.hide()
    }

    private fun fetchDetail() {
        progress_bar.show()
        viewModel.getWoSerahterimaDetail(id, type).observe(this, Observer {
            if (it.success!!) {
                viewModel.setDetail(it.data)
                val data = it.data
                tv_kode_mold.setText(data.kode_mold)
                tv_kode_mold.isEnabled = false
                tv_kode_part.isEnabled = false
                tv_kode_part.setText(data.kode_part.toString())

                tv_qty_wo.setText(data.qty_wo.toString())
                tv_qty_wo.isEnabled = false

                tv_qty_ok.setText(data.qty_ok.toString())
                tv_qty_ok.isEnabled = false

                tv_qty_sisa_wo.setText(data.qty_sisa_wo.toString())
                tv_qty_sisa_wo.isEnabled = false

                sp_sequence.setText(data.sequenceno.toString())
                sp_no_wo.setText(data.wo_no.toString())

                input_jumlah_revisi.setHint("Total OK : " + data.qty_ok)
                if (type == "terima") {
                    tv_save.text = "Terima"
                    tv_title.text = "Terima Barang"
                    input_jumlah_box.setText(data.qty_box.toString())
                    input_jumlah_revisi.setText(data.qty_serah.toString())
                    input_jumlah_revisi.hint = "Jumlah Terima"
                } else {
                    tv_save.text = "Serah"
                    tv_title.text = "Serah Barang"
                    input_jumlah_revisi.hint = "Jumlah Serah"


                }
            }
        })
        progress_bar.hide()
    }

    @SuppressLint("ResourceAsColor")
    private fun seq() {
        progress_bar.show()
        viewModel.getSeq(wo.id).observe(this, Observer {
            if (it.success!!) {

                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                sp_sequence.setAdapter(spinnerAdapter)
                sp_sequence.setText("pilih sequence")
                sp_sequence.setHint("pilih sequence")
                sp_sequence.setHintColor(R.color.colorPrimary)
                sp_sequence.setHintTextColor(R.color.black)
            }
        })
        progress_bar.hide()
    }

    fun submit() {
        if (type == "serah") {
            val res = HashMap<String, Any>()
            res["production_id"] = detail.id.toString()
            res["qty_box"] = input_jumlah_box.text.toString()
            res["qty_serah"] = input_jumlah_revisi.text.toString()
            viewModel.submit(res).observe(this, Observer {
                if (it.success!!) {
                    toast("succes submit serah terima")
                    val i = Intent(this, SerahTerimaListActivity::class.java)
                    i.putExtra("type",type)
                    this.startActivity(i)
                } else {
                    toast(it.message?.first())
                }
            })
        } else {
            val res = HashMap<String, Any>()
            res["id"] = id
            res["qty_box"] = input_jumlah_box.text.toString()
            res["qty_serah"] = input_jumlah_revisi.text.toString()
            viewModel.terimaAct(res).observe(this, Observer {
                if (it.success!!) {
                    toast("succes terima barang")
                    val i = Intent(this, SerahTerimaListActivity::class.java)
                    i.putExtra("type",type)
                    this.startActivity(i)
                } else {
                    toast(it.message?.first())
                }
            })

        }
    }
    override fun setAdapter() {
    }

    override fun loadData() {
    }

    override fun setContent() {
        fetchDetail()
        viewModel.detail.observe(this, Observer {
            detail = it
        })
    }
}