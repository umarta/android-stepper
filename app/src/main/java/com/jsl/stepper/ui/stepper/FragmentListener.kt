package com.jsl.stepper.ui.stepper

interface FragmentListener {
    fun handleOnClick()
}