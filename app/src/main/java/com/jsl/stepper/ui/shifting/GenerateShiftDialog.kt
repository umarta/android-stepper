package com.jsl.stepper.ui.shifting

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.jsl.stepper.R
import kotlinx.android.synthetic.main.dialog_close.tv_action
import kotlinx.android.synthetic.main.dialog_close.tv_cancel
import kotlinx.android.synthetic.main.dialog_close.tv_text
import kotlinx.android.synthetic.main.dialog_generate_shift.*
import java.util.*

class GenerateShiftDialog(val message: String, val listener: Listener, val sort: Boolean = false) :
    DialogFragment() {
    lateinit var date: Date
    private var tahun: Int = 0;
    private var bulan: Int = 0;

    override fun onResume() {
        super.onResume()
        dialog?.window?.setBackgroundDrawableResource(R.color.transparent)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return inflater.inflate(R.layout.dialog_generate_shift, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_text.text = message
        input_bulan_tahun.isFocusable = false
        input_bulan_tahun.isClickable = true
        if (sort) {
            tv_action.text = "Generate"
        } else {
            tv_action.text = "Filter"
        }
        input_bulan_tahun.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener =
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    input_bulan_tahun.setText("" + year + "-" + (month + 1))
                    date = cal.time
                    tahun = year
                    bulan = (month + 1)
                }
            DatePickerDialog(
                requireContext(),
                timeSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH),
            ).show()
        }
        tv_cancel.setOnClickListener {
            listener.cancelClose()
            dismiss()

        }
        tv_action.setOnClickListener {
            if (!sort) {
                listener.onCloseDialog(tahun, bulan)
            } else {
                listener.onSrot(tahun, bulan)

            }
            dismiss()
        }
    }

    interface Listener {
        fun onCloseDialog(tahun_: Int, bulan_: Int)
        fun onSrot(tahun_: Int, bulan_: Int)
        fun cancelClose()
    }

}