package com.jsl.stepper.ui.stepper.steps

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jsl.stepper.R
import com.jsl.stepper.model.stepper.IsMoP
import com.jsl.stepper.utils.BaseApp
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_step1.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class Step1Fragment : Fragment(),BaseApp.Listener {
    private lateinit var isMoP: IsMoP
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        BaseApp(this).set()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_step1, container, false);
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
    }

    override fun setAdapter() {
    }

    override fun setContent() {
    }

    override fun loadData() {
    }
    fun update(s: String?) {
        input_mcP1.setText(s)
    }


}