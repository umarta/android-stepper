package com.jsl.stepper.ui.material.pengisian

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.fragment.app.viewModels
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.IdNameData
import com.jsl.stepper.utils.hide
import com.jsl.stepper.utils.show
import kotlinx.android.synthetic.main.bsheet_brand_filter.*

import kotlinx.android.synthetic.main.bsheet_brand_filter.view.*

class FilterBSheet(
    val listener: Listener,

    private var mesinList: MutableList<IdNameData>,
    private var materialList: MutableList<IdNameData>
) : BottomSheetDialogFragment() {

    private val viewModel: MaterialViewModel by viewModels()
    private lateinit var spMesin: MaterialSpinner
    private lateinit var spMaterial: MaterialSpinner
    private lateinit var progressBar: ProgressBar
    private lateinit var mesinData: IdNameData
    private lateinit var materialData: IdNameData


    fun setMaterial(materialLists: MutableList<IdNameData>) {
        materialList = materialLists
        val subBrandAdapter = MaterialSpinnerAdapter<IdNameData>(context, materialList)
        spMaterial.setItems("")
        spMaterial.setAdapter(subBrandAdapter)
        progressBar.hide()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v: View = inflater.inflate(R.layout.bsheet_brand_filter, container, false)
        spMesin = v.sp_machine
        spMaterial = v.sp_material
        progressBar = v.progress_bar
        v.sp_machine.setOnItemSelectedListener(MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
            listener.onMesin(item.id.toLong())
            progressBar.show()
            mesinData = item
        })
        v.sp_material.setOnItemSelectedListener(MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
            progressBar.hide()
            materialData = item
        })

        val mesinAdapter = MaterialSpinnerAdapter<IdNameData>(context, mesinList)
        v.sp_machine.setAdapter(mesinAdapter)
        val materialAdapter = MaterialSpinnerAdapter<IdNameData>(context, materialList)
        v.sp_material.setAdapter(materialAdapter)
        var material = "0"
        var machine = "0"

        v.tv_action.setOnClickListener {
            if (this::materialData.isInitialized) {
                material = materialData.id.toString()
            }
            if (this::mesinData.isInitialized) {
                machine = mesinData.id.toString()
            }

            val res = HashMap<String, Any>()
            val start = v.start_from.text.toString()
            val end = v.end_at.text.toString()
            listener.onAction(machine, material, start, end)
            dismiss()
        }
        v.start_from.setOnClickListener {
            listener.onDate("start")
        }
        v.end_at.setOnClickListener {
            listener.onDate("end")
        }

        return v
    }

    fun setStart(string: String) {
        start_from.setText(string)
    }

    fun setEnd(string: String) {
        end_at.setText(string)
    }

    interface Listener {
        fun onDate(string: String)
        fun onMesin(id: Long)
        fun onAction(machine: String, material: String, start: String, end: String)
    }

}