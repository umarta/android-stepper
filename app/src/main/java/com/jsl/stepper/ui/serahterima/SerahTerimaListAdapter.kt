package com.jsl.stepper.ui.serahterima

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.serahterima.SerahTerimaListData
import com.jsl.stepper.utils.Constants
import com.jsl.stepper.utils.hide
import kotlinx.android.synthetic.main.view_serah_terima_list.view.*

class SerahTerimaListAdapter(
    private val activity: Activity,
    private var data: MutableList<SerahTerimaListData>,
    val listener: SerahTerimaListActivity,
    val type: String
) : RecyclerView.Adapter<SerahTerimaListAdapter.Holder>() {
    fun setItems(data: MutableList<SerahTerimaListData>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<SerahTerimaListData>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SerahTerimaListAdapter.Holder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_serah_terima_list, parent, false)
        return Holder(view)
    }

    interface Listener {
        fun onLoadData(data: SerahTerimaListData)
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(
            mData: SerahTerimaListData,
            position: Int,
            listener: SerahTerimaListAdapter.Listener
        ) {
            with(itemView) {
                if (type == "serah") {
                    tv_kode_part.text = mData.kode_part.toString()
                    tv_nama_produk.text = mData.nama_produk.toString()
                    tv_qty_serah.text = mData.qty_produksi.toString()
                    tv_qty_terima.text = mData.qty_terima.toString()
                    tv_qty_balance.text = mData.qty_balance.toString()
                    tv_no.text = mData.sequenceno.toString()
                    tv_no_wo.text = mData.no_wo

                    itemView.setOnClickListener {
                        val i = Intent(activity, SerahTerimaActivity::class.java)
                        i.putExtra(Constants.DATA_EXTRA, mData.id.toString())
                        i.putExtra("type", "serah")

                        activity.startActivity(i)
                    }
                } else if (type == "terima") {
                    tv_kode_part.text = mData.kode_part.toString()
                    tv_nama_produk.text = mData.nama_produk.toString()
                    ll_serah.hide()
                    di_sebelah.hide()
                    tv_qty_terima.text = mData.qty_serah.toString()
                    tv_qty_balance.text = mData.qty_sisa.toString()
                    tv_no.text = (position + 1).toString()

                    itemView.setOnClickListener {
                        val i = Intent(activity, SerahTerimaActivity::class.java)
                        i.putExtra(Constants.DATA_EXTRA, mData.id.toString())
                        i.putExtra("type", "terima")
                        activity.startActivity(i)
                    }
                }
            }
        }
    }

    override fun onBindViewHolder(holder: SerahTerimaListAdapter.Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    override fun getItemCount(): Int = data.size
}