package com.jsl.stepper.ui.ipqc

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.ipqc.BomPartData
import com.jsl.stepper.data.remote.response.ipqc.IpQcData
import com.jsl.stepper.ui.dialog.RemarkDialog
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_ipcq.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*
import kotlin.collections.HashMap


@AndroidEntryPoint
class IpQcActivity : AppCompatActivity(), BaseApp.Listener, BomPartListAdapter.Listener,
    ImageDialog.Listener, RemarkDialog.Listener {
    var list = ArrayList<EditModel>()
    private lateinit var bomPartData: BomPartData
    private lateinit var ipQcData: IpQcData
    private lateinit var bomPartListAdapter: BomPartListAdapter
    private lateinit var imageDialog: ImageDialog
    private val viewModel: IpQcViewModel by viewModels()
    private lateinit var l_molding_id: String
    private lateinit var image: String
    private lateinit var imageLink: String
    private lateinit var remark_message: String
    var hasImage: Boolean = false

    var FLASHING = 0;
    private var OVER_SILICON = 0;
    private var FLOW_MARK = 0;
    private var SCRATCH = 0;
    private var MOTIF_CACAT = 0;
    private var FINISHING_RAPI = 0;
    private var GAS_MARK = 0;
    private var WHITE_MARK = 0;
    private var WELD_LINE = 0;
    private var SILVER = 0;
    private var BINTIK = 0;
    private var RETAK = 0;
    private var GATE_RUNNER_RATA = 0;

    private var mImageView: ImageView? = null
    private var mUri: Uri? = null

    //Our widgets
    private lateinit var btnCapture: Button
    private lateinit var btnChoose: Button

    //Our constants
    private val OPERATION_CAPTURE_PHOTO = 1
    private val OPERATION_CHOOSE_PHOTO = 2


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ipcq)
        BaseApp(this).set()
    }


    override fun getIntentData() {
        if (intent.hasExtra("l_molding_id")) {
            l_molding_id = intent.getStringExtra("l_molding_id").toString()
        }

    }

    @SuppressLint("ResourceAsColor")
    override fun setOnClick() {
        iv_image.setOnClickListener {
//            if (ipQcData.image == null) {
//                link = ""
//                toast("kosong")
//            } else {
//                link = imageLink
//                toast("ada isi")
//            }

            val dialog = ImageDialog("Setup mesin ini telah selesai ?", this, imageLink)
            val fm: FragmentManager = supportFragmentManager
            dialog.show(fm, ImageDialog::javaClass.name)

        }
        flashing_ok.setOnClickListener {
            when (FLASHING) {
                0 -> {
                    flashing_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    FLASHING = 1
                }
                1 -> {
                    flashing_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    FLASHING = 0
                }
                2 -> {
                    flashing_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    flashing_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    FLASHING = 1

                }
            }
        }
        flashing_ng.setOnClickListener {
            when (FLASHING) {
                0 -> {
                    flashing_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    FLASHING = 2
                }
                1 -> {
                    flashing_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    flashing_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    FLASHING = 2
                }

                2 -> {
                    flashing_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    FLASHING = 0
                }
            }
            Log.d("f", FLASHING.toString())
        }
        over_silicon_ok.setOnClickListener {
            when (OVER_SILICON) {
                0 -> {
                    over_silicon_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    OVER_SILICON = 1
                }
                1 -> {
                    over_silicon_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    OVER_SILICON = 0
                }
                2 -> {
                    over_silicon_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    over_silicon_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    OVER_SILICON = 1

                }
            }
        }
        over_silicon_ng.setOnClickListener {
            when (OVER_SILICON) {
                0 -> {
                    over_silicon_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    OVER_SILICON = 2
                }
                1 -> {
                    over_silicon_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    over_silicon_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    OVER_SILICON = 2
                }

                2 -> {
                    over_silicon_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    OVER_SILICON = 0
                }
            }
        }
        flow_mark_ok.setOnClickListener {
            when (FLOW_MARK) {
                0 -> {
                    flow_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    FLOW_MARK = 1
                }
                1 -> {
                    flow_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    FLOW_MARK = 0
                }
                2 -> {
                    flow_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    flow_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    FLOW_MARK = 1

                }
            }
        }
        flow_mark_ng.setOnClickListener {
            when (FLOW_MARK) {
                0 -> {
                    flow_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    FLOW_MARK = 2
                }
                1 -> {
                    flow_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    flow_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    FLOW_MARK = 2
                }

                2 -> {
                    flow_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    FLOW_MARK = 0
                }
            }
        }
        stratch_ok.setOnClickListener {
            when (SCRATCH) {
                0 -> {
                    stratch_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    SCRATCH = 1
                }
                1 -> {
                    stratch_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    SCRATCH = 0
                }
                2 -> {
                    stratch_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    stratch_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    SCRATCH = 1

                }
            }
        }
        stratch_ng.setOnClickListener {
            when (SCRATCH) {
                0 -> {
                    stratch_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    SCRATCH = 2
                }
                1 -> {
                    stratch_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    stratch_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    SCRATCH = 2
                }

                2 -> {
                    stratch_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    SCRATCH = 0
                }
            }
        }
        motif_cacat_ok.setOnClickListener {
            when (MOTIF_CACAT) {
                0 -> {
                    motif_cacat_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    MOTIF_CACAT = 1
                }
                1 -> {
                    motif_cacat_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    MOTIF_CACAT = 0
                }
                2 -> {
                    motif_cacat_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    motif_cacat_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    MOTIF_CACAT = 1

                }
            }
        }
        motif_cacat_ng.setOnClickListener {
            when (MOTIF_CACAT) {
                0 -> {
                    motif_cacat_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    MOTIF_CACAT = 2
                }
                1 -> {
                    motif_cacat_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    motif_cacat_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    MOTIF_CACAT = 2
                }

                2 -> {
                    motif_cacat_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    MOTIF_CACAT = 0
                }
            }
        }
        finishing_rapi_ok.setOnClickListener {
            when (FINISHING_RAPI) {
                0 -> {
                    finishing_rapi_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    FINISHING_RAPI = 1
                }
                1 -> {
                    finishing_rapi_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    FINISHING_RAPI = 0
                }
                2 -> {
                    finishing_rapi_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    finishing_rapi_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    FINISHING_RAPI = 1

                }
            }
        }
        finishing_rapi_ng.setOnClickListener {
            when (FINISHING_RAPI) {
                0 -> {
                    finishing_rapi_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    FINISHING_RAPI = 2
                }
                1 -> {
                    finishing_rapi_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    finishing_rapi_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    FINISHING_RAPI = 2
                }

                2 -> {
                    finishing_rapi_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    FINISHING_RAPI = 0
                }
            }
        }
        gas_mark_ok.setOnClickListener {
            when (GAS_MARK) {
                0 -> {
                    gas_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    GAS_MARK = 1
                }
                1 -> {
                    gas_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    GAS_MARK = 0
                }
                2 -> {
                    gas_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    gas_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    GAS_MARK = 1

                }
            }
        }
        gas_mark_ng.setOnClickListener {
            when (GAS_MARK) {
                0 -> {
                    gas_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    GAS_MARK = 2
                }
                1 -> {
                    gas_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    gas_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    GAS_MARK = 2
                }

                2 -> {
                    gas_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    GAS_MARK = 0
                }
            }
        }
        white_mark_ok.setOnClickListener {
            when (WHITE_MARK) {
                0 -> {
                    white_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    WHITE_MARK = 1
                }
                1 -> {
                    white_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    WHITE_MARK = 0
                }
                2 -> {
                    white_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    white_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    WHITE_MARK = 1

                }
            }
        }
        white_mark_ng.setOnClickListener {
            when (WHITE_MARK) {
                0 -> {
                    white_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    WHITE_MARK = 2
                }
                1 -> {
                    white_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    white_mark_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    WHITE_MARK = 2
                }

                2 -> {
                    white_mark_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    WHITE_MARK = 0
                }
            }
        }
        weld_line_ok.setOnClickListener {
            when (WELD_LINE) {
                0 -> {
                    weld_line_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    WELD_LINE = 1
                }
                1 -> {
                    weld_line_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    WELD_LINE = 0
                }
                2 -> {
                    weld_line_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    weld_line_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    WELD_LINE = 1

                }
            }
        }
        weld_line_ng.setOnClickListener {
            when (WELD_LINE) {
                0 -> {
                    weld_line_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    WELD_LINE = 2
                }
                1 -> {
                    weld_line_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    weld_line_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    WELD_LINE = 2
                }

                2 -> {
                    weld_line_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    WELD_LINE = 0
                }
            }
        }
        silver_ok.setOnClickListener {
            when (SILVER) {
                0 -> {
                    silver_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    SILVER = 1
                }
                1 -> {
                    silver_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    SILVER = 0
                }
                2 -> {
                    silver_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    silver_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    SILVER = 1

                }
            }
        }
        silver_ng.setOnClickListener {
            when (SILVER) {
                0 -> {
                    silver_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    SILVER = 2
                }
                1 -> {
                    silver_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    silver_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    SILVER = 2
                }

                2 -> {
                    silver_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    SILVER = 0
                }
            }
        }
        bintik_ok.setOnClickListener {
            when (BINTIK) {
                0 -> {
                    bintik_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    BINTIK = 1
                }
                1 -> {
                    bintik_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    BINTIK = 0
                }
                2 -> {
                    bintik_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    bintik_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    BINTIK = 1

                }
            }
        }
        bintik_ng.setOnClickListener {
            when (BINTIK) {
                0 -> {
                    bintik_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    BINTIK = 2
                }
                1 -> {
                    bintik_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    bintik_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    BINTIK = 2
                }

                2 -> {
                    bintik_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    BINTIK = 0
                }
            }
        }
        retak_ok.setOnClickListener {
            when (RETAK) {
                0 -> {
                    retak_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    RETAK = 1
                }
                1 -> {
                    retak_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    RETAK = 0
                }
                2 -> {
                    retak_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    retak_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    RETAK = 1

                }
            }
        }
        retak_ng.setOnClickListener {
            when (RETAK) {
                0 -> {
                    retak_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    RETAK = 2
                }
                1 -> {
                    retak_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    retak_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    RETAK = 2
                }

                2 -> {
                    retak_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    RETAK = 0
                }
            }
        }
        gate_runner_ok.setOnClickListener {
            when (GATE_RUNNER_RATA) {
                0 -> {
                    gate_runner_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    GATE_RUNNER_RATA = 1
                }
                1 -> {
                    gate_runner_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )
                    GATE_RUNNER_RATA = 0
                }
                2 -> {
                    gate_runner_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    gate_runner_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    GATE_RUNNER_RATA = 1

                }
            }
        }
        gate_runner_ng.setOnClickListener {
            when (GATE_RUNNER_RATA) {
                0 -> {
                    gate_runner_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    GATE_RUNNER_RATA = 2
                }
                1 -> {
                    gate_runner_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    gate_runner_ok.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_blue_700
                        )
                    )

                    GATE_RUNNER_RATA = 2
                }

                2 -> {
                    gate_runner_ng.setBackgroundColor(

                        ContextCompat.getColor(
                            this,
                            R.color.md_red_700
                        )
                    )
                    GATE_RUNNER_RATA = 0
                }
            }
        }
        btn_save.setOnClickListener {
            val fm: FragmentManager = supportFragmentManager
            val dialog = RemarkDialog("IPQC sudah selesai ?", this)
            dialog.show(fm, RemarkDialog::javaClass.name)

        }

    }

    override fun setAdapter() {
        bomPartListAdapter = BomPartListAdapter(this, ArrayList(), this)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        rv_part_bom_list.adapter = bomPartListAdapter
        rv_part_bom_list.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


    }

    @SuppressLint("SetTextI18n")
    override fun loadData() {
        viewModel.getDetail(l_molding_id.toLong()).observe(this, Observer {
            if (it.success!!) {
                viewModel.setIpQcDetailData(it.data)
                pengecekan_ke.text = "Pengecekan Ke : " + (it.data.urutan?.toInt()?.plus(1))
                if (it.data.image != null) {
                    iv_image.setColorFilter(
                        ContextCompat.getColor(
                            this,
                            R.color.teal_700
                        )
                    )
                    imageLink = it.data.image!!
                } else {
                    imageLink = ""
                }
                bomPartListAdapter.setItems(it.data.part_bom)
            }
        })

    }

    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) progress_bar.show()
            else progress_bar.hide()
        })
        viewModel.ipqcDetail.observe(this, Observer {
            ipQcData = it
        })

//        initializeWidgets()
//
//        btnCapture.setOnClickListener { capturePhoto() }
//        btnChoose.setOnClickListener {
//            //check permission at runtime
//            val checkSelfPermission = ContextCompat.checkSelfPermission(
//                this,
//                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
//            )
//            if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
//                //Requests permissions to be granted to this application at runtime
//                ActivityCompat.requestPermissions(
//                    this,
//                    arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), 1
//                )
//            } else {
//                openGallery()
//            }
//        }

    }

    private fun validate() {

        val shot_weight = input_shot_weight.text
        val berat_runner = input_berat_runner.text
        if (shot_weight!!.isEmpty()) {
            input_shot_weight.error = "Shot Weight tidak boleh kosong"
            return
        }
        if (berat_runner!!.isEmpty()) {
            input_berat_runner.error = "Jenis Proses tidak boleh kosong"
            return
        }


    }

    private fun submitData() {

        validate()

        val surface = HashMap<String, Any>()
        surface["flashing"] = FLASHING
        surface["over_silicon"] = OVER_SILICON
        surface["flow_mark"] = FLOW_MARK
        surface["scratch"] = SCRATCH
        surface["motif_cacat"] = MOTIF_CACAT
        surface["finishing_rapi"] = FINISHING_RAPI
        surface["gas_mark"] = GAS_MARK
        surface["white_mark"] = WHITE_MARK
        surface["weld_line"] = WELD_LINE
        surface["silver"] = SILVER
        surface["bintik"] = BINTIK
        surface["retak"] = RETAK
        surface["gate_runner_rata"] = GATE_RUNNER_RATA

        if (FLASHING == 0 || OVER_SILICON == 0 || FLOW_MARK == 0 || SCRATCH == 0 || MOTIF_CACAT == 0 || FINISHING_RAPI == 0 || GAS_MARK == 0 || WHITE_MARK == 0 || WELD_LINE == 0 || SILVER == 0 || BINTIK == 0 || RETAK == 0 || GATE_RUNNER_RATA == 0) {
            toast("Semua surface harus diisi")
            return
        }

        if (bomPartListAdapter.getItemCount()

            != bomPartListAdapter.res.size
        ) {
            toast("Harus isi semua part")
            return
        } else {

            surface["part"] = bomPartListAdapter.res
        }

        surface["shot_weight"] = input_shot_weight.text.toString()
        surface["berat_runner"] = input_berat_runner.text.toString()
        surface["remark_message"] = remark_message
        if (hasImage) {
            val fileSelfie: File = FileUtils.getFile(this, Uri.parse(image))
            val requestSelfie = RequestBody.create("Multipart".toMediaTypeOrNull(), fileSelfie)
            val bodySelfie =
                MultipartBody.Part.createFormData("image", fileSelfie.name, requestSelfie)
            surface["image"] = Constants.DATA_BASE64 + Utils.convertBase64(this, image)
        }
        viewModel.submit(l_molding_id.toLong(),surface).observe(this, Observer {
            if (it.success!!) {
                val i = Intent(this, HomeActivity::class.java)
                this.startActivity(i)
            }
        })

    }


    override fun onLoadBom(data: BomPartData) {
        finish()
    }

    override fun onCloseDialog(string: String, isImage: Boolean) {
        image = string
        hasImage = isImage
    }

    override fun cancelClose() {
    }

    interface OnMyDialogResult {
        fun finish(result: String?)
    }

    override fun onCloseRemarkDialog(string: String) {
        remark_message = string
        submitData()
    }

    override fun cancelRemarkClose() {
        toast("cancel")
    }


}