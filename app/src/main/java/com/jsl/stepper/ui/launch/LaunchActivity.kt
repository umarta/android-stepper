package com.jsl.stepper.ui.launch

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.Window
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.jsl.stepper.R
import com.jsl.stepper.data.user.UserManager
import com.jsl.stepper.ui.auth.AuthenticationViewModel
import com.jsl.stepper.ui.auth.LoginActivity
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.utils.Constants
import com.jsl.stepper.utils.toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class LaunchActivity : AppCompatActivity() {
    @Inject
    lateinit var userManager: UserManager
    private val viewModel: AuthenticationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
    }

    override fun onResume() {
        permission()
        super.onResume()
    }

    private fun permission() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (!report.areAllPermissionsGranted()) {
                        toast("This Application Need Permission")
                        val intent = Intent(
                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.parse("package:$packageName")
                        )
                        intent.addCategory(Intent.CATEGORY_DEFAULT)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    } else {
                        validation()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun validation() {
//        val handler = Handler()
//        handler.postDelayed({
        if (userManager.isLogin) {


            val i = Intent(this, HomeActivity::class.java)

            if (intent.data != null) {
                i.putExtra(
                    Constants.DATA_EXTRA, intent.data.toString()
                        .replace(Constants.HTTPS, Constants.HTTP)
                )
                        }
                        startActivity(i)

            } else {
                userManager.logout()
                val i = Intent(this, LoginActivity::class.java)
                startActivity(i)
            }
            finish()
//        }, 2000)
    }


}