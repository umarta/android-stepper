package com.jsl.stepper.ui.injection

import android.content.Intent
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.jsl.stepper.R
import com.jsl.stepper.data.local.WoDetailObject
import com.jsl.stepper.data.remote.response.wo.WoDetailData
import com.jsl.stepper.model.*
import com.jsl.stepper.ui.home.HomeActivity
import com.jsl.stepper.ui.stepper.InjectionSettingViewModel
import com.jsl.stepper.ui.wo.detail.WoDetailActivity
import com.jsl.stepper.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_injection.*


@AndroidEntryPoint
class InjectionActivity : AppCompatActivity(), BaseApp.Listener {

    private val viewModel: InjectionSettingViewModel by viewModels()
    var isCreated: Boolean = false

    private lateinit var injectionSetting: InjectionSetting
    private lateinit var ejectorSetting: EjectorSetting
    private lateinit var plastizing: Plastizing
    private lateinit var temperatureSetting: TemperatureSetting
    private lateinit var moldSetting: MoldSetting
    private lateinit var woDetailData: WoDetailData
    private lateinit var wo_id: String
    private var first_load: String? = null
    private var created_check: String? = null

    companion object {
        const val PREVIEW = 0
        const val CLAMPING_UNIT_SETTING = 1
        const val EJECTOR_SETTING = 2
        const val INJECTION_UNIT_SETTING = 3
        const val PLASTIZING_SETTING = 4
        const val TEMPERATURE_BARREL_SETTING = 5
        const val REVIEW = 6
        const val FINISH = 7
    }

    private var page = PREVIEW

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_injection)
        BaseApp(this).set()


    }

    override fun getIntentData() {
        if (intent.hasExtra("object")) {
            val woDetailData = intent.getSerializableExtra("object") as? WoDetailObject
            val woData =
                WoDetailData(
                    id = woDetailData?.id,
                    sequenceno = woDetailData?.sequenceno,
                    molding_id = woDetailData?.molding_id,
                    code = woDetailData?.code,
                    name = woDetailData?.name,
                    cavity = woDetailData?.cavity,
                    tonase = woDetailData?.tonase,
                    size = woDetailData?.size,
                    description = woDetailData?.description,
                    colour = woDetailData?.colour,
                    category = woDetailData?.category,
                    merek = woDetailData?.merek,
                    brand = woDetailData?.brand,
                    sub_brand = woDetailData?.sub_brand,
                    status = woDetailData?.status,
                    location = woDetailData?.location,
                    machine = woDetailData?.machine,
                    jenis_proses = woDetailData?.jenis_proses,
                    cycle_time = woDetailData?.cycle_time,
                    material = woDetailData?.material,
                    womolding_id = woDetailData?.womolding_id,
                    is_created = woDetailData?.is_created,
                    product_name = woDetailData?.product_name,
                    mapping_id = woDetailData?.mapping_id,
                    shot_weight = woDetailData?.shot_weight,
                    kode_part = woDetailData?.kode_part
                )
            viewModel.setWoDetail(woData)
        }

        if (intent.hasExtra("wo_id")) {
            wo_id = intent.getStringExtra("wo_id").toString()
        }
        if (intent.hasExtra("created_check")) {
            created_check = intent.getStringExtra("created_check").toString()
        }

        if (intent.hasExtra("first_load")) {
            first_load = intent.getStringExtra("first_load").toString()
        }


    }

    override fun setOnClick() {
        this?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val intent = Intent(this@InjectionActivity, WoDetailActivity::class.java)
                intent.putExtra("type", "machine")
                startActivity(intent)
            }
        })


    }

    override fun setAdapter() {
        val tabAdapter = TabAdapter(supportFragmentManager)
        tabAdapter.addFragment(PreviewFragment(), PreviewFragment::javaClass.name)
        tabAdapter.addFragment(MoldSettingFragment(), MoldSettingFragment::javaClass.name)
        tabAdapter.addFragment(EjectorSettingFragment(), EjectorSettingFragment::javaClass.name)
        tabAdapter.addFragment(InjectionSettingFragment(), InjectionSettingFragment::javaClass.name)
        tabAdapter.addFragment(PlastizingFragment(), PlastizingFragment::javaClass.name)
        tabAdapter.addFragment(TemperatureFragment(), TemperatureFragment::javaClass.name)
        tabAdapter.addFragment(FinishInputFragment(), FinishInputFragment::javaClass.name)
        viewPager.adapter = tabAdapter
    }

    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) progress_bar.show()
            else progress_bar.hide()
        })
        val result = created_check == "yes"

        viewModel.setIsCreated(result)

        viewModel.page.observe(this, Observer { setPage(it) })

        viewModel.woDetail.observe(this, Observer {
            woDetailData = it
            if (it.is_created == true && first_load == "1") {
                it.id?.let { it1 -> getDetail(it1.toInt()) }
            }
        })
        viewModel.moldSetting.observe(this, Observer {
            moldSetting = it

        })
        viewModel.isCreated.observe(this, Observer {
            isCreated = it

        })

        viewModel.injectionSetting.observe(this, Observer {
            injectionSetting = it
        })
        viewModel.ejectorSetting.observe(this, Observer {
            ejectorSetting = it
        })
        viewModel.plastizing.observe(this, Observer {
            plastizing = it
        })
        viewModel.temperatureSetting.observe(this, Observer {
            temperatureSetting = it
        })
    }

    private fun getDetail(settingId: Int) {
        first_load = "2"
        viewModel.settingDetail(settingId).observe(this, {
            if (it.success!!) {
                woDetailData = it.data.preview
                viewModel.setWoDetail(it.data.preview)
                moldSetting = it.data.mold_setting
                viewModel.setMoldSetting(it.data.mold_setting)
                ejectorSetting = it.data.ejector_setting
                viewModel.setEjectorSetting(it.data.ejector_setting)
                injectionSetting = it.data.injection_setting
                viewModel.setInjectionSetting(it.data.injection_setting)

                plastizing = it.data.plastizing
                viewModel.setPlastizing(it.data.plastizing)

                temperatureSetting = it.data.temperature_setting
                viewModel.setTemperatureSetting(it.data.temperature_setting)


            }
        })

    }


    override fun loadData() {
    }

    private fun setPage(mPage: Int) {
        when (mPage) {
            PREVIEW -> {
                page = mPage
                rl_preview_wo.setBackgroundResource(R.drawable.bg_circle_primarylight)
//                tv_preview_wo.setBackgroundResource(R.drawable.bg_circle_primarylight)
                tv_preview_wo_no.setTextColor(ContextCompat.getColor(this, R.color.md_white_1000))
                loadProgress.progress = 15
                viewPager.setCurrentItem(mPage, true)
            }
            CLAMPING_UNIT_SETTING -> {
                page = mPage
                rl_mold_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)
                tv_mold_setting_no.setBackgroundResource(R.drawable.bg_circle_primarylight)
//                tv_mold_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)
                tv_mold_setting_no.setTextColor(ContextCompat.getColor(this, R.color.md_white_1000))
                loadProgress.progress = 30
                viewPager.setCurrentItem(mPage, true)
            }
            EJECTOR_SETTING -> {
                page = mPage
                rl_ejector_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)
                tv_ejector_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)

//                rl_injection_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)
//                tv_injection_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)
                tv_ejector_setting.setTextColor(
                    ContextCompat.getColor(this, R.color.md_white_1000)
                )
                loadProgress.progress = 45
                viewPager.setCurrentItem(mPage, true)
            }
            INJECTION_UNIT_SETTING -> {
                page = mPage
                rl_injection_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)
                tv_injection_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)
                tv_injection_setting.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.md_white_1000
                    )
                )
                loadProgress.progress = 60
                viewPager.setCurrentItem(mPage, true)
            }
            PLASTIZING_SETTING -> {
                page = mPage
                rl_plastizing_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)
                tv_plastizing_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)
                tv_plastizing_setting.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.md_white_1000
                    )
                )
                loadProgress.progress = 75
                viewPager.setCurrentItem(mPage, true)
            }

            TEMPERATURE_BARREL_SETTING -> {
                page = mPage
                rl_temperature_setting.setBackgroundResource(R.drawable.bg_circle_primarylight)
                tv_temperature_setting.setBackgroundResource(R.drawable.bg_circle_primary)
                tv_temperature_setting.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.md_white_1000
                    )
                )
                loadProgress.progress = 80
                viewPager.setCurrentItem(mPage, true)
            }

            REVIEW -> {
                page = mPage
                rl_finish_input.setBackgroundResource(R.drawable.bg_circle_primarylight)
                tv_finish_input.setBackgroundResource(R.drawable.bg_circle_primary)
                tv_finish_input.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.md_white_1000
                    )
                )
                loadProgress.progress = 100
                viewPager.setCurrentItem(mPage, true)
            }

            FINISH -> {
                if (isCreated) {
                    val i = Intent(this, WoDetailActivity::class.java)
                    i.putExtra(Constants.WO_ID, wo_id)
                    this.startActivity(i)

                } else {

                    submit()
                }
            }
        }
    }

    private fun submit() {
        val res = HashMap<String, Any>()
        res["id"] = woDetailData.id.toString()
        res["molding_id"] = woDetailData.molding_id.toString()
        res["mapping_id"] = woDetailData.mapping_id.toString()
        res["preview"] = woDetailData
        res["mold_setting"] = moldSetting
        res["injection_setting"] = injectionSetting
        res["ejector_setting"] = ejectorSetting
        res["plastizing"] = plastizing
        res["sequenceno"] = woDetailData.sequenceno.toString()
        res["temperature_setting"] = temperatureSetting
        viewModel.submit(res).observe(this, Observer {
            if (it.success!!) {
                val i = Intent(this, HomeActivity::class.java)
                this.startActivity(i)
            }
        })

        /*
         res["code"] = woDetailData.code.toString()
         res["tonase"] = woDetailData.tonase.toString()
         res["tonase"] = woDetailData.tonase.toString()
         res["colour"] = woDetailData.colour.toString()
         res["machine"] = woDetailData.machine.toString()
         res["jenis_proses"] = woDetailData.jenis_proses.toString()
         res["material"] = woDetailData.material.toString()
         res["berat_part"] = woDetailData.berat_part.toString()
         res["cavity"] = woDetailData.cavity.toString()
         res["berat_runner"] = woDetailData.berat_runner.toString()
         res["cycle_time"] = woDetailData.cycle_time.toString()
         res["shot_weight"] = woDetailData.shot_weight.toString()*/
    }
}