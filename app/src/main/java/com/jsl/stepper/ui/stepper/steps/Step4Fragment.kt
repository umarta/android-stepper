package com.jsl.stepper.ui.stepper.steps

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jsl.stepper.R
import com.jsl.stepper.utils.BaseApp

class Step4Fragment : Fragment(), BaseApp.Listener {

    override fun onCreate(savedInstanceState: Bundle?) {
        BaseApp(this).set()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_step4, container, false)
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
    }

    override fun setAdapter() {
    }

    override fun setContent() {
    }

    override fun loadData() {
    }


}