package com.jsl.stepper.ui.control

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jsl.stepper.R
import com.jsl.stepper.data.remote.response.production.ProductionControlData
import com.jsl.stepper.ui.material.pengisian.PengisianListAdapter
import com.jsl.stepper.ui.stepper.InjectionSettingViewModel
import com.jsl.stepper.utils.BaseApp
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_injection_setting.*
import kotlinx.android.synthetic.main.activity_pengisian_list.*

@AndroidEntryPoint
class ProductionControlActivity : AppCompatActivity(), BaseApp.Listener,
    ProductionControlAdapter.Listener {
    private val viewModel: ProductionControlViewModel by viewModels()
    private lateinit var productionControlAdapter: ProductionControlAdapter
    private var page: Int = 1;
    private var next_page = true
    private var loading = false
    private var filtered = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_production_control)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        swipeContainer.setOnRefreshListener {
            productionControlAdapter.clear()
            page = 1
            loadData()
            swipeContainer.setRefreshing(false)
        }

    }

    override fun setAdapter() {
        productionControlAdapter = ProductionControlAdapter(this, ArrayList(), this)

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        rv_wo_detail_list.adapter = productionControlAdapter
        rv_wo_detail_list.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_wo_detail_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val countItem = layoutManager.itemCount
                val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                if (!recyclerView.canScrollVertically(1)) {
                    if (next_page) {
                        if (!loading && isLastPosition) {
                            page++
                            loadData()
                        }
                    }
                }
            }
        })

    }

    override fun loadData() {
        viewModel.getData(page).observe(this, Observer {
            if (it.success!!) {
                next_page = it.header.next_page
                if (page == 1) {
                    productionControlAdapter.setItems(it.data)
                } else {
                    productionControlAdapter.addItems(it.data)
                }

            }
        })
    }

    override fun setContent() {

    }

    override fun onLoadList(data: ProductionControlData) {
        finish()
    }
}